# Changelog

## 0.7.0-alpha.7 - Monorepo
  * **Core**
    * UI and API are separated into different packages.
    * Docker integration - easy migration to microservices.
    * Flowtype is back with a vengeance.
  * **Frontend**
    * Separate btblg-frontend package.
  * **Graph**
    * Separate btblg-graph package.

## 0.7.0-alpha.6 - Node.js >= 6.0

## 0.7.0-alpha.5 - Cleanup

## 0.7.0-alpha.4
  * **Core**
    * Refactor directory structure. Separate application code into bundles.
  * **Build**
    * Migrate to webpack 2.0 and use code splitting.

## 0.7.0-alpha.3
  * **Core**
    * Drop flowtype.
    * Integrate react-router with data fetching through redux-saga.

## 0.7.0-alpha.2 - [redux-saga](https://github.com/yelouafi/redux-saga) for side effects

## 0.7.0-alpha.1 - Core directories and dependencies
