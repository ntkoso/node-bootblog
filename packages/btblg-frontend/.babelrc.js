const isProduction = process.env.NODE_ENV === 'production';
const isTest = process.env.NODE_ENV === 'test';

const presets = [
  [
    '@babel/preset-env',
    {
      debug: !isProduction,
      modules: isTest ? 'commonjs' : false,
      targets: { node: true },
      useBuiltIns: 'entry',
    },
  ],
  '@babel/preset-react',
];

let plugins = [
  '@babel/plugin-syntax-flow',
  '@babel/plugin-syntax-class-properties',
  '@babel/plugin-syntax-dynamic-import',
  '@babel/plugin-syntax-object-rest-spread',
  '@babel/plugin-proposal-class-properties',
  '@babel/plugin-proposal-object-rest-spread',
  '@babel/plugin-transform-flow-strip-types',
  'babel-plugin-graphql-tag',
  'babel-plugin-polished',
];

const emotion = {
  sourceMap: !isProduction,
  hoist: isProduction,
  autoLabel: true,
  primaryInstance: '@ntkoso/emotion',
  instances: ['@ntkoso/emotion', '@ntkoso/react-emotion'],
};

if (isProduction) {
  plugins = [['babel-plugin-emotion', emotion], ...plugins];
}

if (!isProduction) {
  plugins = [
    '@babel/plugin-transform-react-jsx-source',
    ['babel-plugin-emotion', emotion],
    ...plugins,
  ];
}

module.exports = { presets, plugins };
