// flow-typed signature: 4b1a2ff2107c2a3e347ba912f58248ce
// flow-typed version: <<STUB>>/@ntkoso/btblg-locale-en_v~0.1.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   '@ntkoso/btblg-locale-en'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module '@ntkoso/btblg-locale-en' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module '@ntkoso/btblg-locale-en/index' {
  declare module.exports: $Exports<'@ntkoso/btblg-locale-en'>;
}
declare module '@ntkoso/btblg-locale-en/index.js' {
  declare module.exports: $Exports<'@ntkoso/btblg-locale-en'>;
}
