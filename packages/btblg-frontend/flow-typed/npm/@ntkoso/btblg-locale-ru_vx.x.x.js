// flow-typed signature: 29cfc6d0bcf715b32e3b2a346c24306c
// flow-typed version: <<STUB>>/@ntkoso/btblg-locale-ru_v~0.1.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   '@ntkoso/btblg-locale-ru'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module '@ntkoso/btblg-locale-ru' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module '@ntkoso/btblg-locale-ru/index' {
  declare module.exports: $Exports<'@ntkoso/btblg-locale-ru'>;
}
declare module '@ntkoso/btblg-locale-ru/index.js' {
  declare module.exports: $Exports<'@ntkoso/btblg-locale-ru'>;
}
