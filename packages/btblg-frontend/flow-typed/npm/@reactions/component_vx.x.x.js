// flow-typed signature: 68a47e2cae3a381dcf85521a6b2cef90
// flow-typed version: <<STUB>>/@reactions/component_v^2.0.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   '@reactions/component'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module '@reactions/component' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module '@reactions/component/es/index' {
  declare module.exports: any;
}

declare module '@reactions/component/umd/react-component-component' {
  declare module.exports: any;
}

declare module '@reactions/component/umd/react-component-component.min' {
  declare module.exports: any;
}

declare module '@reactions/component/umd/reactions-component' {
  declare module.exports: any;
}

declare module '@reactions/component/umd/reactions-component.min' {
  declare module.exports: any;
}

// Filename aliases
declare module '@reactions/component/es/index.js' {
  declare module.exports: $Exports<'@reactions/component/es/index'>;
}
declare module '@reactions/component/index' {
  declare module.exports: $Exports<'@reactions/component'>;
}
declare module '@reactions/component/index.js' {
  declare module.exports: $Exports<'@reactions/component'>;
}
declare module '@reactions/component/umd/react-component-component.js' {
  declare module.exports: $Exports<'@reactions/component/umd/react-component-component'>;
}
declare module '@reactions/component/umd/react-component-component.min.js' {
  declare module.exports: $Exports<'@reactions/component/umd/react-component-component.min'>;
}
declare module '@reactions/component/umd/reactions-component.js' {
  declare module.exports: $Exports<'@reactions/component/umd/reactions-component'>;
}
declare module '@reactions/component/umd/reactions-component.min.js' {
  declare module.exports: $Exports<'@reactions/component/umd/reactions-component.min'>;
}
