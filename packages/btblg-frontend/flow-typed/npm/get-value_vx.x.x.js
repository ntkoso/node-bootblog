// flow-typed signature: a4cb8f459c84d0f54786638bab521dc5
// flow-typed version: <<STUB>>/get-value_v^3.0.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'get-value'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'get-value' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'get-value/index' {
  declare module.exports: $Exports<'get-value'>;
}
declare module 'get-value/index.js' {
  declare module.exports: $Exports<'get-value'>;
}
