// @flow
import React from 'react';
import { hydrate } from 'react-dom';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink } from 'apollo-link';
import { BatchHttpLink } from 'apollo-link-batch-http';
import { withClientState } from 'apollo-link-state';
import { BrowserRouter } from 'react-router-dom';
import Loadable from 'react-loadable';
import loadLocaleData from 'intl-app-utils/loadLocaleData';
import webpackOfflineSW from 'offline-plugin/runtime';
import gql from 'graphql-tag';
import { light as theme } from '@ntkoso/btblg-style-guide/es/themes';
import log from 'log';
import createClientSchema from 'common/createClientSchema';
import { Root } from 'common/components';
import makeUIInteractive from 'features/ui/queries/makeUIInteractive';

const configureDevEnvironment = (): void => {
  if (process.env.NODE_ENV !== 'production') {
    log.info(
      // eslint-disable-line
      `Current environment: ${process.env.NODE_ENV || 'development'}`,
    );
    window.React = React;
  }
};

const cleanup = (): void => {
  delete window.__APOLLO_INITIAL_STATE__;
  delete window.__EMOTION_NONCE__;

  if (process.env.NODE_ENV === 'production') {
    const initialStatesElement = document.getElementById('initialStates');
    if (initialStatesElement && initialStatesElement.parentNode) {
      initialStatesElement.parentNode.removeChild(initialStatesElement);
    }
  }
};

const loadCurrentLocaleData = async client => {
  const {
    data: {
      intl: { locale },
    },
  } = await client.query({
    query: gql`
      query CurrentLocale {
        intl @client {
          locale
        }
      }
    `,
  });

  await loadLocaleData(locale);
};

const configureApolloStack = async apolloInitialState => {
  const cache = new InMemoryCache({ dataIdFromObject: ({ id }) => id });

  const local = withClientState({ ...createClientSchema(), cache });

  const batchHttp = new BatchHttpLink({
    credentials: 'same-origin',
    uri: process.env.GRAPHQL_PREFIX || '/graph',
  });

  let logger;

  if (process.env.NODE_ENV !== 'production') {
    logger = await import(/* webpackChunkName: "dev-ApolloLinkLogger" */ 'apollo-link-logger').then(
      module => module.default,
    );
  }

  const link = ApolloLink.from([logger, local, batchHttp].filter(Boolean));

  const client = new ApolloClient({
    link,
    cache: cache.restore(apolloInitialState),
    ssrForceFetchDelay: 100,
    connectToDevTools: process.env.NODE_ENV !== 'production',
    queryDeduplication: true,
  });

  return client;
};

const start = async () => {
  try {
    process.env.NODE_ENV !== 'production' && configureDevEnvironment(); // eslint-disable-line

    const apolloInitialState = window.__APOLLO_INITIAL_STATE__ || {};

    const client = await configureApolloStack(apolloInitialState);

    await Promise.all([Loadable.preloadReady(), loadCurrentLocaleData(client)]);

    hydrate(
      <Root component={BrowserRouter} apollo={client} theme={theme} />,
      document.getElementById('app'),
    );

    cleanup();

    client.mutate({ mutation: makeUIInteractive });

    log.info('Initialization success'); // eslint-disable-line

    if ('serviceWorker' in navigator) {
      const delay = t => new Promise(resolve => setTimeout(resolve, t));

      // wait for initial assets to load. http 1.1 has a concurrency problem
      await delay(1000);

      webpackOfflineSW.install();

      log.info('service worker initialization success'); // eslint-disable-line
    }
  } catch (err) {
    log.fatal(err, 'Initialization failure'); // eslint-disable-line
  }
};

export default start;
