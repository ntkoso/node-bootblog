// @flow
import get from 'get-value';
import Koa from 'koa';
import conditionalGet from 'koa-conditional-get';
import etag from 'koa-etag';
import logger from 'koa-pino-logger';
import helmet from 'koa-helmet';
import uuidv4 from 'uuid/v4';
import error from '@ntkoso/btblg-koa-error-react';
import intl from '@ntkoso/btblg-koa-react-intl';
import { light as theme } from '@ntkoso/btblg-style-guide/es/themes';
import insertGlobalRules from '@ntkoso/btblg-style-guide/es/globals';
import log from 'log';
import application from 'common/application';
import { Root } from 'common/components';
import loadable from '../public/assets/react-loadable.json';
import stats from '../public/assets/stats.json';
import favicon from '../public/assets/favicon-stats.json';

insertGlobalRules();

const app = new Koa();

app.proxy = true;

app.use(
  logger({
    logger: log,
    genReqId: () => uuidv4(),
  }),
);

app.use(error());

/* ETAG */

app.use(conditionalGet());
app.use(etag());

/* GUARDS */

app.use(helmet.dnsPrefetchControl());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.referrerPolicy());
app.use(helmet.xssFilter());

/* CONTENT SECURITY POLICY */

app.use(async (ctx, next) => {
  ctx.res.nonce = uuidv4();

  return next();
});

app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
      childSrc: ["'self'"],
      scriptSrc: [
        "'self'",
        ({ res: { nonce } }) => `'nonce-${nonce}'`,
        "'unsafe-eval'",
      ],
      styleSrc: ["'self'", "'unsafe-inline'"],
      imgSrc: ["'self'", 'data:', 'http://lorempixel.com'],
      sandbox: ['allow-forms', 'allow-scripts', 'allow-same-origin'],
      objectSrc: ["'none'"],
      workerSrc: ["'self'"],
      upgradeInsecureRequests: true,
    },
  }),
);

/* INTL */

app.use(intl());

/* APPLICATION */

const getInitialState = ctx => ({
  config: {
    __typename: 'Config',
    recaptchaSiteKey: process.env.RECAPTCHA_SITE_KEY || '',
  },
  intl: {
    __typename: 'Intl',
    locale: get(ctx, ['state', 'intl', 'locale']),
    messages: {
      __typename: 'IntlMessages',
      ...get(ctx, ['state', 'intl', 'messages']),
    },
    initialNow: Date.now(),
  },
});

app.use(
  application({
    build: {
      loadable,
      stats,
      favicon,
    },
    graph: {
      port: process.env.GRAPHQL_PORT,
      host: process.env.GRAPHQL_HOST,
    },
    state: {
      getInitialState,
      theme,
    },
  }),
);

export default app;
