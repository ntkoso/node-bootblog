import createAssetsFlush from '../createAssetsFlush';

const stats = {
  assetsByChunkName: { index: 'index.js', vendor: 'vendor.js' },
  publicPath: '/test/',
};

const loadable = {
  'common/components/CommonTest': [
    {
      file: 'common-CommonTest.js',
      publicPath: '/assets/common-CommonTest.js',
      chunkName: 'common-CommonTest',
      id: './src/common/components/CommonTest.js',
      name: './src/common/components/CommonTest.js',
    },
  ],
  '../test/components/Test': [
    {
      file: 'vendors-test-Test.js',
      publicPath: '/assets/vendors-test-Test.js',
      chunkName: 'vendors-test-Test',
      id: './src/test/components/Test.js',
      name: './src/test/components/Test.js',
    },
    {
      file: 'test-Test.js',
      publicPath: '/assets/test-Test.js',
      chunkName: 'test-Test',
      id: './src/test/components/Test.js',
      name: './src/test/components/Test.js',
    },
  ],
};

test('flushes javascript assets', () => {
  const assetsFlush = createAssetsFlush(loadable, stats, {
    before: ['vendor'],
    after: ['index'],
  });

  expect(assetsFlush([])).toMatchSnapshot();

  expect(assetsFlush(['common/components/CommonTest'])).toMatchSnapshot();

  expect(
    assetsFlush(['common/components/CommonTest', '../test/components/Test']),
  ).toMatchSnapshot();

  expect(
    assetsFlush(['common/components/CommonTest', 'common/components/NotFound']),
  ).toMatchSnapshot();
});
