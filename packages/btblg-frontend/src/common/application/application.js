// @flow
import React from 'react';
import type { ComponentType } from 'react';
import { renderToNodeStream } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink } from 'apollo-link';
import { BatchHttpLink } from 'apollo-link-batch-http';
import { withClientState } from 'apollo-link-state';
import { getDataFromTree } from 'react-apollo';
import Loadable from 'react-loadable';
import multistream from 'multistream';
import stringToStream from 'string-to-stream';
import { renderStylesToNodeStream } from '@ntkoso/emotion-server';
import type { Theme } from '@ntkoso/btblg-frontend/es/styles/themes/types';
import { Root } from 'common/components';
import createClientSchema from 'common/createClientSchema';
import createAssetsFlush from './createAssetsFlush';
import type { LoadableStats, BuildStats, FaviconStats } from './types';
import renderHtml from './renderHtml';

function write(data) {
  this.queue(data);
}

const createEnd = after =>
  function end() {
    this.queue(after);
    this.queue(null);
  };

const configureApolloStack = (initialClientState, { uri, headers }) => {
  const cache = new InMemoryCache({
    dataIdFromObject: ({ id }) => id,
  });

  const local = withClientState({
    ...createClientSchema(initialClientState),
    cache,
  });

  const http = new BatchHttpLink({
    credentials: 'same-origin',
    headers,
    uri,
  });

  const link = ApolloLink.from([local, http]);

  const client = new ApolloClient({
    link,
    cache,
    ssrMode: true,
    connectToDevTools: false,
    queryDeduplication: true,
  });

  return client;
};

const defaultGetInitialState = <C: { state: Object }>(
  koaContext: C,
): { [__typename: string]: () => Object } => ({});

type ApplicationParams = {|
  +build: {
    loadable: LoadableStats,
    stats: BuildStats,
    favicon: FaviconStats,
  },
  +graph: {|
    +port: number | string,
    +host: string,
  |},
  +state: {|
    +getInitialState?: typeof defaultGetInitialState,
    +theme?: Theme,
  |},
|};

export default function application({
  build,
  graph,
  state,
}: ApplicationParams): (...args: Array<any>) => Promise<any> {
  const { host, port } = graph;
  const { loadable, stats, favicon } = build;
  const { getInitialState = defaultGetInitialState, theme = {} } = state;

  const flushAssets = createAssetsFlush(loadable, stats, {
    before: ['manifest'],
    after: ['index'],
  });

  return async function applicationMiddleware(
    ctx: {
      body: string,
      headers: Object,
      state: Object,
      url: string,
      redirect: (url: string) => void,
      res: { nonce: string },
    },
    next: (...args: Array<any>) => Promise<any> | Promise<any>,
  ) {
    const client = configureApolloStack(getInitialState(ctx), {
      uri: `http://${host}:${port}`,
      headers: ctx.headers,
    });

    const routerContext = {};
    const helmetContext = {};
    const modules = new Set();

    const app = (
      <Loadable.Capture report={moduleName => modules.add(moduleName)}>
        <Root
          component={StaticRouter}
          apollo={client}
          helmet={helmetContext}
          routerProps={{ location: ctx.url, context: routerContext }}
          theme={theme}
        />
      </Loadable.Capture>
    );

    /* APOLLO DATA FETCHING */
    await getDataFromTree(app);

    /* REDIRECT */
    if (routerContext.url) {
      ctx.redirect(routerContext.url);

      return next();
    }

    /* RENDERING */
    const js = flushAssets(Array.from(modules));

    const [before, after] = renderHtml(
      {
        apollo: {
          STATE_IDENTIFIER: '__APOLLO_INITIAL_STATE__',
          state: client.cache.extract(),
        },
      },
      js,
      favicon,
      helmetContext.helmet,
      {
        nonce: ctx.res.nonce,
      },
    );

    ctx.set('Content-Type', 'text/html');

    ctx.body = multistream([
      stringToStream('<!DOCTYPE html>'),
      stringToStream(before),
      renderToNodeStream(app).pipe(renderStylesToNodeStream()),
      stringToStream(after),
    ]);

    return next();
  };
}
