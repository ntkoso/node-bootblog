// @flow
import type { LoadableStats, BuildStats, LoadableModules } from './types';

type Options = {| +before: Array<string>, +after: Array<string> |};

const generateTags = (files: Array<string>, publicPath: string) =>
  [...new Set(files)]
    .map(
      file =>
        `<script type="text/javascript" charset="utf-8" defer src="${publicPath}${file}"></script>`,
    )
    .join(process.env.NODE_ENV !== 'production' ? '\n' : '');

const getFilenames = (stats: LoadableStats, modules: LoadableModules) =>
  modules
    .map(module => stats[module])
    .filter(Boolean)
    .reduce((prev, bundles) => [...prev, ...bundles], [])
    .map(({ file }) => file);

const createAssetsFlush = (
  loadable: LoadableStats,
  stats: BuildStats,
  { before, after }: Options,
) => {
  const { publicPath, assetsByChunkName } = stats;

  const getAsset = asset => assetsByChunkName[asset];
  const beforeFilenames = before.map(getAsset).filter(Boolean);
  const afterFilenames = after.map(getAsset).filter(Boolean);

  return (modules: LoadableModules): string => {
    return generateTags(
      [
        ...beforeFilenames,
        ...getFilenames(loadable, modules),
        ...afterFilenames,
      ],
      publicPath,
    );
  };
};

export default createAssetsFlush;
