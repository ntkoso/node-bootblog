// @flow
import serialize from 'serialize-javascript';
import { nonce as cssNonce } from '@ntkoso/react-emotion';
import type { FaviconStats } from './types';

const srlz = (data: Array<any> | Object): string =>
  serialize(data, { isJSON: true });

type StateData<I, S> = {| +STATE_IDENTIFIER: I, +state: S |};

export type State = {|
  +apollo: StateData<'__APOLLO_INITIAL_STATE__', Object>,
|};

type RenderParts = {|
  +html: string,
  +js: string,
|};

type HelmetParts = {
  +title: string,
  +meta: string,
  +link: string,
  +script: string,
  +htmlAttributes: string,
};

type SecurityParts = {
  +nonce: string,
};

const renderBefore = (
  js: string,
  { html }: FaviconStats,
  { htmlAttributes, meta, title, link, script }: HelmetParts,
) => `<html ${htmlAttributes}>
  <head>
    ${title}
    ${meta}
    ${Array.isArray(html) ? html.join('') : ''}
    ${link}
    ${js}
    ${script}
  </head>
  <body>
    <div id="app">`;

const renderAfter = (
  { apollo }: State,
  { jsNonce, cssNonce }: { jsNonce: string, cssNonce: string },
) => `</div>
   <script nonce="${jsNonce}" id="initialStates" type="text/javascript" charset="utf-8">
     window.${apollo.STATE_IDENTIFIER}=${srlz(apollo.state)};
     window.__EMOTION_NONCE__="${cssNonce}";
   </script>
  </body>
</html>`;

const renderHtml = (
  state: State,
  js: string,
  favicon: FaviconStats,
  helmet: HelmetParts,
  { nonce }: SecurityParts,
): [string, string] => [
  renderBefore(js, favicon, helmet),
  renderAfter(state, { jsNonce: nonce, cssNonce }),
];

export default renderHtml;
