// @flow
type Bundle = { file: string };

export type BuildStats = {
  assetsByChunkName: { [name: string]: string },
  publicPath: string,
};

export type LoadableStats = { [module: string]: Array<Bundle> };

export type LoadableModules = Array<string>;

export type FaviconStats = {
  outputFilePrefix: string,
  html: Array<string>,
  files: Array<string>,
};
