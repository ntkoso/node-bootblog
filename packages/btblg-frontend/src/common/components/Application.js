// @flow
import get from 'get-value';
import compose from 'compose-function';
import React, { Fragment } from 'react';
import type { Element } from 'react';
import { injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';
import { Route, Switch } from 'react-router-dom';
import Helmet from 'react-helmet-async';
import Loadable from 'react-loadable';
import { withTheme } from 'emotion-theming';
import { rem } from 'polished';
import type { Theme } from '@ntkoso/btblg-style-guide/es/themes/types';
import { Layout } from '@ntkoso/btblg-style-guide/es/components';
import Footer from 'common/components/parts/Footer';
import { Metabar } from 'features/ui/components';
import Loading from './Loading';

const LoadableHomeScreen = Loadable({
  loader: () =>
    import(/* webpackChunkName: "common-HomeScreen" */ './screens/HomeScreen').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['./screens/HomeScreen'],
  webpack: () => [require.resolveWeak('./screens/HomeScreen')],
});

const LoadableAdminScreen = Loadable({
  loader: () =>
    import(/* webpackChunkName: "admin-AdminScreen" */ 'features/admin/components/screens/AdminScreen').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['features/admin/components/screens/AdminScreen'],
  webpack: () => [
    require.resolveWeak('features/admin/components/screens/AdminScreen'),
  ],
});

const LoadableAuthScreen = Loadable({
  loader: () =>
    import(/* webpackChunkName: "auth-AuthScreen" */ 'features/auth/components/screens/AuthScreen').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['features/auth/components/screens/AuthScreen'],
  webpack: () => [
    require.resolveWeak('features/auth/components/screens/AuthScreen'),
  ],
});

const LoadableCategoryScreen = Loadable({
  loader: () =>
    import(/* webpackChunkName: "category-CategoryScreen" */ 'features/category/components/screens/CategoryScreen').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['features/category/components/screens/CategoryScreen'],
  webpack: () => [
    require.resolveWeak('features/category/components/screens/CategoryScreen'),
  ],
});

const LoadableNewThreadScreen = Loadable({
  loader: () =>
    import(/* webpackChunkName: "thread-NewThreadScreen" */ 'features/thread/components/screens/NewThreadScreen').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['features/thread/components/screens/NewThreadScreen'],
  webpack: () => [
    require.resolveWeak('features/thread/components/screens/NewThreadScreen'),
  ],
});

const LoadableThreadsScreen = Loadable({
  loader: () =>
    import(/* webpackChunkName: "thread-ThreadsScreen" */ 'features/thread/components/screens/ThreadsScreen').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['features/thread/components/screens/ThreadsScreen'],
  webpack: () => [
    require.resolveWeak('features/thread/components/screens/ThreadsScreen'),
  ],
});

const LoadableNotFoundScreen = Loadable({
  loader: () =>
    import(/* webpackChunkName: "common-NotFoundScreen" */ './screens/NotFoundScreen').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['./screens/NotFoundScreen'],
  webpack: () => [require.resolveWeak('./screens/NotFoundScreen')],
});

let LoadableStyleGuide;
if (process.env.NODE_ENV !== 'production') {
  LoadableStyleGuide = Loadable({
    loader: () =>
      import(/* webpackChunkName: "dev-StyleGuide" */ '@ntkoso/btblg-style-guide/es/components/StyleGuide').then(
        module => module.default,
      ),
    loading: Loading,
    modules: ['@ntkoso/btblg-style-guide/es/components/StyleGuide'],
    webpack: () => [
      require.resolveWeak('@ntkoso/btblg-style-guide/es/components/StyleGuide'),
    ],
  });
}

type Props = { intl: IntlShape, theme: Theme };

export const Application = ({
  intl,
  theme,
}: Props): Element<typeof Fragment> => (
  <Fragment>
    <Helmet defaultTitle="booblog" titleTemplate="%s - bootblog">
      <html lang={intl.locale || intl.defaultLocale || 'en'} />
      <meta charSet="UTF-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="description" content="bootblog" />
    </Helmet>
    <Layout
      header={({ gridArea }) => <Metabar gridArea={gridArea} />}
      content={({ gridArea }) => (
        <Switch>
          <Route
            exact
            path="/"
            render={props => (
              <LoadableHomeScreen {...props} gridArea={gridArea} />
            )}
          />
          <Route
            path="/admin"
            render={props => (
              <LoadableAdminScreen {...props} gridArea={gridArea} />
            )}
          />
          <Route
            path="/auth"
            render={props => (
              <LoadableAuthScreen {...props} gridArea={gridArea} />
            )}
          />
          <Route
            path="/categories"
            render={props => (
              <LoadableCategoryScreen {...props} gridArea={gridArea} />
            )}
          />
          <Route
            path="/new-thread"
            render={props => (
              <LoadableNewThreadScreen {...props} gridArea={gridArea} />
            )}
          />
          <Route
            path="/threads"
            render={props => (
              <LoadableThreadsScreen {...props} gridArea={gridArea} />
            )}
          />
          {process.env.NODE_ENV !== 'production' && (
            <Route
              path="/_style-guide"
              render={props =>
                LoadableStyleGuide && (
                  <LoadableStyleGuide {...props} gridArea={gridArea} />
                )
              }
            />
          )}
          <Route
            render={props => (
              <LoadableNotFoundScreen {...props} gridArea={gridArea} />
            )}
          />
        </Switch>
      )}
      footer={({ gridArea }) => <Footer gridArea={gridArea}>FOOTER</Footer>}
    />
  </Fragment>
);

const container = compose(injectIntl, withTheme);

export default container(Application);
