// @flow
import React from 'react';
import type { Element } from 'react';
import { LoadingSpinner } from '@ntkoso/btblg-style-guide/es/components';

type Props = {
  isLoading?: boolean,
  timeOut?: boolean,
  pastDelay?: boolean,
  error?: typeof Error,
};

// TODO: this is a placeholder, change it later to real loading indicator
const Loading = ({
  isLoading,
  error,
  timeOut,
  pastDelay,
}: Props): Element<typeof LoadingSpinner> | string | null => {
  if (error) {
    return `Error: ${error.toString()}`;
  } else if (timeOut) {
    return 'Taking a long time';
  } else if (pastDelay || isLoading) {
    return <LoadingSpinner />;
  }

  return null;
};

export default Loading;
