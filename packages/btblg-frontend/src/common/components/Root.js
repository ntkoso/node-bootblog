// @flow
import React, { Component, Fragment, StrictMode } from 'react';
import type { Element, ComponentType } from 'react';
import { ApolloProvider } from 'react-apollo';
import type {
  Router as RouterT,
  StaticRouter as StaticRouterT,
  BrowserRouter as BrowserRouterT,
} from 'react-router-dom';
import { ThemeProvider } from 'emotion-theming';
import { HelmetProvider } from 'react-helmet-async';
import type { Theme } from '@ntkoso/btblg-style-guide/es/themes/types';
import IntlProvider from 'features/intl/components/IntlProvider';
import Application from './Application';

type Props = {
  component: BrowserRouterT | StaticRouterT | RouterT,
  apollo: Object,
  helmet?: Object,
  theme?: Theme,
  routerProps?: Object,
};

type State = {
  message: Error | null,
  componentStack: string | null,
};

const displayComponentStack = (stack: string): Array<Element<'li'>> =>
  stack
    .split(/(?:\r\n|\r|\n)/g)
    .filter(item => item !== '')
    .map(item => <li key={item}>{item}</li>);

class Root extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = { message: null, componentStack: null };
  }

  componentDidCatch(
    error: Error,
    { componentStack }: { componentStack: string },
  ) {
    this.setState(() => ({ message: error, componentStack }));
  }

  render(): Element<typeof Fragment> | Element<typeof StrictMode> {
    const { message, componentStack } = this.state;

    if (message && typeof componentStack === 'string') {
      return (
        <Fragment>
          <h2>{message.toString()}</h2>,
          <h3>Trace: </h3>,
          <ol reversed>{displayComponentStack(componentStack)}</ol>,
        </Fragment>
      );
    }

    const {
      component: Router,
      apollo,
      helmet = {},
      theme = {},
      routerProps = {},
    } = this.props;

    return process.env.NODE_ENV !== 'production' ? (
      <StrictMode>
        <ThemeProvider theme={theme}>
          <ApolloProvider client={apollo}>
            <IntlProvider textComponent={Fragment}>
              <HelmetProvider context={helmet}>
                <Router {...routerProps}>
                  <Application />
                </Router>
              </HelmetProvider>
            </IntlProvider>
          </ApolloProvider>
        </ThemeProvider>
      </StrictMode>
    ) : (
      <ThemeProvider theme={theme}>
        <ApolloProvider client={apollo}>
          <IntlProvider textComponent={Fragment}>
            <HelmetProvider context={helmet}>
              <Router {...routerProps}>
                <Application />
              </Router>
            </HelmetProvider>
          </IntlProvider>
        </ApolloProvider>
      </ThemeProvider>
    );
  }
}

export default Root;
