// @flow
import React from 'react';
import type { Element } from 'react';

type Props = { defaultValue: string, onChange: Function };

export const SearchForm = ({
  defaultValue,
  onChange,
}: Props): Element<'form'> => (
  <form>
    <input type="text" onChange={onChange} defaultValue={defaultValue} />
  </form>
);

export default SearchForm;
