// @flow
import React, { Fragment } from 'react';
import type { Element } from 'react';
import Component from '@reactions/component';
import styled from '@ntkoso/react-emotion';
import MenuDownIcon from '@ntkoso/btblg-icon-set/menu-down.svg';
import MenuUpIcon from '@ntkoso/btblg-icon-set/menu-up.svg';
import { isSelected } from './utils';

const ArrowWrapper = styled.div({
  display: 'inline-block',
  float: 'right',
});

const Options = styled.ul({
  position: 'absolute',
  listStyle: 'none',
});

const Wrapper = styled.div({
  cursor: 'pointer',
});

const Menu = styled.div({});

type Props = {
  initialValue: any,
  renderSegment: Function,
  renderOption: Function,
  onChange?: Function,
  options: Array<Object>,
};

export const DropDown = ({
  initialValue,
  renderSegment,
  renderOption,
  onChange,
  options,
}: Props): Element<typeof Wrapper> => (
  <Wrapper>
    <Component initialState={{ active: false, value: null }}>
      {({ setState, state: { active, value } }) => (
        <Fragment>
          <Menu
            active={active}
            onMouseDown={event => {
              event.preventDefault();

              setState({ active: !active });
            }}
          >
            {renderSegment({ value: value || initialValue || null })}
            <ArrowWrapper>
              {!active ? <MenuDownIcon /> : <MenuUpIcon />}
            </ArrowWrapper>
          </Menu>
          {active && (
            <Options>
              {/* FIXME: CLEANUP THIS MESS */}
              {/* options.filter(option => !isSelected(value)(option)).map(
                ({ val, label }) => (
                  <li key={val}>
                    {renderOption({
                      onMouseDown: v => {
                        typeof onChange === 'function' && onChange(v);

                        setState({ value: v });
                      },
                      value: val,
                      label,
                    })}
                  </li>
                ),
                reject(isSelected(value), options),
              )*/}
            </Options>
          )}
        </Fragment>
      )}
    </Component>
  </Wrapper>
);

export default DropDown;
