// @flow
type IsSelected = (value: mixed) => (option: { value: mixed }) => boolean;
export const isSelected: IsSelected = value => option => option.value === value;
