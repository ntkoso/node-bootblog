// @flow
import React from 'react';
import type { Element } from 'react';
import { Query } from 'react-apollo';
import Recaptcha from 'react-grecaptcha';
import gql from 'graphql-tag';
import get from 'get-value';

const RECAPTCHA_SETTINGS_QUERY = gql`
  query RecaptchaData {
    config @client {
      recaptchaSiteKey
    }
    intl @client {
      locale
    }
  }
`;

type Props = {
  className: string,
  field: { onChange: Function },
};

export const RecaptchaField = ({
  className,
  field: { onChange },
}: Props): Element<typeof Query> => (
  <Query query={RECAPTCHA_SETTINGS_QUERY}>
    {({ data } = {}) => {
      const recaptchaSiteKey = get(data, ['config', 'recaptchaSiteKey']);
      const locale = get(data, ['intl', 'locale']) || 'en';

      return (
        typeof recaptchaSiteKey === 'string' &&
        recaptchaSiteKey !== '' && (
          <Recaptcha
            sitekey={recaptchaSiteKey}
            locale={locale || 'en'}
            className="g-recaptcha"
            data-theme="light"
            callback={onChange}
            expiredCallback={() => {
              window && window.recaptcha && window.recaptcha.reset();

              onChange(null);
            }}
          />
        )
      );
    }}
  </Query>
);

export default RecaptchaField;
