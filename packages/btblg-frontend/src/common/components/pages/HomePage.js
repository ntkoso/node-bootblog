// @flow
import React from 'react';
import type { Element } from 'react';
import { withProps } from 'recompose';
import styled from '@ntkoso/react-emotion';
import { Content } from '@ntkoso/btblg-style-guide/es/components/typography';
import { ProfileLink } from 'features/user/components';

type Props = {
  isLoading: boolean,
  loadMoreThreads: Function,
  threads?: Array<Object>,
  pageInfo?: Object,
};

const Main = withProps({ role: 'main' })(styled.main({}));

export const HomePage = ({
  threads,
  pageInfo,
  isLoading,
  loadMoreThreads,
}: Props): Element<typeof Main> => (
  <Main>
    <Content>
      {/* <ProfileLink username="Archer">Archer</ProfileLink> */}
    </Content>
  </Main>
);

export default HomePage;
