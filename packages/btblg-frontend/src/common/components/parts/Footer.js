// @flow
import { withProps } from 'recompose';
import styled from '@ntkoso/react-emotion';

export const Footer = withProps({ role: 'contentinfo' })(
  styled.footer(
    {
      textAlign: 'center',
    },
    ({ gridArea }) => ({ gridArea }),
  ),
);

export default Footer;
