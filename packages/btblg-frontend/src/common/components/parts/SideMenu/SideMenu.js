// @flow
import React from 'react';
import type { Element } from 'react';
import { Spring, config, animated } from 'react-spring';
import Component from '@reactions/component';
import styled from '@ntkoso/react-emotion';

const Wrapper = styled(animated.div)({
  position: 'fixed',
  top: 0,
  left: 0,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '100vh',
  maxWidth: '75vw',
  minWidth: '25vw',
  backgroundColor: 'rgba(0, 0, 0, 1)',
});

const Section = styled.section({});

type Props = {
  active: boolean,
  onClose?: () => void,
};

export const SideMenu = ({
  active,
  onClose,
}: Props): Element<typeof Spring> => (
  <Spring
    native
    config={config.slow}
    from={{}}
    to={{ x: active ? 0 : -100, opacity: active ? 1 : 0 }}
  >
    {({ x, opacity, backgroundColor }) => (
      <Wrapper
        style={{
          transform: x.interpolate(x => `translate3d(${x}%, 0, 0)`),
          backgroundColor: opacity.interpolate(op => `rgba(0, 0, 0, ${op})`),
        }}
      >
        <div />
      </Wrapper>
    )}
  </Spring>
);

export default SideMenu;
