// @flow
import React from 'react';
import type { Element } from 'react';

export const Sidebar = (): Element<'aside'> => <aside>sidebar</aside>;

export default Sidebar;
