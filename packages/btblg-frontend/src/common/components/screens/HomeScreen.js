// @flow
import React from 'react';
import type { Element } from 'react';
import { Route } from 'react-router-dom';
import styled from '@ntkoso/react-emotion';
import Loadable from 'react-loadable';
import { Loading } from 'common/components';

const LoadableHomePage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "common-HomePage" */ '../pages/HomePage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/HomePage'],
  webpack: () => [require.resolveWeak('../pages/HomePage')],
});

const LoadableSidebar = Loadable({
  loader: () =>
    import(/* webpackChunkName: "common-Sidebar" */ '../parts/Sidebar').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../parts/Sidebar'],
  webpack: () => [require.resolveWeak('../parts/Sidebar')],
});

const Content = styled.div();

type Props = { gridArea: string };

export const HomeScreen = ({ gridArea }: Props): Element<typeof Content> => (
  <Content style={{ gridArea }}>
    <Route path="/" exact render={() => <LoadableHomePage />} />
    <Route path="/" exact render={() => <LoadableSidebar />} />
  </Content>
);

export default HomeScreen;
