// @flow
import React from 'react';
import type { Element } from 'react';
import type { Location } from 'react-routed-dom';
import styled from '@ntkoso/react-emotion';

const Content = styled.div(({ gridArea }) => ({ gridArea }));

type Props = { gridArea: string, location: Location };

export const NotFoundScreen = ({
  gridArea,
  location,
}: Props): Element<typeof Content> => (
  <Content gridArea={gridArea}>{`Oops, requested '${
    location.pathname
  }' path was not found.`}</Content>
);

export default NotFoundScreen;
