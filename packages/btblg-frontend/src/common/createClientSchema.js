// @flow
import merge from 'deepmerge';
import { defaults as configDefaults } from 'features/config/resolvers';
import { defaults as intlDefaults } from 'features/intl/resolvers';
import uiResolvers, { defaults as uiDefaults } from 'features/ui/resolvers';

type InitialState = { [key: string]: any };

const createClientSchema = (initialState?: InitialState = {}) => ({
  defaults: merge.all([configDefaults, intlDefaults, uiDefaults, initialState]),
  resolvers: merge.all([uiResolvers]),
});

export default createClientSchema;
