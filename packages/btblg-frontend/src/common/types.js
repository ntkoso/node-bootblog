// @flow

export type IntlMessage = {
  id: string,
  defaultMessage?: string,
  message?: string,
};

export type IntlProp = {
  formatMessage: (message: IntlMessage) => string,
};
