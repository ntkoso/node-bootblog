// @flow
import React from 'react';
import type { Element } from 'react';
import Helmet from 'react-helmet-async';
import { defineMessages, injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';
import { animated } from 'react-spring';

const messages = defineMessages({
  title: {
    id: 'admin.categories.title',
    defaultMessage: 'Admin - Categories',
  },
});

type Props = {
  intl: IntlShape,
};

export const CategoriesPage = ({
  intl: { formatMessage },
  style,
  ...rest
}: Props): Element<typeof animated.main> => (
  <animated.main role="main" style={style}>
    <Helmet title={formatMessage(messages.title)} />
    Categories
  </animated.main>
);

export default injectIntl(CategoriesPage);
