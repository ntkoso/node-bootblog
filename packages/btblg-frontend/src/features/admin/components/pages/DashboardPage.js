// @flow
import React from 'react';
import type { Element } from 'react';
import Helmet from 'react-helmet-async';
import { defineMessages, injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';
import { animated } from 'react-spring';
import { InteractiveButton } from 'features/ui/components';

const messages = defineMessages({
  title: {
    id: 'admin.dashboard.title',
    defaultMessage: 'Admin',
  },
});

type Props = {
  intl: IntlShape,
};

export const DashboardPage = ({
  intl: { formatMessage },
  style,
  ...rest
}: Props): Element<typeof animated.main> => (
  <animated.main role="main" style={style}>
    <Helmet title={formatMessage(messages.title)} />
    ADMIN PAGE
    <InteractiveButton>Interactive</InteractiveButton>
  </animated.main>
);

export default injectIntl(DashboardPage);
