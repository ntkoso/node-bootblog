// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';
import Helmet from 'react-helmet-async';
import { animated } from 'react-spring';

const messages = defineMessages({
  title: {
    id: 'admin.sections.title',
    defaultMessage: 'Admin - Sections',
  },
});

type Props = { intl: IntlShape };

export const SectionsPage = ({
  intl: { formatMessage },
  style,
  ...rest
}: Props): Element<typeof animated.main> => (
  <animated.main role="main" style={style}>
    <Helmet title={formatMessage(messages.title)} />
    SECTIONS
  </animated.main>
);

export default injectIntl(SectionsPage);
