// @flow
import React, { Fragment } from 'react';
import type { Element } from 'react';
import Downshift from 'downshift';
import { defineMessages, injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';
import Helmet from 'react-helmet-async';
import { rem } from 'polished';
import Component from '@reactions/component';
import styled from '@ntkoso/react-emotion';
import { mediumLarger } from '@ntkoso/btblg-style-guide/es/breakpoints';
import { animated } from 'react-spring';
import {
  default as FilteredThreads,
  AuthorFilter,
  CategoryFilter,
  ThreadFilter,
} from '../parts/FilteredThreads';

const messages = defineMessages({
  title: {
    id: 'admin.sections.title',
    defaultMessage: 'Admin - Threads',
  },
});

const AnimatedMain = styled(animated.main)({
  display: 'flex',
  flexDirection: 'column',
});

const Filters = styled.ul({
  padding: rem(12),
  display: 'flow',
  flowDirection: 'column',
  margin: `${rem(8)} 0`,

  [mediumLarger]: {
    flowDirection: 'row',
    margin: `0 ${rem(8)}`,
  },
});

type Props = {
  intl: IntlShape,
  style: { [property: string]: string | number },
};

export const ThreadsPage = ({
  intl: { formatMessage },
  style,
  ...rest
}: Props): Element<typeof AnimatedMain> => (
  <AnimatedMain role="main" style={style}>
    <Helmet title={formatMessage(messages.title)} />
    <Component
      initialState={{
        name: '',
        category: '',
        user: '',
        tags: [],
        dateRange: [],
      }}
    >
      {({ state, setState }) => (
        <Fragment>
          <Filters>
            <Downshift
              onChange={name => setState({ name })}
              render={({
                inputValue,
                getInputProps,
                getItemProps,
                selectedItem,
                highlightedIndex,
                isOpen,
              }) => (
                <div>
                  <input {...getInputProps()} />
                  {inputValue && (
                    <ThreadFilter
                      inputValue={inputValue}
                      getItemProps={getItemProps}
                      selectedItem={selectedItem}
                      highlightedIndex={highlightedIndex}
                      isOpen={isOpen}
                    />
                  )}
                </div>
              )}
            />
            <Downshift
              onChange={category => setState({ category })}
              render={({
                inputValue,
                getInputProps,
                getItemProps,
                selectedItem,
                highlightedIndex,
                isOpen,
              }) => (
                <div>
                  <input {...getInputProps()} />
                  {inputValue && (
                    <CategoryFilter
                      inputValue={inputValue}
                      getItemProps={getItemProps}
                      selectedItem={selectedItem}
                      highlightedIndex={highlightedIndex}
                      isOpen={isOpen}
                    />
                  )}
                </div>
              )}
            />
            <Downshift
              onChange={user => setState({ user })}
              render={({
                inputValue,
                getInputProps,
                getItemProps,
                selectedItem,
                highlightedIndex,
                isOpen,
              }) => (
                <div>
                  <input {...getInputProps()} />
                  {inputValue && (
                    <AuthorFilter
                      inputValue={inputValue}
                      getItemProps={getItemProps}
                      selectedItem={selectedItem}
                      highlightedIndex={highlightedIndex}
                      isOpen={isOpen}
                    />
                  )}
                </div>
              )}
            />
          </Filters>
          <FilteredThreads {...state} />
        </Fragment>
      )}
    </Component>
  </AnimatedMain>
);

export default injectIntl(ThreadsPage);
