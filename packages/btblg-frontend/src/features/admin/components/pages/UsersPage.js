// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';
import Helmet from 'react-helmet-async';
import { animated } from 'react-spring';
import { UsersList } from '../parts';

const messages = defineMessages({
  title: {
    id: 'admin.users.title',
    defaultMessage: 'Admin - Users',
  },
});

type Props = {
  intl: IntlShape,
};

export const UsersPage = ({
  intl: { formatMessage },
  style,
  ...rest
}: Props): Element<typeof animated.main> => (
  <animated.main role="main" style={style}>
    <Helmet title={formatMessage(messages.title)} />
    {/* <UsersList /> */}
  </animated.main>
);

export default injectIntl(UsersPage);
