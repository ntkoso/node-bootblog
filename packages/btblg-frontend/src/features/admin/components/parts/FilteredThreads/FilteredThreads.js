// @flow
import React from 'react';
import type { Element } from 'react';
import { rem } from 'polished';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import styled from '@ntkoso/react-emotion';
import {
  smallLarger,
  mediumLarger,
} from '@ntkoso/btblg-style-guide/es/breakpoints';

const Wrapper = styled.div({});

const ThreadList = styled.ul({
  padding: rem(12),
  '@supports (grid-area:auto)': {
    [smallLarger]: {
      display: 'grid',
      gridTemplateColumns: 'repeat(4, 1fr)',
      gridAutoRows: `minmax(${rem(150)}, auto)`,
      gridGap: rem(12),
      listStyle: 'none',
    },
    [mediumLarger]: {
      gridTemplateColumns: 'repeat(6, 1fr)',
    },
  },
});

const FILTERED_THREADS_QUERY = gql`
  query filteredThreads(
    $first: Int!
    $cursor: String
    $name: String
    $category: String
    $user: String
    $tags: [String]
    $dateRange: [String]
  ) {
    threads(
      first: $first
      after: $cursor
      name: $name
      category: $category
      user: $user
      tags: $tags
      dateRange: $dateRange
    ) {
      total
      pageInfo {
        endCursor
        hasNextPage
      }
      edges {
        node {
          id
          title
          body
          author {
            id
            username
            slug
            profile {
              avatar {
                path
                size
                deleted
              }
            }
          }
          createdAt
          updatedAt
        }
      }
    }
  }
`;

const updateQuery = (previousResult, { fetchMoreResult }) =>
  !fetchMoreResult
    ? previousResult
    : {
        ...previousResult,
        threads: {
          total: fetchMoreResult.threads.total,
          pageInfo: fetchMoreResult.threads.pageInfo,
          edges: [
            ...previousResult.threads.edges,
            ...fetchMoreResult.threads.edges,
          ],
        },
      };

type Props = {
  name?: string,
  category?: string,
  user?: string,
  tags?: Array<string>,
  dateRange?: [string, string],
  first?: number,
};

export const FilteredThreads = ({
  name,
  category,
  user,
  tags,
  dateRange,
  first = 15,
}: Props): Element<typeof Query> => (
  <Query
    query={FILTERED_THREADS_QUERY}
    variables={{
      name,
      category,
      user,
      tags,
      dateRange,
      first,
    }}
  >
    {({
      data: {
        threads: { total, pageInfo, edges },
      },
      loading,
      fetchMore,
    }) => (
      <Wrapper>
        <h3>{total}</h3>
        <ThreadList>
          {edges.map(({ node }) => <li key={node.id}>{node.title}</li>)}
        </ThreadList>
        {pageInfo.hasNextPage &&
          !loading && (
            <button
              onClick={() =>
                fetchMore({
                  variables: {
                    name,
                    category,
                    user,
                    tags,
                    dateRange,
                    first,
                    cursor: pageInfo.endCursor,
                  },
                  updateQuery,
                })
              }
            />
          )}
      </Wrapper>
    )}
  </Query>
);

export default FilteredThreads;
