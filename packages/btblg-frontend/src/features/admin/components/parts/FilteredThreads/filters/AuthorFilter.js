// @flow
import React from 'react';
import type { Element } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { pickDefaultDataProps, hideIfNeeded } from './utils';

const AUTHOR_QUERY = gql`
  query User($username: String!) {
    users(username: $username, first: 30) {
      edges {
        node {
          id
          username
          profile {
            avatar {
              path
              size
              deleted
            }
          }
        }
      }
    }
  }
`;

type AuthorNode = {
  id: string,
  username: string,
  profile: {
    avatar: {
      path: string,
      size: string,
      deleted: boolean,
    },
  },
};

type Props = {
  inputValue: string,
  isOpen?: boolean,
};

export const AuthorFilter = ({
  inputValue,
  isOpen,
}: Props): Element<typeof Query> | void | boolean =>
  isOpen && (
    <Query query={AUTHOR_QUERY} variables={{ username: inputValue }}>
      {({ data: { users: { edges } }, loading }) =>
        loading ? (
          '...Loading'
        ) : (
          <ul>
            {edges.map(({ node: { id, username } }) => (
              <li key={id}>{username}</li>
            ))}
          </ul>
        )
      }
    </Query>
  );

export default AuthorFilter;
