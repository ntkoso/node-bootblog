// @flow
import React from 'react';
import type { Element } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const CATEGORY_QUERY = gql`
  query CategoryTitle($title: String!) {
    categories(title: $title) {
      id
      title
    }
  }
`;

type CategoryNode = { id: string, title: string };

type Props = {
  inputValue: string,
  isOpen?: boolean,
};

export const CategoryFilter = ({
  inputValue,
  isOpen,
}: Props): Element<typeof Query> | void | boolean =>
  isOpen && (
    <Query query={CATEGORY_QUERY} variables={{ title: inputValue }}>
      {({ data: { categories: { edges } }, loading }) =>
        loading ? (
          'Loading...'
        ) : (
          <ul>
            {edges.map(({ node: { id, title } }) => <li key={id}>{title}</li>)}
          </ul>
        )
      }
    </Query>
  );

export default CategoryFilter;
