// @flow
import React from 'react';
import type { Element } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { pickDefaultDataProps, hideIfNeeded } from './utils';

const THREAD_QUERY = gql`
  query ThreadTitle($title: String!) {
    threads(title: $title) {
      id
      title
    }
  }
`;

type ThreadNode = { id: string, title: string };

type Props = {
  inputValue: string,
  isOpen?: boolean,
};

export const ThreadFilter = ({
  inputValue,
  isOpen,
}: Props): Element<typeof Query> | void | boolean =>
  isOpen && (
    <Query query={THREAD_QUERY} variables={{ title: inputValue }}>
      {({ data: { threads: { edges } }, loading }) =>
        loading ? (
          '...Loading'
        ) : (
          <ul>
            {edges.map(({ node: { id, title } }) => <li key={id}>{title}</li>)}
          </ul>
        )
      }
    </Query>
  );

export default ThreadFilter;
