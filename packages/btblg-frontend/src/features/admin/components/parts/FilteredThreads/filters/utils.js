// @flow
import React, { Fragment } from 'react';
import type { ComponentType, Element } from 'react';

export type Edges = Array<{ node: any }>;

type Props<P> = {
  isOpen: boolean,
  loading: boolean,
  ...P,
};

export const hideIfNeeded = <P>({ isOpen, loading, ...props }: Props<P>) => (
  BaseComponent: ComponentType<P>,
) => {
  if (!isOpen) {
    return null;
  }

  if (loading) {
    return <Fragment>Loading...</Fragment>;
  }

  return <BaseComponent {...props} />;
};

export const pickDefaultDataProps = ({
  data: { edges, loading },
}: {
  data: { edges: Array<{ node: any }>, loading: boolean },
}) => ({ edges, loading });
