// @flow
import FilteredThreads from './FilteredThreads';

export { default as AuthorFilter } from './filters/AuthorFilter';
export { default as CategoryFilter } from './filters/CategoryFilter';
export { default as ThreadFilter } from './filters/ThreadFilter';
export default FilteredThreads;
