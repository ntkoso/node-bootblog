// @flow
import React from 'react';
import type { Element } from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import type { LocationShape } from 'react-router-dom';
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';
import type { IntlMessage } from 'common/types';

const ItemLink = styled(Link)({
  display: 'block',
  width: '100%',
});

const LinkLabel = styled.span({
  fontSize: rem(14),
  textAlign: 'center',
  wordWrap: 'break-word',
});

const IconWrapper = styled.div({ textAlign: 'center' });

export type Props = {
  to: string | LocationShape,
  message: IntlMessage,
  icon: Element<*>,
  onMouseOver?: (event: SyntheticMouseEvent) => void,
};

const Li = styled.li({
  height: '100%',
});

export const Item = ({
  to,
  message,
  icon,
  onMouseOver,
}: Props): Element<typeof Li> => (
  <Li role="presentation">
    <ItemLink role="menuitem" to={to} onMouseOver={onMouseOver}>
      <IconWrapper>{icon}</IconWrapper>
      <LinkLabel>
        <FormattedMessage {...message} />
      </LinkLabel>
    </ItemLink>
  </Li>
);

export default Item;
