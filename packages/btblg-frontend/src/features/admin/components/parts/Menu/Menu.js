// @flow
import React, { Fragment } from 'react';
import type { Element, ChildrenArray } from 'react';
import { withRouter } from 'react-router-dom';
import { rem } from 'polished';
import Component from '@reactions/component';
import styled from '@ntkoso/react-emotion';
import SwapVerticalIcon from '@ntkoso/btblg-icon-set/swap-vertical.svg';
import PrimarySection from './PrimarySection';
import SecondarySection from './SecondarySection';

const Wrapper = styled.div({
  display: 'flex',
  height: rem(48),
});

const Navigation = styled.nav({
  flex: '1 1 100%',
});

const Sections = styled.div({
  position: 'relative',
  perspective: '1000px',
  width: '100%',
  height: '100%',
});

const FlipButton = styled.button({
  flex: '0 0 auto',
  float: 'right',
  width: rem(48),
  height: rem(48),
  border: 'none',
});

export type Props = {
  children: ChildrenArray<
    Element<typeof PrimarySection | typeof SecondarySection>,
  >,
  pathname: string,
};

export const Menu = ({
  children,
  pathname,
  ...rest
}: Props): Element<typeof Wrapper> => (
  <Wrapper role="presentation" {...rest}>
    <Component initialState={{ on: false }}>
      {({ state: { on }, setState }) => (
        <Fragment>
          <Navigation role="navigation">
            <Sections role="presentation">
              {React.Children.map(children, child =>
                React.cloneElement(child, {
                  pathname,
                  isFlipped: on,
                }),
              )}
            </Sections>
          </Navigation>
          <FlipButton
            type="button"
            aria-label="Flip Menu"
            onClick={() => setState(state => ({ ...state, on: !state.on }))}
          >
            <SwapVerticalIcon />
          </FlipButton>
        </Fragment>
      )}
    </Component>
  </Wrapper>
);

export default withRouter(Menu);
