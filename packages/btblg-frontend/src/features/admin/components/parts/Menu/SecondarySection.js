// @flow
import React from 'react';
import type { Element, ChildrenArray } from 'react';
import styled from '@ntkoso/react-emotion';
import { containsActiveChild, SectionBase } from './internal';
import Item from './Item';

const Wrapper = styled(SectionBase)(
  { transform: 'rotateX(-180deg)' },
  ({ isFlipped }) =>
    isFlipped ? { zIndex: 1000, transform: 'rotateX(0)' } : {},
);

type Props = {
  children: ChildrenArray<Element<typeof Item>>,
  pathname: string,
  isFlipped?: ?boolean,
};

const SecondarySection = ({
  pathname,
  isFlipped,
  children,
}: Props): Element<typeof Wrapper> => (
  <Wrapper
    isFlipped={
      typeof isFlipped === 'undefined'
        ? containsActiveChild(pathname, children)
        : isFlipped
    }
  >
    {children}
  </Wrapper>
);

export default SecondarySection;
