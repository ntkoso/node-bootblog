// @flow
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';
import { withProps } from 'recompose';

const SectionBase = withProps({ role: 'menu' })(
  styled.ul({
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    height: rem(48),
    backfaceVisibility: 'hidden',
    listStyle: 'none',
    transition: 'all 0.3s ease-in-out',
    transformStyle: 'preserve-3d',

    '& > *': {
      flex: '1 0 auto',
    },
  }),
);

export default SectionBase;
