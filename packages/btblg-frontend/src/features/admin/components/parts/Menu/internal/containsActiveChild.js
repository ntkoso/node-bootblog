// @flow
import React from 'react';
import type { Element, ComponentType, ChildrenArray } from 'react';

type ContainsActiveChild = (
  pathname: string,
  children: ChildrenArray<Element<ComponentType<{ to: string }>>>,
) => boolean;
const containsActiveChild: ContainsActiveChild = (pathname, children) => {
  let result = false;

  React.Children.forEach(children, child => {
    const { to } = child.props;

    if (pathname === to) {
      result = true;
    }
  });

  return result;
};

export default containsActiveChild;
