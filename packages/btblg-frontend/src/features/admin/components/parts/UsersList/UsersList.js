// @flow
import React from 'react';
import type { Element } from 'react';
import { Query } from 'react-apollo';
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';
import gql from 'graphql-tag';
import UsersListItem from './UsersListItem';

const Wrapper = styled.ul({
  display: 'flex',
  flexDirection: 'column',
  flexWrap: 'nowrap',
  listStyle: 'none',
  paddingTop: rem(8),
  paddingBottom: rem(8),
});

const USER_LIST_QUERY = gql`
  query usersList($cursor: String) {
    users(first: 15, after: $cursor, orderBy: "username asc") {
      total
      pageInfo {
        endCursor
        hasNextPage
      }
      edges {
        node {
          id
          username
          email
          profile {
            avatar {
              path
              size
              deleted
            }
            createdAt
            updatedAt
          }
        }
      }
    }
  }
`;

const UsersList = (): Element<typeof Wrapper> => (
  <Wrapper>
    <Query query={USER_LIST_QUERY}>
      {({ data: { loading, users: { edges }, fetchMore } }) =>
        !loading && Array.isArray(edges)
          ? edges.map(({ node }) => <UsersListItem key={node.id} user={node} />)
          : 'Not a single user is registered on our site'
      }
    </Query>
  </Wrapper>
);

export default UsersList;
