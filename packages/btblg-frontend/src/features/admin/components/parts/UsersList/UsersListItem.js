// @flow
import get from 'get-value';
import React from 'react';
import type { Element } from 'react';
import { Link, withRouter } from 'react-router-dom';
import type { Match } from 'react-router-dom';
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';
import { Avatar } from '@ntkoso/btblg-style-guide/es/components';
import {
  Content,
  H3,
} from '@ntkoso/btblg-style-guide/es/components/typography';

const Wrapper = styled.li({ flex: '0 0 auto', height: rem(56) });

const ListLink = styled(Link)({
  display: 'flex',
  flexDirection: 'row',
  textDecoration: 'none',
});

const AvatarWrapper = styled.div({
  display: 'flex',
  justifyContent: 'center',
  alignContent: 'center',
  flex: '0 0 auto',
  padding: rem(16),
});

const UsernameWrapper = styled.div({
  display: 'flex',
  flex: '1 1 auto',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'baseline',
  paddingRight: rem(16),
});

const Username = styled(H3)(({ theme: { color: { text } } }) => ({
  color: text,
}));

type Props = {
  loading: boolean,
  user: {
    id: string,
    username: string,
    email: string,
    profile: {
      avatar: {
        url: string,
      },
    },
  },
  match: Match,
};

const UsersListItem = ({
  loading,
  user,
  match: { url },
}: Props): Element<typeof Wrapper> => (
  <Wrapper>
    <ListLink to={`${url}/${user.id}`}>
      <AvatarWrapper>
        <Avatar
          ready={!loading}
          src={get(user, ['profile', 'avatar', 'url'])}
        />
      </AvatarWrapper>
      <UsernameWrapper>
        <Username>{user.username}</Username>
      </UsernameWrapper>
    </ListLink>
  </Wrapper>
);

export default withRouter(UsersListItem);
