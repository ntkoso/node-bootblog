// @flow
import React from 'react';
import type { Element } from 'react';
import { Route, Switch } from 'react-router-dom';
import type { Match, Location } from 'react-router-dom';
import { defineMessages } from 'react-intl';
import Loadable from 'react-loadable';
import { rem } from 'polished';
import styled from '@ntkoso/react-emotion';
import { smallLarger } from '@ntkoso/btblg-style-guide/es/breakpoints';
import DashboardIcon from '@ntkoso/btblg-icon-set/view-dashboard.svg';
import AccountGroupIcon from '@ntkoso/btblg-icon-set/account-group.svg';
import NewspaperIcon from '@ntkoso/btblg-icon-set/newspaper.svg';
import ListIcon from '@ntkoso/btblg-icon-set/view-list.svg';
import ModuleIcon from '@ntkoso/btblg-icon-set/view-module.svg';
import { Loading } from 'common/components';
import {
  Menu as AdminMenu,
  PrimarySection,
  SecondarySection,
  Item as MenuItem,
} from '../parts/Menu';

const messages = defineMessages({
  dashboard: {
    id: 'admin.dashboard',
    defaultMessage: 'Dashboard',
  },
  categories: {
    id: 'admin.categories',
    defaultMessage: 'Categories',
  },
  sections: {
    id: 'admin.sections',
    defaultMessage: 'Sections',
  },
  threads: {
    id: 'admin.threads',
    defaultMessage: 'Threads',
  },
  users: {
    id: 'admin.users',
    defaultMessage: 'Users',
  },
});

const LoadableDashboardPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "admin-DashboardPage" */ '../pages/DashboardPage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/DashboardPage'],
  webpack: () => [require.resolveWeak('../pages/DashboardPage')],
});

const LoadableUsersPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "admin-UsersPage" */ '../pages/UsersPage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/UsersPage'],
  webpack: () => [require.resolveWeak('../pages/UsersPage')],
});

const LoadableSectionsPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "admin-SectionsPage" */ '../pages/SectionsPage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/SectionsPage'],
  webpack: () => [require.resolveWeak('../pages/SectionsPage')],
});

const LoadableCategoriesPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "admin-CategoriesPage" */ '../pages/CategoriesPage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/CategoriesPage'],
  webpack: () => [require.resolveWeak('../pages/CategoriesPage')],
});

const LoadableThreadsPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "admin-ThreadsPage" */ '../pages/ThreadsPage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/ThreadsPage'],
  webpack: () => [require.resolveWeak('../pages/ThreadsPage')],
});

const preload = Component => () =>
  typeof Component.preload === 'function' && Component.preload();

const preloadLoadableCategoriesPage = preload(LoadableCategoriesPage);
const preloadLoadableDashboardPage = preload(LoadableDashboardPage);
const preloadLoadableUsersPage = preload(LoadableUsersPage);
const preloadLoadableSectionsPage = preload(LoadableSectionsPage);
const preloadLoadableThreadsPage = preload(LoadableThreadsPage);

const Content = styled.div({
  '@supports (grid-area: auto)': {
    display: 'grid',
    height: '100vh',
    gridTemplateColumns: `${rem(48)} auto`,
    gridTemplateRows: 'auto',
    gridTemplateAreas: `
      "menu main"
    `,

    [smallLarger]: {
      gridTemplateColumns: 'auto',
      gridTemplateRows: `${rem(48)} auto`,
      gridTemplateAreas: `
        "menu"
        "main"
      `,
    },
  },
});

const mainGridArea = { gridArea: 'main' };

type Props = { gridArea: string, match: Match, location: Location };

export const AdminScreen = ({
  gridArea,
  match: { url, path },
  location,
}: Props): Element<typeof Content> => (
  <Content style={{ gridArea }}>
    <AdminMenu style={{ gridArea: 'menu' }} pathname={location.pathname}>
      <PrimarySection>
        <MenuItem
          to={url}
          icon={<DashboardIcon />}
          message={messages.dashboard}
          onMouseOver={preloadLoadableDashboardPage}
          onFocus={preloadLoadableDashboardPage}
        />
        <MenuItem
          to={`${url}/users`}
          icon={<AccountGroupIcon />}
          message={messages.users}
          onMouseOver={preloadLoadableUsersPage}
          onFocus={preloadLoadableUsersPage}
        />
      </PrimarySection>
      <SecondarySection>
        <MenuItem
          to={`${url}/sections`}
          icon={<ModuleIcon />}
          message={messages.sections}
          onMouseOver={preloadLoadableSectionsPage}
          onFocus={preloadLoadableSectionsPage}
        />
        <MenuItem
          to={`${url}/categories`}
          icon={<ListIcon />}
          message={messages.categories}
          onMouseOver={preloadLoadableCategoriesPage}
          onFocus={preloadLoadableCategoriesPage}
        />
        <MenuItem
          to={`${url}/threads`}
          icon={<NewspaperIcon />}
          message={messages.threads}
          onMouseOver={preloadLoadableThreadsPage}
          onFocus={preloadLoadableThreadsPage}
        />
      </SecondarySection>
    </AdminMenu>
    <Switch location={location}>
      <Route
        exact
        path={path}
        render={props => (
          <LoadableDashboardPage {...props} style={mainGridArea} />
        )}
      />
      <Route
        path={`${path}/users`}
        render={props => <LoadableUsersPage {...props} style={mainGridArea} />}
      />
      <Route
        path={`${path}/sections`}
        render={props => (
          <LoadableSectionsPage {...props} style={mainGridArea} />
        )}
      />
      <Route
        path={`${path}/categories`}
        render={props => (
          <LoadableCategoriesPage {...props} style={mainGridArea} />
        )}
      />
      <Route
        path={`${path}/threads`}
        render={props => (
          <LoadableThreadsPage {...props} style={mainGridArea} />
        )}
      />
    </Switch>
  </Content>
);

export default AdminScreen;
