// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import type { IntlShape } from 'react-intl';
import { getIntlMessageById } from '@ntkoso/utils';
import EmailIcon from '@ntkoso/btblg-icon-set/email.svg';
import { InteractiveInput } from 'features/ui/components';

const messages = defineMessages({
  label: {
    id: 'field.email.label',
    defaultMessage: 'Email',
  },
  required: {
    id: 'validations.email.required',
    defaultMessage: 'Email address is required',
  },
  invalid: {
    id: 'validations.email.invalid',
    defaultMessage: 'Email address is invalid',
  },
});

const renderEmailError = ({
  path,
  message,
}: {
  path: string,
  message: string,
}): Element<typeof FormattedMessage> => (
  <FormattedMessage
    {...getIntlMessageById(message, messages)}
    values={{ path }}
  />
);

type Props = {
  intl: IntlShape,
};

export const EmailField = ({
  field,
  form,
  intl,
}: Props): Element<typeof InteractiveInput> => (
  <InteractiveInput
    type="email"
    field={field}
    form={form}
    icon={<EmailIcon />}
    label={intl.formatMessage(messages.label)}
    renderError={renderEmailError}
    required
  />
);

export default injectIntl(EmailField);
