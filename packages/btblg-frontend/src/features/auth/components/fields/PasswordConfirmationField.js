// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import type { IntlShape } from 'react-intl';
import { getIntlMessageById } from '@ntkoso/utils';
import LockIcon from '@ntkoso/btblg-icon-set/lock.svg';
import { InteractiveInput } from 'features/ui/components';

const messages = defineMessages({
  label: {
    id: 'field.passwordConfirmation.label',
    defaultMessage: 'Password confirmation',
  },
  required: {
    id: 'validation.password.required',
    default: 'Password confirmation is required',
  },
});

const renderPasswordConfirmationError = ({
  path,
  message,
}: {
  path: string,
  message: string,
}): Element<typeof FormattedMessage> => (
  <FormattedMessage
    {...getIntlMessageById(message, messages)}
    values={{ path }}
  />
);

type Props = {
  intl: IntlShape,
};

export const PasswordConfirmationField = ({
  field,
  form,
  intl,
}: Props): Element<typeof InteractiveInput> => (
  <InteractiveInput
    type="password"
    field={field}
    form={form}
    icon={<LockIcon />}
    label={intl.formatMessage(messages.label)}
    renderError={renderPasswordConfirmationError}
    required
  />
);

export default injectIntl(PasswordConfirmationField);
