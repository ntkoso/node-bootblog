// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import type { IntlShape } from 'react-intl';
import { getIntlMessageById } from '@ntkoso/utils';
import LockIcon from '@ntkoso/btblg-icon-set/lock.svg';
import { InteractiveInput } from 'features/ui/components';

const messages = defineMessages({
  label: {
    id: 'field.password.label',
    defaultMessage: 'Password',
  },
  required: {
    id: 'validation.password.required',
    defaultMessage: 'Password is required',
  },
  invalid: {
    id: 'validaiton.password.invalid',
    defaultMessage: "Password is invalid, check if it's typed correctly",
  },
  long: {
    id: 'validation.password.long',
    defaultMessage: 'Password should be longer than 8 characters',
  },
  uppercaseLetters: {
    id: 'validation.password.uppercase_letters',
    defaultMessage: 'Password should have at least 1 uppercase letter',
  },
  lowercaseLetters: {
    id: 'validation.password.lowercase_letters',
    defaultMessage: 'Password should have at least 1 lowercase letter',
  },
  numbers: {
    id: 'validation.password.numbers',
    defaultMessage: 'Password should have at least 1 number',
  },
});

const renderPasswordError = ({
  path,
  message,
}: {
  path: string,
  message: string,
}): Element<typeof FormattedMessage> => (
  <FormattedMessage
    {...getIntlMessageById(message, messages)}
    values={{ path }}
  />
);

type Props = { intl: IntlShape };

export const PasswordField = ({
  field,
  form,
  intl,
}: Props): Element<typeof InteractiveInput> => (
  <InteractiveInput
    type="password"
    field={field}
    form={form}
    icon={<LockIcon />}
    label={intl.formatMessage(messages.label)}
    renderError={renderPasswordError}
    required
  />
);

export default injectIntl(PasswordField);
