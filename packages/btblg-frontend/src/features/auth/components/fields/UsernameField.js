// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import type { IntlShape } from 'react-intl';
import { getIntlMessageById } from '@ntkoso/utils';
import UserIcon from '@ntkoso/btblg-icon-set/account.svg';
import { InteractiveInput } from 'features/ui/components';

const messages = defineMessages({
  label: {
    id: 'field.username.label',
    defaultMessage: 'Username',
  },
  required: {
    id: 'validation.username.required',
    defaultMessage: 'Username is required',
  },
});

const renderUsernameError = ({
  path,
  message,
}: {
  path: string,
  message: string,
}): Element<typeof FormattedMessage> => (
  <FormattedMessage
    {...getIntlMessageById(message, messages)}
    values={{ path }}
  />
);

type Props = {
  intl: IntlShape,
};

export const UsernameField = ({
  field,
  form,
  intl,
}: Props): Element<typeof InteractiveInput> => (
  <InteractiveInput
    type="text"
    field={field}
    form={form}
    icon={<UserIcon />}
    label={intl.formatMessage(messages.label)}
    renderError={renderUsernameError}
    required
  />
);

export default injectIntl(UsernameField);
