// @flow
import get from 'get-value';
import React from 'react';
import type { Element } from 'react';
import { Mutation } from 'react-apollo';
import { Formik, Field } from 'formik';
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';
import { shadow } from '@ntkoso/btblg-style-guide/es/mixins';
import { medium } from '@ntkoso/btblg-style-guide/es/breakpoints';
import { signIn as signInValidationSchema } from '@ntkoso/btblg-validations/es/mutations';
import { srvValidationToFormErrors } from '@ntkoso/utils';
import log from 'log';
import { signIn as SIGN_IN_MUTATION } from 'features/auth/mutations';
import { EmailField, PasswordField } from '../../fields';
import SignInSubmitButton from './parts/SignInSubmitButton';

const Form = styled.form(
  {
    padding: rem(12),
    borderRadius: rem(4),
    ...shadow(2),

    [medium]: {
      padding: rem(24),
    },

    '& > *': {
      margin: `${rem(8)} auto`,
    },
  },
  ({ gridArea }) => ({ gridArea }),
);

type Props = {
  gridArea?: string,
};

export const SignInForm = ({ gridArea }: Props): Element<typeof Formik> => (
  <Mutation mutation={SIGN_IN_MUTATION}>
    {mutate => (
      <Formik
        initialValues={{
          email: '',
          password: '',
        }}
        validationSchema={signInValidationSchema}
        onSubmit={(input, { setSubmitting, setErrors }) =>
          mutate({ variables: { input } })
            .then(result => {
              setSubmitting(false);
              const errors = get(result, ['data', 'signIn', 'errors']);

              if (Array.isArray(errors) && errors.length > 0) {
                setErrors(srvValidationToFormErrors(errors));
              }
            })
            .catch(error => {
              setSubmitting(false);
              log.error(error);
            })
        }
        render={({ isSubmitting, handleSubmit }) => (
          <Form onSubmit={handleSubmit} gridArea={gridArea}>
            <Field name="email" component={EmailField} />
            <Field name="password" component={PasswordField} />
            <SignInSubmitButton disabled={isSubmitting} />
          </Form>
        )}
      />
    )}
  </Mutation>
);

export default SignInForm;
