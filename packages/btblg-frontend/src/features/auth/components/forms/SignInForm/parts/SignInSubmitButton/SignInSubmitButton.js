// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import LoginIcon from '@ntkoso/btblg-icon-set/login.svg';
import { InteractivePrimaryButton } from 'features/ui/components/InteractiveButton';

const messages = defineMessages({
  button: {
    id: 'auth.signIn.submitButton',
    description: 'SignIn submit button name',
    defaultMessage: 'SIGN IN',
  },
});

const SignInSubmitButton = <P>(
  props: P,
): Element<typeof InteractivePrimaryButton> => (
  <InteractivePrimaryButton
    type="submit"
    icon={<LoginIcon />}
    accent
    {...props}
  >
    <FormattedMessage {...messages.button} />
  </InteractivePrimaryButton>
);

export default SignInSubmitButton;
