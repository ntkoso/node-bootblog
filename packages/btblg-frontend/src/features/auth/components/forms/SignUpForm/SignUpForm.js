// @flow
import get from 'get-value';
import React from 'react';
import type { Element } from 'react';
import { Formik, Field } from 'formik';
import { Mutation } from 'react-apollo';
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';
import log from 'log';
import { shadow } from '@ntkoso/btblg-style-guide/es/mixins';
import { medium } from '@ntkoso/btblg-style-guide/es/breakpoints';
import { signUp as signUpValidationSchema } from '@ntkoso/btblg-validations/es/mutations';
import { srvValidationToFormErrors } from '@ntkoso/utils';
import { signUp as SIGN_UP_MUTATION } from 'features/auth/mutations';
import { RecaptchaField } from 'common/components/form/fields';
import {
  EmailField,
  UsernameField,
  PasswordField,
  PasswordConfirmationField,
} from '../../fields';
import SignUpSubmitButton from './parts/SignUpSubmitButton';

const Form = styled.form(
  {
    padding: rem(12),
    borderRadius: rem(4),
    ...shadow(2),

    [medium]: {
      padding: rem(24),
    },

    '& > *': {
      margin: `${rem(8)} auto`,
    },
  },
  ({ gridArea }) => ({ gridArea }),
);

type Props = {
  gridArea?: string,
};

const initialFormikState = {
  email: '',
  username: '',
  password: '',
  passwordConfirmation: '',
  recaptcha: null,
};

export const SignUpForm = ({ gridArea }: Props): Element<typeof Formik> => (
  <Mutation mutation={SIGN_UP_MUTATION}>
    {mutate => (
      <Formik
        initialValues={initialFormikState}
        validationSchema={signUpValidationSchema}
        onSubmit={(input, { setSubmitting, setErrors }) =>
          mutate({ variables: { input } })
            .then(result => {
              setSubmitting(false);
              const errors = get(result, ['data', 'signUp', 'errors']);

              if (Array.isArray(errors) && errors.length > 0) {
                setErrors(srvValidationToFormErrors(errors));
              }
            })
            .catch(error => {
              setSubmitting(false);
              log.error(error);
            })
        }
        render={({ isSubmitting, handleSubmit }) => (
          <Form onSubmit={handleSubmit} gridArea={gridArea}>
            <Field name="email" component={EmailField} />
            <Field name="username" component={UsernameField} />
            <Field name="password" component={PasswordField} />
            <Field
              name="passwordConfirmation"
              component={PasswordConfirmationField}
            />
            <Field name="recaptcha" component={RecaptchaField} />
            <SignUpSubmitButton disabled={isSubmitting} />
          </Form>
        )}
      />
    )}
  </Mutation>
);

export default SignUpForm;
