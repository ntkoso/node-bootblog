// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import LoginIcon from '@ntkoso/btblg-icon-set/login.svg';
import { InteractivePrimaryButton } from 'features/ui/components';

const messages = defineMessages({
  button: {
    id: 'auth.signUp.submitButton',
    description: 'SignUp submit button name',
    defaultMessage: 'SIGN UP',
  },
});

type Props = { invalid?: boolean };

export const SignUpSubmitButton = ({
  invalid,
}: Props): Element<typeof InteractivePrimaryButton> => (
  <InteractivePrimaryButton
    type="submit"
    accent
    disabled={invalid}
    icon={<LoginIcon />}
  >
    <FormattedMessage {...messages.button} />
  </InteractivePrimaryButton>
);

export default SignUpSubmitButton;
