// @flow
import React from 'react';
import type { Element } from 'react';

type Props = { email: string };

export const EmailConfirmationPage = ({ email }: Props): Element<'div'> => (
  <div>{email} verification</div>
);

export default EmailConfirmationPage;
