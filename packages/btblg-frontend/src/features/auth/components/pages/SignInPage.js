// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';
import Helmet from 'react-helmet-async';
import styled from '@ntkoso/react-emotion';
import { smallLarger } from '@ntkoso/btblg-style-guide/es/breakpoints';
import { SignInForm } from '../forms';

const messages = defineMessages({
  title: {
    id: 'sign-in.title',
    description: 'SignIn page title',
    defaultMessage: 'SignIn',
  },
});

const Main = styled.main(
  {
    '@supports (grid-area: auto)': {
      [smallLarger]: {
        display: 'grid',
        gridTemplateColumns: `1fr minmax(40%, 2fr) 1fr`,
        gridTemplateRows: `1fr minmax(90%, auto) 1fr`,
        gridTemplateAreas: `
            ". . ."
            ". form ."
            ". . ."
          `,
      },
    },
  },
  ({ gridArea }) => ({ gridArea }),
);

type Props = { gridArea?: string, intl: IntlShape };

export const SignInPage = ({ gridArea, intl }: Props): Element<Main> => (
  <Main role="main" gridArea={gridArea}>
    <Helmet title={intl.formatMessage(messages.title)} />
    <SignInForm gridArea="form" />
  </Main>
);

export default injectIntl(SignInPage);
