// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

const messages = defineMessages({
  registeredAs: {
    id: 'auth.sign_up.registered_as',
    description: 'Successfully registered as username',
    defaultMessage: 'You successfully registered as {username}.',
  },
  confirmationRequestSentAt: {
    id: 'auth.sign_up.email_confirmation_request_sent_at',
    description: "Email confirmation request sent to user's email",
    defaultMessage: 'Email address verification request was sent to {email}.',
  },
});

type Props = { username: string, email: string };

export const SignUpSuccessPage = ({
  username,
  email,
}: Props): Element<'div'> => (
  <div>
    <FormattedMessage
      {...messages.registeredAs}
      values={{
        username: <b>{username}</b>,
      }}
    />
    <FormattedMessage
      {...messages.confirmationRequestSentAt}
      values={{
        email: <b>{email}</b>,
      }}
    />
  </div>
);

export default SignUpSuccessPage;
