// @flow
import React from 'react';
import type { Element } from 'react';
import { Route, Switch } from 'react-router-dom';
import type { Match } from 'react-router-dom';
import Loadable from 'react-loadable';
import { Loading } from 'common/components';

const LoadableSignInPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "auth-SignInPage" */ '../pages/SignInPage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/SignInPage'],
  webpack: () => [require.resolveWeak('../pages/SignInPage')],
});

const LoadableSignUpPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "auth-SignUpPage" */ '../pages/SignUpPage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/SignUpPage'],
  webpack: () => [require.resolveWeak('../pages/SignUpPage')],
});

const LoadableSignUpSuccessPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "auth-SignUpSuccessPage" */ '../pages/SignUpSuccessPage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/SignUpSuccessPage'],
  webpack: () => [require.resolveWeak('../pages/SignUpSuccessPage')],
});

const LoadableEmailConfirmationPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "auth-EmailConfirmationPage" */ '../pages/EmailConfirmationPage').then(
      module => module.default,
    ),
  loading: Loading,
  modules: ['../pages/EmailConfirmationPage'],
  webpack: () => [require.resolveWeak('../pages/EmailConfirmationPage')],
});

type Props = { gridArea?: string, match: Match };

export const AuthScreen = ({
  gridArea,
  match: { url },
}: Props): Element<typeof Switch> => (
  <Switch>
    <Route
      exact
      path={url}
      render={props => <LoadableSignInPage {...props} gridArea={gridArea} />}
    />
    <Route
      path={`${url}/signup`}
      render={props => <LoadableSignUpPage {...props} gridArea={gridArea} />}
    />
    <Route
      path={`${url}/signup-success`}
      render={props => (
        <LoadableSignUpSuccessPage {...props} gridArea={gridArea} />
      )}
    />
    <Route
      path={`${url}/email-confirmation`}
      render={props => (
        <LoadableEmailConfirmationPage {...props} gridArea={gridArea} />
      )}
    />
  </Switch>
);

export default AuthScreen;
