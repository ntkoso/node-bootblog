// @flow
import gql from 'graphql-tag';

const UsersAuthInfoFragment = gql`
  fragment UserAuthInfo on User {
    id
    email
    username
    usernameCanonical
  }
`;

export default UsersAuthInfoFragment;
