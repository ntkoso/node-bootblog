// @flow
import gql from 'graphql-tag';

const refreshTokenMutation = gql`
  mutation RefreshToken($input: RefreshTokenInput!) {
    refreshToken(input: $input) {
      clientMutationId
    }
  }
`;

export default refreshTokenMutation;
