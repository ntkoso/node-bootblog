// @flow
import gql from 'graphql-tag';
import UsersAuthInfo from '../fragments/UserAuthInfo';

const signInMutation = gql`
  mutation SignIn($input: SignInInput!) {
    signIn(input: $input) {
      user {
        ...UserAuthInfo
      }
      accessTokenTTL
      accessTokenRefreshTTL
      errors {
        path
        message
      }
    }
  }

  ${UsersAuthInfo}
`;

export default signInMutation;
