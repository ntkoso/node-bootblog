// @flow
import gql from 'graphql-tag';

const signOutMutation = gql`
  mutation SignOut($input: SignOutInput!) {
    signOut(input: $input) {
      clientMutationId
    }
  }
`;

export default signOutMutation;
