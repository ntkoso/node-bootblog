// @flow
import gql from 'graphql-tag';
import UsersAuthInfo from '../fragments/UserAuthInfo';

const signUpMutation = gql`
  mutation SignUp($input: SignUpInput!) {
    signUp(input: $input) {
      user {
        ...UserAuthInfo
      }
      errors {
        path
        message
      }
    }
  }

  ${UsersAuthInfo}
`;

export default signUpMutation;
