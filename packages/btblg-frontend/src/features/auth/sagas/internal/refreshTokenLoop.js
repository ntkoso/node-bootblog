import { call, getContext } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import getXsrfToken from 'utils/token/getXsrfToken';
import refreshTokenQuery from '../../queries/refreshToken';

const refreshTokenLoop = function* refreshTokenLoop(
  xsrfToken,
  TTL,
  skipFirstDelay = false,
) {
  const client = yield getContext('client');

  let delayDisabled = skipFirstDelay;

  while (true) {
    // eslint-disable-line
    if (!delayDisabled) {
      yield call(delay, TTL);
    }

    try {
      yield call(
        client.mutate({
          query: refreshTokenQuery,
        }),
      );
    } catch (error) {
      return;
    }

    xsrfToken = yield call(getXsrfToken); // eslint-disable-line no-param-reassign
    delayDisabled = false; // activate delay
  }
};

export default refreshTokenLoop;
