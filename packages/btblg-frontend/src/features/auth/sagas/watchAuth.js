import uuidV4 from 'uuid/v4';
import get from 'get-value';
import { call, getContext, take, race } from 'redux-saga/effects';
import { SubmissionError } from 'redux-form';
import { errorsToFormErrors } from 'redux-form-helpers';
import getXsrfToken from 'utils/token/getXsrfToken';
import { signIn, signOut } from '../actions';
import signInQuery from '../queries/signIn';
import signOutQuery from '../queries/signOut';
import refreshTokenLoop from './internal/refreshTokenLoop';

const watchAuth = function* watchAuth() {
  const client = yield getContext('client');

  let xsrfToken = yield call(getXsrfToken);
  let TTL = 3600000; // 1h default
  const isInitiallySignedIn = Boolean(xsrfToken);

  while (true) {
    // eslint-disable-line
    if (!xsrfToken) {
      const { payload } = yield take(signIn);
      const { values, resolve, reject } = payload;
      let response;

      try {
        response = yield call(
          client.mutate({
            query: signInQuery,
            variables: { email: values.email, password: values.password },
          }),
        );
      } catch (error) {
        reject(error);
        continue;
      }

      if (get(response, ['data', 'signIn', 'errors', 'length']) > 0) {
        reject(
          new SubmissionError(errorsToFormErrors(response.data.signIn.errors)),
        );
        continue;
      } else {
        xsrfToken = yield call(getXsrfToken);
        TTL = response.data.signIn.accessTokenRefreshTTL;
        resolve(response);
      }
    }

    const { signOut: signOutAction } = yield race({
      signOut: take(signOut),
      refresh: call(refreshTokenLoop, xsrfToken, TTL, isInitiallySignedIn),
    });

    if (signOutAction) {
      yield call(
        client.mutate({
          query: signOutQuery,
          variables: { input: { clientMutationId: uuidV4() } },
        }),
      );

      xsrfToken = yield call(getXsrfToken);
    }
  }
};

export default watchAuth;
