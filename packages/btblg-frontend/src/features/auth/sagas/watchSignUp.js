// @flow
import get from 'get-value';
import compose from 'compose-function';
import { call, getContext, take } from 'redux-saga/effects';
import {
  hasErrors,
  toSubmissionError,
} from 'redux-form-helpers/responseHelpers';
import { signUp } from '../actions';
import signUpQuery from '../queries/signUp';

const getResponseSubtree = response => get(response, ['data', 'signUp']);
const hasSignUpErrors = compose(hasErrors, getResponseSubtree);
const toSignUpSubmissionError = compose(toSubmissionError, getResponseSubtree);
/*
const queryParams = compose(
  pick(['username', 'email']),
  prop('user'),
  getResponseSubtree
);
*/
const watchSignUp = function* watchSignUp() {
  const client = yield getContext('client');

  while (true) {
    // eslint-disable-line
    const { payload } = yield take(signUp);
    const { values, resolve, reject } = payload;
    let response;

    try {
      response = yield call(
        client.mutate({
          query: signUpQuery,
          variables: values,
        }),
      );
    } catch (error) {
      reject(error);
      return;
    }

    if (hasSignUpErrors(response)) {
      reject(toSignUpSubmissionError(response));
      return;
    }

    //  const action = yield put(push({ pathname: '/sign-up-success', query: queryParams(response) }));

    resolve(response);
  }
};

export default watchSignUp;
