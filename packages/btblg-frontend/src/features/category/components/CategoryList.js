// @flow
import React from 'react';
import type { Element } from 'react';
import CategoryListItem from './CategoryListItem';

type Props = { ids: Array<string> };

export const CategoryList = ({ ids }: Props): Element<'ul'> => (
  <ul>{ids.map(id => <CategoryListItem id={id} key={id} />)}</ul>
);

export default CategoryList;
