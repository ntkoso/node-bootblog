// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';

const messages = defineMessages({
  threadsCount: {
    id: 'category.threadsCount',
    description: 'Nubmer of threads that category contains',
    defaultMessage: `{threadsCount, plural,
      =0 {no threads}
      one {# thread}
      other {# threads}
    }`,
  },
});

const findHeight = ({ lines }: { lines: ?number }): string => {
  switch (lines) {
    case 1:
      return rem(56);
    case 2:
      return rem(72);
    case 3:
      return rem(88);
    default:
      return rem(56);
  }
};

const Wrapper = styled.li(props => ({
  padding: `0 ${rem(16)}`,
  height: findHeight(props),
}));

const PrimaryLink = styled(Link)({ fontSize: rem(16) });
const SecondaryText = styled(Link)({ fontSize: rem(14) });

type Props = {
  category: { slug: string, slug: string, title: string, threadsCount: number },
};

export const CategoryListItem = ({
  category,
}: Props): Element<typeof Wrapper> | boolean =>
  !!category && (
    <Wrapper lines={2}>
      <PrimaryLink to={`/categories/${category.slug}`}>
        {category.title}
      </PrimaryLink>
      <FormattedMessage
        {...messages.threadsCount}
        values={{ threadsCount: category.threadsCount }}
      >
        {text => <SecondaryText>{text}</SecondaryText>}
      </FormattedMessage>
    </Wrapper>
  );

export default CategoryListItem;
