// @flow
import React from 'react';
import type { Element } from 'react';
import Helmet from 'react-helmet-async';
import { defineMessages, FormattedMessage, injectIntl } from 'react-intl';
import type { IntlProp } from 'common/types';

const messages = defineMessages({
  title: {
    id: 'categories.title',
    description: "CategoriesPage's title and header",
    defaultMessage: 'Categories',
  },
});

type Props = {
  intl: IntlProp,
  search?: string,
  isLoading?: boolean,
  onChange?: (event: SyntheticInputEvent<*>) => void,
};

export const CategoriesPage = ({
  search,
  isLoading,
  onChange,
  intl,
}: Props): Element<'main'> => (
  <main role="main">
    <Helmet title={intl.formatMessage(messages.title)} />
    <h3>
      <FormattedMessage {...messages.title} />
      <span>{isLoading ? '...' : ''}</span>
    </h3>
  </main>
);

const container = injectIntl;

export default container(CategoriesPage);
