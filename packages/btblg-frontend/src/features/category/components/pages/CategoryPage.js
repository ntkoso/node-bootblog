// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import ThreadPreview from 'features/thread/components/ThreadPreview';

const messages = defineMessages({
  loadMore: {
    id: 'category.loadMoreThreads',
    description: 'Load more threads button text',
    defaultMessage: 'Load more threads',
  },
});

type Props = {
  category: Object,
  pageInfo: Object,
};

export const CategoryPage = ({
  category,
}: Props): Element<typeof 'div'> | void =>
  category && (
    <div>
      <h3>{category.title}</h3>
      <h2>{category.threadsCount}</h2>
      {category.threads && category.threads.length > 0 && 'LIST'}
    </div>
  );

export default CategoryPage;
