// @flow
import React, { Fragment } from 'react';
import type { Element } from 'react';
import { Route } from 'react-router-dom';
import type { Match } from 'react-router-dom';
import styled from '@ntkoso/react-emotion';
import { CategoriesPage, CategoryPage } from '../pages';

type Props = { gridArea: string, match: Match };

export const CategoryScreen = ({
  gridArea,
  match: { url },
}: Props): Element<typeof Fragment> => (
  <Fragment>
    <Route
      exact
      path={url}
      render={props => <CategoriesPage {...props} gridArea={gridArea} />}
    />
    <Route
      path={`${url}/:id`}
      render={props => <CategoryPage {...props} gridArea={gridArea} />}
    />
  </Fragment>
);

export default CategoryScreen;
