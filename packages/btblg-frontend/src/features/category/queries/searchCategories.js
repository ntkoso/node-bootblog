// @flow
import gql from 'graphql-tag';

const searchCategoriesQuery = gql`
  query SearchCategories(
    $first: Int
    $last: Int
    $before: String
    $after: String
    $orderBy: String
    $search: String
  ) {
    categories(
      first: $first
      last: $last
      before: $before
      after: $after
      orderBy: $orderBy
      search: $search
    ) {
      edges {
        cursor
        node {
          id
          title
          slug
        }
      }
      total
      pageInfo {
        startCursor
        endCursor
        hasPreviousPage
        hasNextPage
      }
    }
  }
`;

export default searchCategoriesQuery;
