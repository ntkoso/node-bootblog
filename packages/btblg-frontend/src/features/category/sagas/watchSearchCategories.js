import { call, getContext, take } from 'redux-saga/effects';
import searchCategoriesQuery from '../queries/searchCategories';
import { searchCategories } from '../actions';

const watchSearchCategoies = function* watchSearchCategories() {
  const client = yield getContext('client');

  while (true) {
    const { payload: search } = yield take(searchCategories);

    /*
      yield put(
      search === '' ?
      push('/categories') :
      push(`/categories?search=${search}`)
    ); */

    yield call(
      client.query({
        query: searchCategoriesQuery,
        variables: { first: 15, search },
      }),
    );
  }
};

export default watchSearchCategoies;
