// @flow

export const defaults = {
  config: {
    __typename: 'Config',
    recaptchaSiteKey: '',
  },
};

const resolvers = {};

export default resolvers;
