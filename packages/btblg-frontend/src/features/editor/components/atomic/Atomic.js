import React from 'react';
import { Entity } from 'draft-js';
import Image from './Image';
import Embed from './Embed';

export const Atomic = ({ block }) => {
  const entity = Entity.get(block.getEntityAt(0));
  const data = entity.getData();
  const type = entity.getType();
  let media;

  if (type === 'IMAGE') {
    media = <Image {...data} />;
  } else if (type === 'EMBED') {
    media = <Embed {...data} />;
  }

  return media;
};

export default Atomic;
