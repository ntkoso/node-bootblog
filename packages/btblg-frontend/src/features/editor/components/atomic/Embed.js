// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';
import getYoutubeId from 'get-youtube-id';

const messages = defineMessages({
  unsupported: {
    'editor.atomic.embed.unsupported':
      'Given URL embed is currently unsupported',
  },
});

const IframeWrapper = styled.div({
  position: 'relative',
  height: 0,
  paddingTop: rem(25),
  paddingBottom: '56.25%',
});

const Iframe = styled.iframe({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
});

type Props = { url: string };

export const Embed = ({
  url,
}: Props): Element<typeof IframeWrapper | typeof FormattedMessage> => {
  const youtubeId = getYoutubeId(url);

  if (typeof youtubeId === 'string') {
    return (
      <IframeWrapper>
        <Iframe
          src={`https://www.youtube.com/embed/${youtubeId}`}
          frameBorder="0"
          allowFullScreen
        />
      </IframeWrapper>
    );
  }

  return <FormattedMessage {...messages.unsupported} />;
};

export default Embed;
