// @flow
import React from 'react';
import type { Element } from 'react';
import styled from '@ntkoso/react-emotion';
import rewriteLegacyLink from 'rewrite-legacy-link';

const Img = styled.img({
  width: '100%',
  height: 'auto',
});

type Props = { caption: string };

export const Image = ({ caption, ...rest }: Props): Element<typeof Img> =>
  !caption ? (
    <Img {...rest} role="presentation" />
  ) : (
    <Img alt={caption} {...rest} />
  );

/*
  FIXME:
  BAD WORKAROUND!
  LEGACY LINKS FIX
  REMOVE OR CHANGE IF YOU USING FRESH DATABASE
*/
export default rewriteLegacyLink('src', 'http://noviyvitok.com/')(Image);
