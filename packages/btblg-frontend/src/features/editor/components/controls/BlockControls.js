// @flow
import React from 'react';
import type { Element } from 'react';
import styled from '@ntkoso/react-emotion';
import FaQuoteLeftIcon from '@ntkoso/btblg-icon-set/format-quote-open.svg';
import FaListOl from '@ntkoso/btblg-icon-set/format-list-bulleted.svg';
import FaListUl from '@ntkoso/btblg-icon-set/format-list-numbers.svg';
import StyleButton from './StyleButton';

const controls = [
  { label: 'Unordered list', style: 'unordered-list-item', icon: <FaListUl /> },
  { label: 'Ordered list', style: 'ordered-list-item', icon: <FaListOl /> },
  { label: 'Quote', style: 'blockquote', icon: <FaQuoteLeftIcon /> },
];

const Wrapper = styled.div({ position: 'relative' });

const Control = styled.div({ float: 'right' });

type Props = { editorState: Object, onToggle: Function, top: number };

export const BlockControls = ({
  editorState,
  onToggle,
  top,
}: Props): Element<typeof Wrapper> => {
  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();

  return (
    <Wrapper style={{ transform: `translateY(${top}px)` }}>
      {controls.map(control => (
        <Control key={control.label}>
          <StyleButton
            active={control.style === blockType}
            icon={control.icon}
            label={control.label}
            onToggle={onToggle}
            style={control.style}
          />
        </Control>
      ))}
    </Wrapper>
  );
};

export default BlockControls;
