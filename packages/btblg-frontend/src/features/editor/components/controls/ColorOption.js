// @flow
import React from 'react';
import type { Element } from 'react';
import { withHandlers } from 'recompose';
import { rgba, rem } from 'polished';
import styled from '@ntkoso/react-emotion';
import { styleMap } from '../../settings';

const Wrapper = styled.div({
  width: '100%',
  height: '100%',
  padding: `${rem(6)} ${rem(12)}`,
  marginTop: rem(6),
  border: `1px solid ${rgba('black', 0.48)}`,

  '&:first-child': {
    marginTop: rem(18),
  },

  '&:last-child': {
    marginBottom: rem(18),
  },
});

type Props = { value: string, handleMouseDown: (event: MouseEvent) => void };

export const ColorOption = ({
  value,
  handleMouseDown,
}: Props): Element<typeof Wrapper> => (
  <Wrapper style={styleMap[value] || {}} onMouseDown={handleMouseDown} />
);

const container = withHandlers({
  handleMouseDown: ({ value, onMouseDown }) => event => {
    event.preventDefault();

    if (typeof onMouseDown === 'function') {
      onMouseDown(value);
    }
  },
});

export default container(ColorOption);
