// @flow
import React from 'react';
import type { Element } from 'react';
import rgbaConvert from 'rgba-convert';
import { rgba, rem } from 'polished';
import styled from '@ntkoso/react-emotion';
import FormatBoldIcon from '@ntkoso/btblg-icon-set/format-bold.svg';
import FormatItalicIcon from '@ntkoso/btblg-icon-set/format-italic.svg';
import FormatUnderlinedIcon from '@ntkoso/btblg-icon-set/format-underline.svg';
import FormatColorFillIcon from '@ntkoso/btblg-icon-set/format-color-fill.svg';
import DropDown from 'common/components/design/dropdown/DropDown';
import { styleMap } from '../../settings';
import ColorOption from './ColorOption';
import StyleButton from './StyleButton';

const controls = [
  { label: 'Bold', style: 'BOLD', icon: <FormatBoldIcon /> },
  { label: 'Italic', style: 'ITALIC', icon: <FormatItalicIcon /> },
  { label: 'Underline', style: 'UNDERLINE', icon: <FormatUnderlinedIcon /> },
];

const colorOptions = [
  { label: 'Blue', value: 'COLOR_BLUE' },
  { label: 'Red', value: 'COLOR_RED' },
  { label: 'Green', value: 'COLOR_GREEN' },
];

const emptyColorOption = { label: 'Empty', value: 'COLOR_EMPTY' };

const getFillColorsFor = (value, inlineStyle) => {
  if (
    !inlineStyle.has(value) ||
    !styleMap[value] ||
    typeof styleMap[value].backgroundColor !== 'string'
  ) {
    return { fill: 'currentColor' };
  }

  const { a, ...rgb } = rgbaConvert.obj(styleMap[value].backgroundColor);

  return {
    fill: rgbaConvert.css(rgb),
    fillOpacity: a / 255,
  };
};

const Wrapper = styled.div({
  position: 'absolute',
  zIndex: 99,
  display: 'inline-block',
  backgroundColor: 'white',
  borderRadius: rem(2),

  /* TODO: box-shadow 2dp */

  '&::before': {
    position: 'absolute',
    top: '100%',
    left: '50%',
    zIndex: 98,
    display: 'block',
    width: 0,
    height: 0,
    marginLeft: rem(12) /* ( arrow width / 2 ) */,
    clear: 'both',
    color: 'white',
    pointerEvents: 'none',
    content: '',
    border: `${rem(1)} solid black`,
    borderColor: 'transparent transparent white white',
    boxShadow: `-1px 3px 1px 0 ${rgba('black', 0.2)}`,
    transform: 'rotate(-45deg)',
    transformOrigin: '0 0',
  },
});

const InlineControl = styled(StyleButton)({
  display: 'inner-block',
  overflow: 'visible',
});

const InlineDropDown = styled(DropDown)({
  display: 'inline-block',
  padding: `${rem(5)} ${rem(12)}`,
  overflow: 'visible',
});

const ColorFillIcon = styled(FormatColorFillIcon)({
  width: rem(24),
  height: rem(24),
  color: rgba('black', 0.26),
  stroke: rgba('black', 0.54),
  strokeWidth: 1,
});

type Props = {
  inlineStyle: Function,
  onToggle: Function,
  onColorSelect: Function,
  top: number,
  left: number,
};

export const InlineControls = ({
  inlineStyle,
  onToggle,
  onColorSelect,
  top,
  left,
}: Props): Element<typeof Wrapper> => {
  const selectedColorOption = colorOptions.find(({ value }) =>
    inlineStyle.has(value),
  );
  const selected = selectedColorOption && selectedColorOption.value;
  let options = colorOptions.filter(({ value }) => !inlineStyle.has(value));

  if (selected) {
    options = [emptyColorOption, ...options];
  }

  return (
    <Wrapper style={{ top, left }}>
      {controls.map(control => (
        <InlineControl
          active={inlineStyle.has(control.style)}
          key={control.label}
          icon={control.icon}
          label={control.label}
          onToggle={onToggle}
          style={control.style}
        />
      ))}
      <InlineDropDown
        options={options}
        initialValue={selected}
        onChange={onColorSelect}
        renderOption={props => <ColorOption {...props} />}
        renderSegment={({ value }) => {
          const { fill, fillOpacity = 'inherit' } = getFillColorsFor(
            value,
            inlineStyle,
          );

          return <ColorFillIcon fill={fill} fillOpacity={fillOpacity} />;
        }}
      />
    </Wrapper>
  );
};

export default InlineControls;
