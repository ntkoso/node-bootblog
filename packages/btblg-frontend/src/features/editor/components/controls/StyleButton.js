// @flow
import React from 'react';
import type { Element } from 'react';
import { withHandlers } from 'recompose';
import styled from '@ntkoso/react-emotion';
import { rgba, rem } from 'polished';

const Wrapper = styled.div(
  {
    width: rem(48),
    padding: `${rem(6)} ${rem(12)}`,
    cursor: 'pointer',
    '-webkit-tap-highlight-color': rgba('black', 0),

    '& svg': {
      width: rem(24),
      height: rem(24),

      color: rgba('black', 0.26),
      fill: rgba('black', 0.26),
    },
  },
  ({ active }) =>
    !active
      ? {}
      : {
          backgroundColor: rgba('black', 0.21),

          '& svg': {
            color: rgba('black', 0.54),
            fill: rgba('black', 0.54),
          },

          '& + &': {
            borderLeft: `1px solid ${rgba('black', 0.12)}`,
          },
        },
);

type Props = {
  handleOnToggle: (event: SyntheticInputEvent) => void,
  icon: Element<*>,
  active?: boolean,
};

export const StyleButton = ({
  handleOnToggle,
  icon,
  active,
}: Props): Element<typeof Wrapper> => (
  <Wrapper active={active} onMouseDown={handleOnToggle}>
    {icon}
  </Wrapper>
);

const container = withHandlers({
  handleOnToggle: ({ onToggle, style }) => event => {
    event.preventDefault();

    if (typeof onToggle === 'function') {
      onToggle(style);
    }
  },
});

export default container(StyleButton);
