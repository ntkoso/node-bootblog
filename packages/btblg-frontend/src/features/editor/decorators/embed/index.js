import { findEntities } from '../../utils';

const decorator = {
  strategy: findEntities('EMBED'),
};

export default decorator;
