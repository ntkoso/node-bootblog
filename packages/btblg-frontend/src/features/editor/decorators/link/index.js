import { findEntities } from '../../utils';
import EditorLink from './EditorLink';

const decorator = {
  strategy: findEntities('LINK'),
  component: EditorLink,
};

export default decorator;
