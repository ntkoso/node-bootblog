import { RichUtils } from 'draft-js';

const toggleBlockType = type => state => RichUtils.toggleBlockType(state, type);

export default toggleBlockType;
