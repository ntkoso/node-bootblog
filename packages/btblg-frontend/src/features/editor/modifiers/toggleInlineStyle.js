import { RichUtils } from 'draft-js';

const toggleInlineStyle = style => state =>
  RichUtils.toggleInlineStyle(state, style);

export default toggleInlineStyle;
