import Atomic from '../components/atomic/Atomic';

const blockRenderer = block => {
  const type = block.getType();

  if (type === 'atomic') {
    return {
      component: Atomic,
      editable: false,
    };
  }

  return null;
};

export default blockRenderer;
