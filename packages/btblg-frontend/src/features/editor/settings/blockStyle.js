const blockStyle = block => {
  const blockType = block.getType();

  if (blockType === 'blockquote') {
    return '.blockquote';
  }

  return null;
};

export default blockStyle;
