// @flow
import { rgba } from 'polished';

const alpha = 0.24;

const styleMap = {
  COLOR_RED: {
    backgroundColor: rgba('red', alpha),
  },
  COLOR_GREEN: {
    backgroundColor: rgba('green', alpha),
  },
  COLOR_BLUE: {
    backgroundColor: rgba('blue', alpha),
  },
};

export default styleMap;
