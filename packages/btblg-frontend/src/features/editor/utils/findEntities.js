import hasEntityType from './hasEntityType';

const findEntities = type => (blck, cb) =>
  blck.findEntityRanges(hasEntityType(type), cb);

export default findEntities;
