const getSelectedBlockElement = range => {
  let node = range.startContainer;

  do {
    if (node.getAttribute && node.getAttribute('data-block') === 'true') {
      return node;
    }

    node = node.parentNode;
  } while (node !== null);

  return null;
};

export default getSelectedBlockElement;
