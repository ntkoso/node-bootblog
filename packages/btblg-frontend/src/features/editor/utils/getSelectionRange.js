const getSelectionRange = windw => {
  const selection = windw.getSelection();

  if (selection.rangeCount === 0) {
    return null;
  }

  return selection.getRangeAt(0);
};

export default getSelectionRange;
