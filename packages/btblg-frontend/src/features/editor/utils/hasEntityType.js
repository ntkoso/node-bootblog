import { Entity } from 'draft-js';

const hasEntityType = type => character => {
  const entity = character.getEntity();

  return entity !== null && Entity.get(entity).getType() === type;
};

export default hasEntityType;
