// @flow
import React from 'react';
import { Node } from 'react';
import { IntlProvider } from 'react-intl';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

export { IntlProvider };

const INTL_QUERY = gql`
  query Intl {
    intl @client {
      locale
      messages
    }
  }
`;

type Props = {
  children: Node,
};

const LocalizedIntlProvider = ({ children }: Props) => (
  <Query query={INTL_QUERY}>
    {({ data: { intl: { locale, messages } } }) => (
      <IntlProvider locale={locale} messages={messages}>
        {children}
      </IntlProvider>
    )}
  </Query>
);

export default LocalizedIntlProvider;
