// @flow

export const defaults = {
  intl: {
    __typename: 'Intl',
    locale: 'en',
    messages: {},
    initialNow: Date.now(),
  },
};

const resolvers = {};

export default resolvers;
