// @flow
import React from 'react';
import type { Element } from 'react';
import { ThreadInfo, ThreadHeader, ThreadBody } from './parts';

type Props = {
  thread: {
    title: string,
    slug: string,
    category: { title: string, slug: string },
    body: string,
    author: { username: string },
    views: number,
    createdAt: string,
    updatedAt: string,
  },
};

export const Thread = ({ thread }: Props): Element<'article'> => (
  <article>
    <ThreadHeader
      title={thread.title}
      slug={thread.slug}
      categoryTitle={thread.category.title}
      categorySlug={thread.category.slug}
    />
    <ThreadBody bodyString={thread.body} />
    {thread.author && <div>{thread.author.username}</div>}
    <ThreadInfo
      views={thread.views}
      createdAt={thread.createdAt}
      updatedAt={thread.updatedAt}
    />
    <section />
  </article>
);

export default Thread;
