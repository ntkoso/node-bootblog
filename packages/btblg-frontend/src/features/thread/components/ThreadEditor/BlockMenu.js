// @flow
import React from 'react';
import Portal from 'react-portal';
import type { Value } from 'slate';
import styled from '@ntkoso/react-emotion';
import StyleButton from './StyleButton';

const Wrapper = styled.div({ position: 'relative' });

type Props = { value: Value };

export const BlockMenu = ({ value }: Props) => {
  return (
    <Portal>
      <Wrapper>
        <StyleButton />
      </Wrapper>
    </Portal>
  );
};

export default BlockMenu;
