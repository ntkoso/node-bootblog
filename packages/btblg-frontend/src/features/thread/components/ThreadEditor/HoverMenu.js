// @flow
import React from 'react';
import { Portal } from 'react-portal';
import type { Value } from 'slate';
import { rem, rgba } from 'polished';
import styled from '@ntkoso/react-emotion';
import StyleButton from './StyleButton';

const Wrapper = styled.div({
  position: 'absolute',
  zIndex: 99,
  display: 'inline-block',
  backgroundColor: 'white',
  borderRadius: rem(2),

  /* TODO: box-shadow 2dp */

  '&::before': {
    position: 'absolute',
    top: '100%',
    left: '50%',
    zIndex: 98,
    display: 'block',
    width: 0,
    height: 0,
    marginLeft: rem(12) /* ( arrow width / 2 ) */,
    clear: 'both',
    color: 'white',
    pointerEvents: 'none',
    content: '',
    border: `${rem(1)} solid black`,
    borderColor: 'transparent transparent white white',
    boxShadow: `-1px 3px 1px 0 ${rgba('black', 0.2)}`,
    transform: 'rotate(-45deg)',
    transformOrigin: '0 0',
  },
});

type Props = { value: Value };

export const HoverMenu = ({ value }: Props) =>
  console.log(value.selection) &&
  value.selection && (
    <Portal>
      <Wrapper>
        <StyleButton />
      </Wrapper>
    </Portal>
  );

export default HoverMenu;
