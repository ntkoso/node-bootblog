// @flow
import React from 'react';
import { rem, rgba } from 'polished';
import styled from '@ntkoso/react-emotion';

const Wrapper = styled.button(
  {
    width: rem(48),
    padding: `${rem(6)} ${rem(12)}`,
    cursor: 'pointer',
    '-webkit-tap-highlight-color': rgba('black', 0),

    '& svg': {
      width: rem(24),
      height: rem(24),

      color: rgba('black', 0.26),
      fill: rgba('black', 0.26),
    },
  },
  ({ active }) =>
    !active
      ? {}
      : {
          backgroundColor: rgba('black', 0.21),

          '& svg': {
            color: rgba('black', 0.54),
            fill: rgba('black', 0.54),
          },

          '& + &': {
            borderLeft: `1px solid ${rgba('black', 0.12)}`,
          },
        },
);

const StyleButton = ({ active, icon, onToggle }) => (
  <Wrapper active={active} onMouseDown={onToggle}>
    {icon}
  </Wrapper>
);

export default StyleButton;
