// @flow
import React, { Component, createRef, Fragment } from 'react';
import { Value } from 'slate';
import { Editor } from 'slate-react';
import HoverMenu from './HoverMenu';
import BlockMenu from './BlockMenu';

const plugins = [];

type onChangeHandler = (params: { value: Value }) => void;

type Props = { value: Value, onChange: onChangeHandler };

class ThreadEditor extends Component<Props> {
  editorRef = createRef();

  render() {
    return (
      <Fragment>
        <Editor
          plugins={plugins}
          ref={this.editorRef}
          value={this.props.value}
        />
      </Fragment>
    );
  }
}

export default ThreadEditor;
