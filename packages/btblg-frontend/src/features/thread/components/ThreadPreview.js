// @flow
import React from 'react';
import type { Element } from 'react';
import { ThreadBody, ThreadHeader, ThreadInfo } from './parts';

type Thread = {
  title: string,
  slug: string,
  category: { title: string, slug: string },
  body: string,
  views: number,
  createdAt: string,
  updatedAt: string,
};

type Props = { thread?: Thread };

export const ThreadPreview = ({ thread }: Props): Element<'li'> | boolean =>
  !!thread && (
    <li>
      <ThreadHeader
        title={thread.title}
        slug={thread.slug}
        categoryTitle={thread.category.title}
        categorySlug={thread.category.slug}
      />
      <ThreadBody bodyString={thread.body} truncate />
      <ThreadInfo
        views={thread.views}
        createdAt={thread.createdAt}
        updatedAt={thread.updatedAt}
      />
    </li>
  );

export default ThreadPreview;
