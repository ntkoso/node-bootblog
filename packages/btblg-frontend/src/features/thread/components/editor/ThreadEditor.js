// @flow
// FIXME: CLEAN UP THIS MESS
import React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import {
  Editor,
  RichUtils,
  EditorState,
  Modifier,
  getVisibleSelectionRect,
} from 'draft-js';
import compose from 'compose-function';
import { withHandlers, withStateHandlers } from 'recompose';
import styled from '@ntkoso/react-emotion';
import { toggleBlockType, toggleInlineStyle } from 'features/editor/modifiers';
import {
  getSelectionRange,
  getSelectedBlockElement,
} from 'features/editor/utils';
import { blockRenderer, blockStyle, styleMap } from 'features/editor/settings';
import BlockControls from 'features/editor/components/controls/BlockControls';
import InlineControls from 'features/editor/components/controls/InlineControls';

const messages = defineMessages({
  placeholder: {
    id: 'threadEditor.placeholder',
    description: 'Placeholder message for Thread Editor',
    defaultMessage: 'Tell your story...',
  },
});

const calculateBlockOffset = (editorRect, blockRect) =>
  blockRect.bottom - editorRect.top - 32;

const calculateInlineOffsets = (editorRect, selectionRect) => {
  const selectionWidth = selectionRect.right - selectionRect.left;

  const left =
    selectionRect.left - editorRect.left + selectionWidth / 2 - 208 / 2; // width of inlineControls

  const top = selectionRect.top - editorRect.top - 56; // height of inlineControls

  return { left, top };
};

type BlockControlsState = {| +show: boolean, +top: number |};
type InlineControlsState = {| +show: boolean, +top: number, +left: number |};
type ControlsState = {|
  +blockControls: BlockControlsState,
  +inlineControls: InlineControlsState,
|};
type ControlType = 'block' | 'inline';

const Side = styled.div({ minHeight: '1px' });
const Content = styled.div({ position: 'relative' });

type Props = {
  editorKey: string,
  editorState: Object,
  blockControls: Object,
  inlineControls: Object,
  updateEditorState: Function,
  enableControls: Function,
  disableControls: Function,
  onColorSelect: Function,
  onToggleBlockType: Function,
  onToggleInlineStyle: Function,
  onChange: Function,
  readOnly?: boolean,
};

export class ThreadEditor extends React.Component<Props> {
  constructor(props: Props) {
    super(props);

    this.focus = () => {
      if (this.editor) {
        this.editor.focus();
      }
    };

    this.handleEditorChange = this.handleEditorChange.bind(this);
    this.updateBlockControls = this.updateBlockControls.bind(this);
  }

  focus: Function;
  editor: { focus: Function, refs: Object };
  handleEditorChange: Function;
  updateBlockControls: Function;

  updateBlockControls() {
    const { enableControls, disableControls } = this.props;

    const selectionRange = getSelectionRange(window);

    if (selectionRange) {
      const selectedBlock = getSelectedBlockElement(selectionRange);

      if (selectedBlock) {
        const editorRect = this.editor.refs.editorContainer.getBoundingClientRect();
        const blockRect = selectedBlock.getBoundingClientRect();
        const top = calculateBlockOffset(editorRect, blockRect);

        enableControls('block', { top });
      }
    }

    disableControls('block');
  }

  handleEditorChange(currentEditorState: Function): void {
    const { enableControls, disableControls, updateEditorState } = this.props;

    if (!currentEditorState.getSelection().isCollapsed()) {
      // NOTE: https://github.com/facebook/draft-js/issues/290 - bug!
      const selectionRect = getVisibleSelectionRect(window);
      const editorRect = this.editor.refs.editorContainer.getBoundingClientRect();

      if (selectionRect && editorRect) {
        const { top, left } = calculateInlineOffsets(editorRect, selectionRect);

        enableControls('inline', { top, left });
      } else {
        disableControls('inline');
      }
    } else {
      disableControls('inline');
    }

    updateEditorState(currentEditorState);
    setTimeout(this.updateBlockControls, 0);
  }

  render() {
    const {
      editorKey,
      editorState,
      blockControls,
      inlineControls,
      onColorSelect,
      onToggleBlockType,
      onToggleInlineStyle,
      readOnly,
      onChange,
    } = this.props;

    const { handleEditorChange } = this;

    return (
      <div onClick={this.focus}>
        {!readOnly && (
          <Side>
            {blockControls.show && (
              <BlockControls
                editorState={editorState}
                onToggle={onToggleBlockType}
                top={blockControls.top}
              />
            )}
          </Side>
        )}
        <Content>
          {inlineControls.show && (
            <InlineControls
              inlineStyle={editorState.getCurrentInlineStyle()}
              onColorSelect={onColorSelect}
              onToggle={onToggleInlineStyle}
              top={inlineControls.top}
              left={inlineControls.left}
            />
          )}
          <Editor
            blockRendererFn={blockRenderer}
            blockStyleFn={blockStyle}
            customStyleMap={styleMap}
            editorKey={editorKey}
            editorState={editorState}
            onChange={handleEditorChange}
            placeholder={<FormattedMessage {...messages.placeholder} />}
            readOnly={readOnly}
            ref={ref => {
              this.editor = ref;
            }}
          />
        </Content>
      </div>
    );
  }
}

const initialState: ControlsState = {
  blockControls: {
    show: false,
    top: 0,
  },
  inlineControls: {
    show: false,
    top: 0,
    left: 0,
  },
};

const enableControls = (state: ControlsState) => (
  controlType: ControlType,
  { top = 0, left = 0 }: { top?: number, left?: number } = {},
): ControlsState => {
  if (controlType === 'block') {
    return {
      blockControls: { show: true, top },
      inlineControls: state.inlineControls,
    };
  }

  if (controlType === 'inline') {
    return {
      blockControls: state.blockControls,
      inlineControls: { show: true, top, left },
    };
  }

  return state;
};

const disableControls = (state: ControlsState) => (
  controlType: ControlType,
): ControlsState => {
  if (controlType === 'block') {
    return {
      blockControls: initialState.blockControls,
      inlineControls: state.inlineControls,
    };
  }

  if (controlType === 'inline') {
    return {
      blockControls: state.blockControls,
      inlineControls: initialState.inlineControls,
    };
  }

  return state;
};

const container = compose(
  withStateHandlers(initialState, { enableControls, disableControls }),
  withHandlers({
    onColorSelect({ updateEditorState }) {
      return color =>
        updateEditorState(editorState => {
          const selection = editorState.getSelection();

          let contentState = Object.keys(styleMap)
            .filter(styleKey => styleKey.startsWith('COLOR_'))
            .reduce(
              (contentState, clr) =>
                Modifier.removeInlineStyle(contentState, selection, clr),
              editorState.getCurrentContent(),
            );

          if (color === 'COLOR_EMPTY') {
            contentState = Modifier.removeInlineStyle(
              contentState,
              selection,
              'COLOR_EMPTY',
            );
          }

          let nxtState = EditorState.push(
            editorState,
            contentState,
            'change-inline-style',
          );

          const currentStyle = editorState.getCurrentInlineStyle();

          if (selection.isCollapsed()) {
            nxtState = reduce(
              (acc, style) => RichUtils.toggleInlineStyle(acc, style),
              nxtState,
              currentStyle,
            );
          }

          if (!currentStyle.has(color)) {
            nxtState = RichUtils.toggleInlineStyle(nxtState, color);
          }

          return nxtState;
        });
    },
    onToggleBlockType({ updateEditorState }) {
      return type => updateEditorState(toggleBlockType(type));
    },
    onToggleInlineStyle({ updateEditorState }) {
      return style => updateEditorState(toggleInlineStyle(style));
    },
  }),
);

export default container(ThreadEditor);
