// @flow
import React from 'react';
import type { Element } from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import { Formik, Form, Field } from 'formik';
import { Value } from 'slate';
import styled from '@ntkoso/react-emotion';
// import { rem } from 'polished';
import { InteractivePrimaryButton } from 'features/ui/components';
import ThreadEditor from 'features/thread/components/ThreadEditor';

const messages = defineMessages({
  titlePlaceholder: {
    id: 'thread.form.title.placeholder',
    defaultMessage: 'Enter thread name...',
  },
  editorPlaceholder: {
    id: 'thread.form.editor.placeholder',
    defaultMessage: 'Please enter thread body',
  },
});

const TitleField = styled(Field)({
  borderColor: 'transparent',

  ':focus': {
    outline: 'none',
  },
});

type Props = {
  initialValue?: {},
};

const ThreadForm = ({ initialValue }: Props): Element<typeof Form> => (
  <Formik
    initialValues={{
      title: '',
      editorValue: Value.fromJS(initialValue || {}),
    }}
    onSubmit={({ title, editorValue }, { setSubmitting }) => {
      console.log('TODO: Write graphql submit mutation', {
        title,
        editorValue: Value.toJS(editorValue),
      });
      setSubmitting(false);
    }}
    render={({ values, handleBlur, setFieldValue, isSubmitting }) => (
      <Form>
        <TitleField
          name="title"
          component="input"
          type="text"
          value={values.title}
          placeholder={<FormattedMessage {...messages.titlePlaceholder} />}
        />
        <ThreadEditor
          value={values.editorValue}
          onChange={setFieldValue}
          onBlur={handleBlur}
        />
        <InteractivePrimaryButton type="submit" disabled={isSubmitting}>
          Submit
        </InteractivePrimaryButton>
      </Form>
    )}
  />
);

export default ThreadForm;
