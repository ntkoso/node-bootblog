// @flow
import React from 'react';
import type { Element } from 'react';
import { TransitionGroup } from 'react-transition-group';
import { FadeTransition } from '@ntkoso/btblg-style-guide/es/components/transitions';
import ThreadPreview from '../ThreadPreview';

type Props = { ids: Array<string> };

const toThreadPreview = id => (
  <FadeTransition key={id}>
    <ThreadPreview id={id} />
  </FadeTransition>
);

export const ThreadPreviewList = ({
  ids,
}: Props): Element<typeof TransitionGroup> => (
  <TransitionGroup component="ul">{ids.map(toThreadPreview)}</TransitionGroup>
);

export default ThreadPreviewList;
