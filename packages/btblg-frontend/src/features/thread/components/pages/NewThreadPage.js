// @flow
import React from 'react';
import type { Element } from 'react';
import Helmet from 'react-helmet-async';
import { defineMessages, injectIntl } from 'react-intl';
import type { IntlProp } from 'common/types';
import { ThreadForm } from '../forms';

const messages = defineMessages({
  title: {
    id: 'new-thread.title',
    description: 'NewThreadPage title',
    defaultMessage: 'New Thread - {title}',
  },
});

type Props = { intl: IntlProp, title: string };

export const NewThreadPage = ({ intl, title }: Props): Element<'div'> => (
  <div>
    <Helmet title={intl.formatMessage(messages.title, { title })} />
    <ThreadForm submitLabel="submit" />
  </div>
);

export default injectIntl(NewThreadPage);
