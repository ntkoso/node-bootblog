// @flow
import React from 'react';
import type { Element } from 'react';
import Thread from '../Thread';

type Props = { thread: Object };

export const ThreadPage = ({ thread }: Props): Element<'div'> => (
  <div>{thread && <Thread thread={thread} />}</div>
);

export default ThreadPage;
