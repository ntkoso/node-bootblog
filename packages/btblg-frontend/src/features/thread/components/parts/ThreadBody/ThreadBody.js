// @flow
import React, { Component } from 'react';
import type { Value } from 'slate';
import { Editor } from 'slate-react';

type Props = { value: Value };

export const ThreadBody = ({ value }: Props) => (
  <Editor value={value} readOnly />
);

export default ThreadBody;
