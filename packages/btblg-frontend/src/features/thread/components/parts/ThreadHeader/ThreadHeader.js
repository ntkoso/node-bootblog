// @flow
import React from 'react';
import type { Element } from 'react';
import { Link } from 'react-router-dom';

type Props = {
  title: string,
  slug: string,
  categoryTitle: string,
  categorySlug: string,
};

export const ThreadHeader = ({
  title,
  slug,
  categoryTitle,
  categorySlug,
}: Props): Element<'header'> => (
  <header>
    <h2>
      <Link to={`/threads/${slug}`}>{title}</Link>
    </h2>
    <h3>
      <Link to={`/categories/${categorySlug}`}>{categoryTitle}</Link>
    </h3>
  </header>
);

export default ThreadHeader;
