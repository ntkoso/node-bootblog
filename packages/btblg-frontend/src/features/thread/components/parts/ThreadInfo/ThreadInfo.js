// @flow
import React from 'react';
import type { Element } from 'react';
import { FormattedRelative } from 'react-intl';

type Props = { views: number, createdAt: string, updatedAt: string };

export const ThreadInfo = ({
  views,
  createdAt,
  updatedAt,
}: Props): Element<'div'> => (
  <div>
    {views && <span>{`${views} views`}</span>}
    {createdAt && <FormattedRelative updateInterval={null} value={createdAt} />}
    {updatedAt && <FormattedRelative updateInterval={null} value={updatedAt} />}
  </div>
);

export default ThreadInfo;
