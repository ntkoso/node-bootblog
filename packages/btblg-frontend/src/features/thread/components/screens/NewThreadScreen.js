// @flow
import React from 'react';
import type { Element } from 'react';
import { Route } from 'react-router-dom';
import type { Match } from 'react-router-dom';
import styled from '@ntkoso/react-emotion';
import { NewThreadPage } from '../pages';

const Content = styled.div(({ gridArea }) => ({ gridArea }));

type Props = { gridArea: string, match: Match };

export const NewThreadScreen = ({
  gridArea,
  match: { url },
}: Props): Element<typeof Content> => (
  <Content gridArea={gridArea}>
    <Route
      exact
      path={url}
      render={props => (
        <main>
          <NewThreadPage {...props} />
        </main>
      )}
    />
  </Content>
);

export default NewThreadScreen;
