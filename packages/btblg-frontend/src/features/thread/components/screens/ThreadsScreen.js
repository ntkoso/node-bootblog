// @flow
import React from 'react';
import type { Element } from 'react';
import { Route } from 'react-router-dom';
import type { Match } from 'react-router-dom';
import styled from '@ntkoso/react-emotion';
import { ThreadPage } from '../pages';

const Content = styled.div(({ gridArea }) => ({ gridArea }));

type Props = { gridArea: string, match: Match };

export const ThreadsScreen = ({
  gridArea,
  match: { url },
}: Props): Element<typeof Content> => (
  <Content gridArea={gridArea}>
    <Route
      path={`${url}/:id`}
      render={props => (
        <main>
          <ThreadPage {...props} />
        </main>
      )}
    />
  </Content>
);

export default ThreadsScreen;
