// @flow
import React from 'react';
import type { Node, Element, ComponentType } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { render } from 'react-universal-interface';

const IS_INTERACTIVE_QUERY = gql`
  query IsInteractive {
    ui @client {
      isInteractive
    }
  }
`;

type ChildRenderProp = (isInteractive: boolean) => Node;

type Props = { children: ChildRenderProp };

const Interactive = props => (
  <Query query={IS_INTERACTIVE_QUERY}>
    {({ data: { ui: { isInteractive = false } } }) =>
      render(props, { isInteractive })
    }
  </Query>
);

export const withInteractive = <Props: {}>(propName: string) => (
  BaseComponent: ComponentType<Props>,
) => (props: Props): Element<typeof Interactive> => (
  <Interactive>
    {({ isInteractive }) => (
      <BaseComponent
        {...{
          ...props,
          [propName]: !isInteractive || props[propName],
        }}
      />
    )}
  </Interactive>
);

export default Interactive;
