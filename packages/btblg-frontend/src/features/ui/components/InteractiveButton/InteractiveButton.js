// @flow
import React from 'react';
import { Button, PrimaryButton } from '@ntkoso/btblg-style-guide/es/components';
import { withInteractive } from '../Interactive';

export const InteractivePrimaryButton = withInteractive('disabled')(
  PrimaryButton,
);

export default withInteractive('disabled')(Button);
