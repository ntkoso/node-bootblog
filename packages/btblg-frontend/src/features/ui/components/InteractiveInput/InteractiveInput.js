// @flow
import { Input } from '@ntkoso/btblg-style-guide/es/components';
import { withInteractive } from '../Interactive';

export default withInteractive('disabled')(Input);
