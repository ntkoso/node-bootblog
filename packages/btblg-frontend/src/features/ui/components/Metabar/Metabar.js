// @flow
import get from 'get-value';
import React, { Fragment } from 'react';
import type { Element } from 'react';
import { Query } from 'react-apollo';
import Loadable from 'react-loadable';
import { Link } from 'react-router-dom';
import gql from 'graphql-tag';
import { rem } from 'polished';
import Component from '@reactions/component';
import styled from '@ntkoso/react-emotion';
import { Action, Logo } from './parts';
import { SideMenu } from 'common/components/parts';
import { ProfileLink } from 'features/user/components';

const Wrapper = styled.header(
  {
    zIndex: 1,
    display: 'flex',
    height: rem(48),
    margin: '0 auto',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  ({ theme: { maxWidth } }) => ({ maxWidth: maxWidth || rem(1140) }),
);

const CURRENT_USER_QUERY = gql`
  query currentUser {
    currentUser {
      id
      usernameCanonical
    }
  }
`;

type ActionLink = { to: string, message: { id: string } };

const hasMessage = actionLink =>
  get(actionLink, ['to', 'length']) > 0 && get(actionLink, ['message', 'id']);

type Props = {
  gridArea: string,
  title?: string,
  actionLink?: ActionLink,
};

export const Metabar = ({
  gridArea,
  title,
  actionLink,
}: Props): Element<typeof Wrapper> => (
  <Wrapper style={{ gridArea }}>
    <Component initialState={{ active: false }}>
      {({ state, setState }) => {
        const handleOpen = () => setState({ active: true });
        const handleClose = () => setState({ active: false });

        return (
          <Fragment>
            <button onClick={handleOpen}>===</button>
            <SideMenu active={state.active} onClose={handleClose} />
          </Fragment>
        );
      }}
    </Component>
    <Link to="/">
      <Logo size={24} color="#000000" />
    </Link>
    {title && <span>{title}</span>}
    {actionLink &&
      hasMessage(actionLink) && (
        <Link to={actionLink.to}>
          <Action message={actionLink.message} />
        </Link>
      )}
    <Query query={CURRENT_USER_QUERY}>
      {({ data: { currentUser } }) =>
        currentUser &&
        currentUser.id && <Link to="/profile">{currentUser.id}</Link>
      }
    </Query>
  </Wrapper>
);

export default Metabar;
