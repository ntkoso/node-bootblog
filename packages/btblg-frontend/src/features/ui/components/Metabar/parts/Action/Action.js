// @flow
import React from 'react';
import type { Element } from 'react';
import { FormattedMessage } from 'react-intl';
import { IntlMessage } from 'common/types';

type Props = { message: IntlMessage };

export const Action = ({ message }: Props): Element<'div'> => (
  <div>
    <FormattedMessage {...message} />
  </div>
);

export default Action;
