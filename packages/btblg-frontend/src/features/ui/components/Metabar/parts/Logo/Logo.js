// flow
import React from 'react';
import type { Element } from 'react';
import styled from '@ntkoso/react-emotion';

const Span = styled.span({ color: 'lightblue' });

export const Logo = (): Element<typeof Logo> => (
  <Span>LOGO</Span>
);

export default Logo;
