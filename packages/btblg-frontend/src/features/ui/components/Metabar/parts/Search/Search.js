// @flow
import React from 'react';
import type { Element } from 'react';
import MagnifyIcon from '@ntkoso/btblg-icon-set/magnify.svg';

export const Search = (): Element<typeof MagnifyIcon> => <MagnifyIcon />;

export default Search;
