import gql from 'graphql-tag';

const makeUIInteractive = gql`
  mutation MakeUIInteractive {
    makeUIInteractive @client
  }
`;

export default makeUIInteractive;
