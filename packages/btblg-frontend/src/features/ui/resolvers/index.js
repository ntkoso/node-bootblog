// @flow
import makeUIInteractive from './makeUIInteractive';

export const defaults = {
  ui: { __typename: 'UI', isInteractive: false },
};

const resolvers = {
  Mutation: {
    makeUIInteractive,
  },
};

export default resolvers;
