// @flow
import gql from 'graphql-tag';

const IS_INTERACTIVE_QUERY = gql`
  query IsInteractive {
    ui @client {
      __typename
      isInteractive
    }
  }
`;

const makeUIInteractive = (_, __, { cache }) => {
  cache.writeQuery({
    query: IS_INTERACTIVE_QUERY,
    data: { ui: { isInteractive: true, __typename: 'UI' } },
  });

  return null;
};

export default makeUIInteractive;
