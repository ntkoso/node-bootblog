// @flow
import React, { Fragment, createRef } from 'react';
import { Link } from 'react-router-dom';
import Component from '@reactions/component';
import ProfileLinkTooltip from './Tooltip';

type Props = { username: string };

class ProfileLink extends React.Component<Props> {
  constructor(props: Props) {
    super(props);

    this.linkRef = createRef();
  }

  linkRef: { current: HTMLElement | null };

  render() {
    const { username, ...rest } = this.props;
    const { linkRef } = this;

    return (
      <Component initialState={{ on: false }}>
        {({ state: { on }, setState }) => {
          const toggle = () => setState({ on: !on });

          return (
            <Fragment>
              <Link
                to={`/users/${username}`}
                innerRef={linkRef}
                onMouseEnter={toggle}
                onMouseLeave={toggle}
                {...rest}
              />
              <ProfileLinkTooltip on={on} username={username} ref={linkRef} />
            </Fragment>
          );
        }}
      </Component>
    );
  }
}

export default ProfileLink;
