// @flow
import get from 'get-value';
import React, { Fragment } from 'react';
import type { Element } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import {
  Tooltip as BaseTooltip,
  Avatar,
} from '@ntkoso/btblg-style-guide/es/components';

const PROFILE_PREVIEW = gql`
  query profilePreview($username: String!) {
    user(username: $username) {
      id
      username
      profile {
        avatar {
          path
        }
      }
      threads {
        total
      }
    }
  }
`;

type Props = {
  on: boolean,
  username: string,
};

export const Tooltip = React.forwardRef(({ on, username }: Props, ref): Element<
  typeof BaseTooltip,
> => (
  <BaseTooltip active={on} side="top" target={ref.current}>
    <Query query={PROFILE_PREVIEW} variables={{ username }}>
      {({ data, loading }) =>
        !loading || (data && data.user) ? (
          <Fragment>
            <Avatar src="https://lorempixel.com/36/36" /> `${get(
              data,
              ['user', 'username'],
              { default: 'oops' },
            )}`
          </Fragment>
        ) : (
          `Loading - ${loading}`
        )
      }
    </Query>
  </BaseTooltip>
));

export default Tooltip;
