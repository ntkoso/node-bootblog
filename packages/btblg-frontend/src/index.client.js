// @flow
import '@babel/polyfill';
import ready from 'document-ready';
import loadPolyfills from './loadPolyfills';
import start from './app.client';

ready(() => loadPolyfills().then(() => start()));
