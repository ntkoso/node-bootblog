import '@babel/polyfill';
import 'isomorphic-fetch';
import http from 'http';
import Loadable from 'react-loadable';
import log from 'log';
import app from './app.server';

const appCallback = app.callback();
const server = http.createServer(appCallback);

Loadable.preloadAll().then(() => {
  server.listen(process.env.PORT, () => {
    log.info({ port: process.env.PORT }, 'listen');
  });
});
