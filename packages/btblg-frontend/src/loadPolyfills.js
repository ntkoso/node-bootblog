// @flow
const hasFetch = () => global && typeof global.fetch === 'function';

const hasIntersectionObserver = () =>
  global &&
  'IntersectionObserver' in global &&
  'IntersectionObserverEntry' in global &&
  'intersectionRatio' in global.IntersectionObserverEntry.prototype;

const loadPolyfills = () => {
  const polyfills = [];

  /* fetch */
  if (!hasFetch()) {
    polyfills.push(
      import(/* webpackChunkName: "polyfill-fetch" */ 'whatwg-fetch'),
    );
  }

  /* IntersectionObserver */
  if (hasIntersectionObserver()) {
    // Minimal polyfill for Edge 15's lack of `isIntersecting`
    // See: https://github.com/w3c/IntersectionObserver/issues/211
    if (!('isIntersecting' in global.IntersectionObserverEntry.prototype)) {
      Object.defineProperty(
        window.IntersectionObserverEntry.prototype,
        'isIntersecting',
        {
          get() {
            return this.intersectionRatio > 0;
          },
        },
      );
    }
  }

  if (!hasIntersectionObserver()) {
    polyfills.push(
      import(/* webpackChunkName: "polyfill-IntersectionObserver" */ 'intersection-observer'),
    );
  }

  return Promise.all(polyfills);
};

export default loadPolyfills;
