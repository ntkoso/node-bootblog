// @flow
import { addLocaleData } from 'react-intl';

type LocaleData = { locale: string, [key: string]: any };

const loadLocaleData = async (locale: string): Promise<void> => {
  const reactIntlLocale: LocaleData = await import(/* webpackChunkName: "react-intl-[request]" */ `react-intl/locale-data/${
    locale
  }`);

  addLocaleData(reactIntlLocale);
};

export default loadLocaleData;
