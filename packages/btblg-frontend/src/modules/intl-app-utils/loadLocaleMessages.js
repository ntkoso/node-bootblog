// @flow

const loadLocaleMessages = (
  locale: string,
): Promise<{ [id: string]: string }> =>
  import(/* webpackChunkName: "translations-[request]" */ `@ntkoso/btblg-locale-${
    locale
  }`);

export default loadLocaleMessages;
