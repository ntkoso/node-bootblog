// @flow
import pino from 'pino';

const logger = pino({
  name: 'frontend',
  level: process.env.DEBUG ? 'debug' : 'info',
  prettyPrint: process.env.NODE_ENV !== 'production',
  base: {
    pid: process.pid,
    application: 'btblg',
    package: '@ntkoso/btblg-frontend',
  },
});

export default logger;
