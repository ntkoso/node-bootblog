import React from 'react';
import { create } from 'react-test-renderer';
import rewriteLegacyLink from '../rewriteLegacyLink';

const TestComponent = ({ url }) => <a href={url} />;

test('rewrites address of a given prop', () => {
  const WrappedTestComponent = rewriteLegacyLink('url', 'http://test.url')(
    TestComponent,
  );

  const testElement = <WrappedTestComponent url="http://test.url/address" />;

  expect(create(testElement)).toMatchSnapshot('url gets rewritten');
});
