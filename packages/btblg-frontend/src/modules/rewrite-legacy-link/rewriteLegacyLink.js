// @flow
import React from 'react';
import type { Element, ComponentType } from 'react';
import { absoluteToRelativePath } from '@ntkoso/utils';

const startsWith = (start: string) => (prp: string): boolean =>
  Boolean(prp && typeof prp === 'string' && prp.startsWith(start));

const rewriteLegacyLink = (propName: string, startString: string) => (
  BaseComponent: ComponentType<any>,
) => (props: { [prop: string]: any }): Element<ComponentType<any>> => {
  const startsWithStartString = startsWith(startString);
  const newProps =
    props[propName] && startsWithStartString(props[propName])
      ? {
          ...props,
          [propName]: absoluteToRelativePath(props[propName], startString),
        }
      : props;

  return <BaseComponent {...newProps} />;
};

export default rewriteLegacyLink;
