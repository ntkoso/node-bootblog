import getXsrfToken from '../getXsrfToken';

jest.mock('js-cookie', () => ({
  get(cookieName) {
    return cookieName;
  },
}));

test('gets default xsrf token from cookies', () => {
  expect(getXsrfToken()).toEqual('xsrf_token');
});

test('gets custom xsrf token from cookies', () => {
  expect(getXsrfToken('custom_token')).toEqual('custom_token');
});
