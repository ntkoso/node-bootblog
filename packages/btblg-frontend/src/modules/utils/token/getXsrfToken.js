// @flow
import cookies from 'js-cookie';

const getXsrfToken = (key?: string = 'xsrf_token'): ?string => cookies.get(key);

export default getXsrfToken;
