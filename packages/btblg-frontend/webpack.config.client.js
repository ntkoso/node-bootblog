const path = require('path');
const webpack = require('webpack');
const WebpackMd5Hash = require('webpack-md5-hash');
const WebpackShellPlugin = require('webpack-shell-plugin');
const OfflinePlugin = require('offline-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const {
  bundleAnalyzer,
  brotli,
  gzip,
  inlineEnv,
  reactIntlLocalesPick,
  reactLoadableStats,
  stats,
} = require('@ntkoso/btblg-webpack-parts/lib/plugins');
const {
  minimizer,
  runtimeChunk,
} = require('@ntkoso/btblg-webpack-parts/lib/optimization');
const {
  js,
  gql,
  fonts,
  svgr,
} = require('@ntkoso/btblg-webpack-parts/lib/rules');
const { resolve } = require('@ntkoso/btblg-webpack-parts/lib/settings');
const babelrc = require('./.babelrc.client');
const packagejson = require('./package.json');

const isProduction = process.env.NODE_ENV === 'production';

const JS_OUTPUT_FILENAME = isProduction ? '[name].[chunkhash].js' : '[name].js';

const index = ['./src/index.client'];

let plugins = [
  reactIntlLocalesPick(),
  inlineEnv('client'),
  bundleAnalyzer(),
  stats(),
  reactLoadableStats({ filename: './public/assets/react-loadable.json' }),
  new FaviconsWebpackPlugin({
    emitStats: true,
    inject: false,
    logo: './src/common/assets/icon.png',
    prefix: 'favicon/',
    statsFilename: 'favicon-stats.json',
  }),
  new OfflinePlugin({
    excludes: ['stats.json', 'react-loadable.json'],
    ServiceWorker: { minify: false },
  }),
];

if (!isProduction) {
  plugins = [
    ...plugins,
    new WebpackShellPlugin({
      onBuildEnd: ['npm run build:server:dev'],
    }),
  ];
}

if (isProduction) {
  plugins = [
    ...plugins,
    new webpack.HashedModuleIdsPlugin(),
    new WebpackMd5Hash(),
    gzip(),
    brotli(),
  ];
}

module.exports = {
  devtool: false,
  mode: !isProduction ? 'development' : 'production',
  watch: !isProduction,
  entry: { index },
  target: 'web',
  module: {
    rules: [
      js({
        target: 'client',
        name: packagejson.name,
        babelrc,
      }),
      gql(),
      fonts(),
      svgr({ icon: true }),
    ],
  },
  plugins,
  optimization: {
    minimize: process.env.NODE_ENV === 'production',
    minimizer,
    runtimeChunk,
    splitChunks: {
      automaticNameDelimiter: '-',
    },
  },
  resolve: resolve(),
  output: {
    publicPath: '/assets/',
    path: path.join(__dirname, 'public', 'assets'),
    filename: JS_OUTPUT_FILENAME,
    chunkFilename: JS_OUTPUT_FILENAME,
  },
};
