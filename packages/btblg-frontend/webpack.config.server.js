const path = require('path');
const RestartServerPlugin = require('reload-server-webpack-plugin');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const {
  inlineEnv,
  reactIntlLocalesPick,
} = require('@ntkoso/btblg-webpack-parts/lib/plugins');
const {
  js,
  gql,
  fonts,
  svgr,
} = require('@ntkoso/btblg-webpack-parts/lib/rules');
const { resolve } = require('@ntkoso/btblg-webpack-parts/lib/settings');
const babelrc = require('./.babelrc');
const packagejson = require('./package.json');

const isProduction = process.env.NODE_ENV === 'production';

const entry = ['./src/index.server'];

const externalsWhitelist = [/@ntkoso/i, /react-loadable/i];

let plugins = [
  !isProduction
    ? new webpack.NamedModulesPlugin()
    : new webpack.HashedModuleIdsPlugin(),
  reactIntlLocalesPick(),
  inlineEnv('server'),
  new webpack.optimize.MinChunkSizePlugin({ minChunkSize: Infinity }),
];

if (!isProduction) {
  plugins = [
    ...plugins,
    new RestartServerPlugin({ script: './.build/index.js' }),
  ];
}

module.exports = {
  entry,
  mode: !isProduction ? 'development' : 'production',
  watch: !isProduction,
  target: 'node',
  externals: [
    nodeExternals({ whitelist: externalsWhitelist, modulesFromFile: true }),
  ],
  module: {
    rules: [
      js({
        target: 'server',
        name: packagejson.name,
        babelrc,
      }),
      gql(),
      fonts('../public/assets/'),
      svgr(),
    ],
  },
  plugins,
  optimization: {
    minimize: false,
  },
  resolve: resolve(),
  output: {
    path: path.join(__dirname, '.build'),
    filename: 'index.js',
  },
};
