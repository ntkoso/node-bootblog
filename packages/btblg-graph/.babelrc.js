const isProduction = process.env.NODE_ENV === 'production';
const isTest = process.env.NODE_ENV === 'test';
const moduleType = process.env.MODULE_ENV || false;

let presets = [
  [
    '@babel/preset-env',
    {
      debug: !isProduction,
      modules: isTest ? 'commonjs' : moduleType,
      targets: { node: true },
      useBuiltIns: 'usage',
    },
  ],
  '@babel/preset-react',
];

let plugins = [
  '@babel/plugin-syntax-flow',
  '@babel/plugin-syntax-class-properties',
  '@babel/plugin-syntax-object-rest-spread',
  '@babel/plugin-transform-flow-strip-types',
  '@babel/plugin-proposal-class-properties',
  '@babel/plugin-proposal-object-rest-spread',
];

if (isProduction) {
}

if (!isProduction) {
  plugins = ['@babel/plugin-transform-react-jsx-source', ...plugins];
}

module.exports = { presets, plugins };
