// flow-typed signature: d166df542d62361876fdd186ab751c5b
// flow-typed version: <<STUB>>/join-monster_v^2.0.3

/**
 * This is an autogenerated libdef stub for:
 *
 *   'join-monster'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'join-monster' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'join-monster/dist/alias-namespace' {
  declare module.exports: any;
}

declare module 'join-monster/dist/array-to-connection' {
  declare module.exports: any;
}

declare module 'join-monster/dist/batch-planner/index' {
  declare module.exports: any;
}

declare module 'join-monster/dist/define-object-shape' {
  declare module.exports: any;
}

declare module 'join-monster/dist/index' {
  declare module.exports: any;
}

declare module 'join-monster/dist/query-ast-to-sql-ast/index' {
  declare module.exports: any;
}

declare module 'join-monster/dist/resolve-unions' {
  declare module.exports: any;
}

declare module 'join-monster/dist/stringifiers/dialects/mariadb' {
  declare module.exports: any;
}

declare module 'join-monster/dist/stringifiers/dialects/mixins/pagination-not-supported' {
  declare module.exports: any;
}

declare module 'join-monster/dist/stringifiers/dialects/mysql' {
  declare module.exports: any;
}

declare module 'join-monster/dist/stringifiers/dialects/oracle' {
  declare module.exports: any;
}

declare module 'join-monster/dist/stringifiers/dialects/pg' {
  declare module.exports: any;
}

declare module 'join-monster/dist/stringifiers/dialects/sqlite3' {
  declare module.exports: any;
}

declare module 'join-monster/dist/stringifiers/dispatcher' {
  declare module.exports: any;
}

declare module 'join-monster/dist/stringifiers/shared' {
  declare module.exports: any;
}

declare module 'join-monster/dist/util' {
  declare module.exports: any;
}

// Filename aliases
declare module 'join-monster/dist/alias-namespace.js' {
  declare module.exports: $Exports<'join-monster/dist/alias-namespace'>;
}
declare module 'join-monster/dist/array-to-connection.js' {
  declare module.exports: $Exports<'join-monster/dist/array-to-connection'>;
}
declare module 'join-monster/dist/batch-planner/index.js' {
  declare module.exports: $Exports<'join-monster/dist/batch-planner/index'>;
}
declare module 'join-monster/dist/define-object-shape.js' {
  declare module.exports: $Exports<'join-monster/dist/define-object-shape'>;
}
declare module 'join-monster/dist/index.js' {
  declare module.exports: $Exports<'join-monster/dist/index'>;
}
declare module 'join-monster/dist/query-ast-to-sql-ast/index.js' {
  declare module.exports: $Exports<'join-monster/dist/query-ast-to-sql-ast/index'>;
}
declare module 'join-monster/dist/resolve-unions.js' {
  declare module.exports: $Exports<'join-monster/dist/resolve-unions'>;
}
declare module 'join-monster/dist/stringifiers/dialects/mariadb.js' {
  declare module.exports: $Exports<'join-monster/dist/stringifiers/dialects/mariadb'>;
}
declare module 'join-monster/dist/stringifiers/dialects/mixins/pagination-not-supported.js' {
  declare module.exports: $Exports<'join-monster/dist/stringifiers/dialects/mixins/pagination-not-supported'>;
}
declare module 'join-monster/dist/stringifiers/dialects/mysql.js' {
  declare module.exports: $Exports<'join-monster/dist/stringifiers/dialects/mysql'>;
}
declare module 'join-monster/dist/stringifiers/dialects/oracle.js' {
  declare module.exports: $Exports<'join-monster/dist/stringifiers/dialects/oracle'>;
}
declare module 'join-monster/dist/stringifiers/dialects/pg.js' {
  declare module.exports: $Exports<'join-monster/dist/stringifiers/dialects/pg'>;
}
declare module 'join-monster/dist/stringifiers/dialects/sqlite3.js' {
  declare module.exports: $Exports<'join-monster/dist/stringifiers/dialects/sqlite3'>;
}
declare module 'join-monster/dist/stringifiers/dispatcher.js' {
  declare module.exports: $Exports<'join-monster/dist/stringifiers/dispatcher'>;
}
declare module 'join-monster/dist/stringifiers/shared.js' {
  declare module.exports: $Exports<'join-monster/dist/stringifiers/shared'>;
}
declare module 'join-monster/dist/util.js' {
  declare module.exports: $Exports<'join-monster/dist/util'>;
}
