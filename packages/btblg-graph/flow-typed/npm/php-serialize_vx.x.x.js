// flow-typed signature: 41b1425e144eee78a3b957d5627d0344
// flow-typed version: <<STUB>>/php-serialize_v^1.0.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'php-serialize'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'php-serialize' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'php-serialize/lib/helpers' {
  declare module.exports: any;
}

declare module 'php-serialize/lib/index' {
  declare module.exports: any;
}

// Filename aliases
declare module 'php-serialize/lib/helpers.js' {
  declare module.exports: $Exports<'php-serialize/lib/helpers'>;
}
declare module 'php-serialize/lib/index.js' {
  declare module.exports: $Exports<'php-serialize/lib/index'>;
}
