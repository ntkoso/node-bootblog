// @flow
import Koa from 'koa';
import conditionalGet from 'koa-conditional-get';
import etag from 'koa-etag';
import { graphqlKoa } from 'graphql-server-koa';
import logger from 'koa-pino-logger';
import betterBody from 'koa-better-body';
import helmet from 'koa-helmet';
import convert from 'koa-convert';
import LRU from 'lru-cache';
import uuidV4 from 'uuid/v4';
import error from '@ntkoso/btblg-koa-error-react';
import intl from '@ntkoso/btblg-koa-react-intl';
import jwt from '@ntkoso/btblg-koa-jwt';
import { contextKey as resolverCacheKey } from '@ntkoso/graphql-custom-resolver-cache';
import LRUCacheProvider from '@ntkoso/graphql-custom-resolver-cache/es/providers/LRUCacheProvider';
import { serverIntlContext } from '@ntkoso/utils';
import log from 'log';
import TokenManager from './utils/token/TokenManager';
import { decrypt as decryptToken } from './utils/token/encryption/token';
import schema from './graph/schema';

const ACCESS_TOKEN_NAME = 'access_token';

const lru = new LRU();

const createGraphqlConfig = ctx => {
  const {
    request,
    state: {
      intl: { locale = 'en', messages = {} },
    },
  } = ctx;

  const intlContext = serverIntlContext({ locale, messages });
  const tokenManager = new TokenManager(ctx, {
    accessTokenKey: ACCESS_TOKEN_NAME,
  });

  return {
    schema,
    context: {
      [resolverCacheKey]: new LRUCacheProvider(lru),
      auth: {
        isAuthenticated: Boolean(tokenManager.getAccessToken()),
      },
      intl: intlContext,
      request,
      tokenManager,
    },
  };
};

const app = new Koa();

app.proxy = true;

app.use(
  logger({
    logger: log,
    genReqId: () => uuidV4(),
  }),
);

app.use(error());

app.use(conditionalGet());
app.use(etag());

app.use(convert(betterBody({ fields: 'body' })));

app.use(helmet.dnsPrefetchControl());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.referrerPolicy());
app.use(helmet.xssFilter());

app.use(intl());
app.use(
  jwt(process.env.SECRET, decryptToken, {
    passthrough: true,
    cookie: ACCESS_TOKEN_NAME,
  }),
);

app.use(graphqlKoa(createGraphqlConfig));

export default app;
