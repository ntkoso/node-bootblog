// @flow
import React from 'react';
import type { Node, Element } from 'react';
import type { formatMessage as formatMessageType } from 'react-intl';
import { Img } from 'oy-vey';
import tm from './messages';
import Layout from './Layout';
import Item from './Item';
import Span from './Span';

type MessageDescriptor = {
  id: string,
  description?: string,
  defaultMessage?: string,
};

type LogoProps = { hostname: string, publicPath: string, logo: string };
const Logo = ({
  hostname,
  publicPath,
  logo,
}: LogoProps): Element<typeof Item> => (
  <Item width="100%">
    <Img
      alt={`${hostname} logo`}
      src={`//${hostname}${publicPath}${logo}`}
      width={256}
      height={256}
    />
  </Item>
);

const Header = ({ children }: { children: Node }): Element<typeof Item> => (
  <Item align="center">
    <Span style={{ fontSize: 20, marginBottom: 15 }}>{children}</Span>
  </Item>
);

type MessageProps = {
  formatMessage: formatMessageType,
  username: string,
  hostname: string,
  sitename: MessageDescriptor,
  success: MessageDescriptor,
};
const Message = ({
  formatMessage,
  username,
  hostname,
  sitename,
  success,
}: MessageProps): Element<typeof Item> => (
  <Item>
    <Span style={{ fontSize: 16 }}>
      {formatMessage(success, { username })}{' '}
      <Span style={{ fontSize: 16, fontWeight: 'bold' }}>
        <a href={`//${hostname}`}>{formatMessage(sitename)}</a>
      </Span>
      {'.'}
    </Span>
  </Item>
);

type CallToActionProps = {
  formatMessage: formatMessageType,
  hostname: string,
  tokenLink: string,
  email: string,
  verify: MessageDescriptor,
  success: MessageDescriptor,
};
const CallToAction = ({
  formatMessage,
  hostname,
  tokenLink,
  email,
  verify,
  success,
}: CallToActionProps): Element<typeof Item> => (
  <Item>
    <Span style={{ fontSize: 16 }}>
      {formatMessage(success)}{' '}
      <Span style={{ fontSize: 16, fontWeight: 'bold' }}>
        <a href={`//${hostname}${tokenLink}`}>
          {formatMessage(verify, { email })}
        </a>
      </Span>
      {'.'}
    </Span>
  </Item>
);

type EmailConfirmationProps = {
  formatMessage: formatMessageType,
  email: string,
  hostname: string,
  publicPath: string,
  tokenLink: string,
  username: string,
};
export const EmailConfirmation = ({
  formatMessage,
  email,
  hostname,
  publicPath,
  tokenLink,
  username,
}: EmailConfirmationProps): Element<typeof Layout> => (
  <Layout>
    <Logo hostname={hostname} publicPath={publicPath} logo="" />
    <Header>{formatMessage(tm.siteName)}</Header>
    <Message
      formatMessage={formatMessage}
      username={username}
      hostname={hostname}
      sitename={tm.siteName}
      success={tm.emailConfirmationRegistrationSuccess}
    />
    <CallToAction
      formatMessage={formatMessage}
      hostname={hostname}
      tokenLink={tokenLink}
      email={email}
      verify={tm.verifyEmailLink}
      success={tm.emailConfirmationVerificationRequest}
    />
  </Layout>
);

export default EmailConfirmation;
