// @flow
import React from 'react';
import type { Node, Element } from 'react';
import { Table, TBody, TR, TD } from 'oy-vey';

export const Item = ({
  children,
  ...props
}: {
  children: Node,
}): Element<typeof Table> => (
  <Table {...props}>
    <TBody>
      <TR>
        <TD>{children}</TD>
      </TR>
    </TBody>
  </Table>
);

export default Item;
