// @flow
import React from 'react';
import type { Node, Element } from 'react';
import { Table, TBody, TR, TD } from 'oy-vey';

export const Layout = ({
  children,
}: {
  children: Node,
}): Element<typeof Table> => (
  <Table align="center" width="100%">
    <TBody>
      <TR>
        <TD align="center">
          <Table align="center" width="600">
            <TBody>
              <TR>
                <TD>{children}</TD>
              </TR>
            </TBody>
          </Table>
        </TD>
      </TR>
    </TBody>
  </Table>
);

export default Layout;
