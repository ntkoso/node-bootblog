// @flow
import React from 'react';
import type { Node, Element } from 'react';
import { Table, TBody, TR, TD } from 'oy-vey';

export const Span = ({
  children,
  ...props
}: {
  children: Node,
}): Element<typeof Table> => (
  <Table width="100%">
    <TBody width="100%">
      <TR>
        <TD align="center" {...props}>
          {children}
        </TD>
      </TR>
    </TBody>
  </Table>
);

export default Span;
