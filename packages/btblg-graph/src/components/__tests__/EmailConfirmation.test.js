import React from 'react';
import { create } from 'react-test-renderer';
import { EmailConfirmation } from '../EmailConfirmation';

test('basic render', () => {
  const layout = create(
    <EmailConfirmation
      formatMessage={({ message, defaultMessage }) =>
        message || defaultMessage || 'intl message'
      }
      email="email"
      hostname="hostname"
      publicPath="path"
      tokenLink="token"
      username="username"
    />,
  );

  expect(layout).toMatchSnapshot();
});
