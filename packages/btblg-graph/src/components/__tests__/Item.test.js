import React from 'react';
import { create } from 'react-test-renderer';
import { Item } from '../Item';

test('basic render', () => {
  const item = create(<Item>test</Item>);

  expect(item).toMatchSnapshot();
});

test('props render', () => {
  const item = create(<Item align="center">test</Item>);

  expect(item).toMatchSnapshot();
});
