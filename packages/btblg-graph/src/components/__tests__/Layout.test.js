import React from 'react';
import { create } from 'react-test-renderer';
import { Layout } from '../Layout';

test('basic render', () => {
  const layout = create(<Layout>test</Layout>);

  expect(layout).toMatchSnapshot();
});
