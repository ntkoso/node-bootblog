import React from 'react';
import { create } from 'react-test-renderer';
import { Span } from '../Span';

test('basic render', () => {
  const span = create(<Span>test</Span>);

  expect(span).toMatchSnapshot();
});

test('props render', () => {
  const span = create(
    <Span style={{ fontSize: 20, marginBottom: 20 }}>test</Span>,
  );

  expect(span).toMatchSnapshot();
});
