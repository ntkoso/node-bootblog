import { defineMessages } from 'react-intl';

const messages = defineMessages({
  siteName: {
    id: 'site_name',
    description: 'Site name, e.g. brand',
    defaultMessage: 'Bootblog',
  },
  loading: {
    id: 'loading',
    description: 'Loading message',
    defaultMessage: 'Loading...',
  },
  emailConfirmationSubject: {
    id: 'mail.email_confirmation.subject',
    description: 'Subject for email confirmation mail',
    defaultMessage: 'Email confirmation',
  },
  emailConfirmationRegistrationSuccess: {
    id: 'mail.email_confirmation.registration_success',
    description: 'Registration success text for email confirmation mail',
    defaultMessage: 'You successfully registered as {username} on',
  },
  emailConfirmationVerificationRequest: {
    id: 'mail.email_confirmation.verification_request',
    description: 'Text that requires a user to click on verifyEmail link',
    defaultMessage: 'To verify your email please click a link',
  },
  verifyEmailLink: {
    id: 'mail.email_confirmation.verify_email_link',
    description: 'Verify email link text',
    defaultMessage: 'Verify {email}',
  },
});

export default messages;
