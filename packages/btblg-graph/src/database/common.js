import { method } from './helpers';
import { create as createUser } from './users';
import { create as createProfile } from './profiles';

export const createUserWithProfile = method(
  async (trx, { profile, ...user }) => {
    const savedUser = await createUser(trx, user);
    const savedProfile = await createProfile(trx, {
      ...profile,
      user_id: savedUser.id,
    });

    return Promise.resolve({ ...savedUser, profile: savedProfile });
  },
);
