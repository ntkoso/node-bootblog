import castArray from 'cast-array';

const canonical = props => {
  const propsArray = castArray(props);

  return async data =>
    propsArray.filter(prop => data[prop]).reduce(
      (prev, prop) => ({
        ...prev,
        [`${prop}_canonical`]: prev[prop].toLowerCase(),
      }),
      data,
    );
};

export default canonical;
