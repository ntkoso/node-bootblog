const defaults = (props = {}) => async data =>
  Object.entries(props)
    .filter(([key]) => !data[key])
    .reduce(
      (prev, [key, value]) => ({
        ...prev,
        [key]: value,
      }),
      data,
    );

export default defaults;
