import phpSerialize from 'php-serialize';

const { serialize } = phpSerialize;

const format = () => async data => {
  const phpSerializeArray = arr => (Array.isArray(arr) ? serialize(arr) : arr);

  return Object.values(data).map(phpSerializeArray);
};

export default format;
