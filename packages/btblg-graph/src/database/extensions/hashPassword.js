import { encryptWithSalt } from '../../utils/token/encryption/password';

const hashPassword = (
  propName = 'password',
  saltPropName = 'salt',
) => async data => {
  if (data[propName]) {
    const { password, salt } = await encryptWithSalt(data[propName]);

    return {
      ...data,
      [propName]: password,
      [saltPropName]: salt,
    };
  }

  return data;
};

export default hashPassword;
