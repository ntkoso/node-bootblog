import castArray from 'cast-array';
import phpSerialize from 'php-serialize';

const { unserialize } = phpSerialize;

const parse = fields => {
  const fieldsArray = castArray(fields);

  return async data =>
    fieldsArray.filter(field => data[field]).reduce(
      (prev, field) => ({
        ...prev,
        [field]: unserialize(prev[field]),
      }),
      data,
    );
};

export default parse;
