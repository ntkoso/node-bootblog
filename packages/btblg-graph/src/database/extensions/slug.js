import castArray from 'cast-array';
import getSlug from 'speakingurl';

const slug = props => {
  const propsArray = castArray(props);

  return async data => {
    const str = propsArray.reduce((prev, prop) => `${prev} ${data[prop]}`, '');

    return {
      ...data,
      slug: getSlug(str),
    };
  };
};

export default slug;
