const timestamp = (
  insert = true,
  { createdAtKey = 'created_at', updatedAtKey = 'updated_at' } = {},
) => async data => {
  const now = Date.now();

  return insert
    ? {
        ...data,
        [createdAtKey]: now,
        [updatedAtKey]: now,
      }
    : {
        ...data,
        [updatedAtKey]: now,
      };
};

export default timestamp;
