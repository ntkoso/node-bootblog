// @flow
import joinMonster from 'join-monster';
import knex from './knex';

const jm = <RI, C>(resolveInfo: RI, context?: C) =>
  joinMonster(resolveInfo, context || {}, sql => knex.raw(sql), {
    dialect: process.env.DB_CLIENT || 'pg',
  });

export default jm;
