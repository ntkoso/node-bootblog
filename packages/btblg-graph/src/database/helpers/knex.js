import knexConstructor from 'knex';

const knex = knexConstructor({
  client: process.env.DB_CLIENT,
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
  },
  migrations: {
    tableName: process.env.DB_MIGRATIONS_TABLE_NAME || 'knex_migrations',
  },
});

export default knex;
