import knexlessMethod from 'knexed/tx/method';
import knex from './knex';

const method = fn => knexlessMethod(knex, fn);

export default method;
