import knexlessTable from 'knexed/table/table';
import knex from './knex';

const table = (...args) => knexlessTable(knex, ...args);

export default table;
