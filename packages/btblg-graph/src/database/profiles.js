import { method, table } from './helpers';
import { format, timestamp } from './extensions';

const profiles = table('boot_profiles');

const onCreate = async profile => {
  const timestamped = await timestamp()(profile);
  const formatted = await format()(timestamped);

  return formatted;
};

export const create = method(async (trx, profile) => {
  const data = await onCreate(profile);

  return Promise.resolve(data);
});

export default profiles;
