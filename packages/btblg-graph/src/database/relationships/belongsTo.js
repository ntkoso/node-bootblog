// @flow
type BelongsTo = (key: string) => (child: string, parent: string) => string;
const belongsTo: BelongsTo = key => (child, parent) =>
  `${child}.${key} = ${parent}.id`;

export default belongsTo;
