// @flow
import hasMany from './hasMany';
import belongsTo from './belongsTo';

type BelongsToMany = (parentKey: string, relationshipKey: string) => [*, *];
const belongsToMany: BelongsToMany = (parentKey, relationshipKey) => [
  hasMany(parentKey),
  belongsTo(relationshipKey),
];

export default belongsToMany;
