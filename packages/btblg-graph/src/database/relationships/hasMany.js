// @flow
type HasMany = (key: string) => (parent: string, child: string) => string;
const hasMany: HasMany = key => (parent, child) =>
  `${parent}.id = ${child}.${key}`;

export default hasMany;
