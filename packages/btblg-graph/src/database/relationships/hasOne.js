// @flow
import hasMany from './hasMany';

const hasOne = hasMany;

export default hasOne;
