import { method, table } from './helpers';
import { canonical, defaults, format, hashPassword } from './extensions';

const users = table('boot_users');

const onEnter = async user => {
  const defaulted = await defaults({
    enabled: false,
    locked: false,
    roles: [],
  })(user);

  const canonized = await canonical(['username', 'email'])(defaulted);

  const hashed = await hashPassword()(canonized);
  const formatted = await format()(hashed);

  return formatted;
};

export const create = method(async (trx, user) => {
  const data = await onEnter(user);

  return Promise.resolve(data); // users(trx).insert(data);
});

export const update = method(async (trx, user) => Promise.resolve(user));

export default users;
