// @flow
import { GraphQLObjectType } from 'graphql';
import { nodeField as node } from './nodeDefinitions';
import * as queries from './queries';

const Query = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({ node, ...queries }),
});

export default Query;
