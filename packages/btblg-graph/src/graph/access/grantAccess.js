// @flow
import type { AccessTypes } from './types';

const grantAccess = (
  access: Array<AccessTypes>,
  __typenames: Array<string>,
): { [__typename: string]: Array<AccessTypes> } =>
  __typenames.reduce(
    (prev, __typename) => ({ ...prev, [__typename]: access }),
    {},
  );

export default grantAccess;
