// @flow
import grantAccess from './grantAccess';
import type { AccessTypes } from './types';

const fullAccess = ['*'];
const defaultAccess = ['create', 'read', 'delete:own', 'update:own'];

const grantAccess = (
  access: Array<AccessTypes>,
  __typenames: Array<string>,
): { [__typename: string]: Array<AccessTypes> } =>
  __typenames.reduce(
    (prev, __typename) => ({ ...prev, [__typename]: access }),
    {},
  );

const access = {
  admin: grantAccess(fullAccess, [
    'Thread',
    'Comment',
    'Category',
    'User',
    'Question',
    'Answer',
  ]),

  user: grantAccess(defaultAccess, [
    'Thread',
    'Comment',
    'Category',
    'User',
    'Question',
    'Answer',
  ]),
};

export default access;
