Fullstack (Node.js + React) разработчик.


Примерно 2 года опыта React.js и его экосистемы,
до этого занимался классическим сервер сайдом на PHP и немного Ruby on Rails

Из технологий уверенное знание:

Типизированный Javascript (Typescript и Flow)
Flow используется Facebook'ом во всех их проектах
Но экосистема начинает мигрировать на Typescript

Node.js

React
Redux
Redux-saga
Apollo-client (React клиент для GraphQL)
Babel
Webpack
тесты (jest, mocha + sinon)

HTML5

CSS:
BEM
SCSS
CSSModules
CSS-in-JS (https://www.styled-components.com/ и https://emotion.sh/)

Backend:
REST
GraphQL (https://graphql.org/)
Server side rendering
Express \ Koa (и другие middleware фреймворки)
ORM и SQL (mysql, postgres)


Опыт работы с git
Опыт настройки nginx
Знание MVC архитектуры на сервер сайде.
Некоторое понимание microservice архитектуры (не было возможности использовать на практике)

8+ лет с ОС Linux

Свободное владение английским языком
Свободное чтение технической документации \ статей
Свободное общение с зарубежными разработчиками, включая авторов библиотек
и разбор новых патчей \ пуллреквестов \ вопросы в официальном дискорде и слаке

Как хобби смотрю в сторону Rust (https://www.rust-lang.org/en-US/)
для интеграции WASM модулей на фронэнде и бэкэнде в будущем.