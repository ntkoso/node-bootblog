// @flow
import { GraphQLNonNull, GraphQLString } from 'graphql';
import { createMutation } from '../utils';
import UserType from '../types/User';
import { confirmEmail as confirmEmailResolver } from '../resolvers';

const confirmEmail = createMutation({
  name: 'ConfirmEmail',
  inputFields: {
    confirmationToken: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  outputFields: {
    user: {
      type: UserType,
      resolve: ({ user }) => user,
    },
  },
  mutateAndGetPayload: confirmEmailResolver,
});

export default confirmEmail;
