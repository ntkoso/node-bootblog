// @flow
import { createMutation } from '../utils';
import refreshTokenResolver from '../resolvers/refreshToken';

const refreshToken = createMutation({
  name: 'RefreshToken',
  mutateAndGetPayload: refreshTokenResolver,
});

export default refreshToken;
