// @flow
import { GraphQLString, GraphQLInt, GraphQLNonNull } from 'graphql';
import { createMutation } from '../utils';
import UserType from '../types/User';
import signInResolver from '../resolvers/signIn';

const signIn = createMutation({
  name: 'SignIn',
  inputFields: {
    email: {
      type: new GraphQLNonNull(GraphQLString),
    },
    password: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  outputFields: {
    accessTokenTTL: {
      type: GraphQLInt,
      description:
        'Access token ttl, used to kill token if user was offline for a long time',
    },
    accessTokenRefreshTTL: {
      type: GraphQLInt,
      description:
        'Access token refresh ttl, used to automatically prolong token expiration time',
    },
    user: {
      type: UserType,
      resolve: ({ user }) => user,
    },
  },
  mutateAndGetPayload: signInResolver,
});

export default signIn;
