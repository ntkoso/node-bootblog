// @flow
import { createMutation } from '../utils';
import signOutResolver from '../resolvers/signOut';

const signOut = createMutation({
  name: 'SignOut',
  mutateAndGetPayload: signOutResolver,
});

export default signOut;
