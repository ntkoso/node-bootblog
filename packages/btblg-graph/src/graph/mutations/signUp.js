// @flow
import { GraphQLString, GraphQLNonNull } from 'graphql';
import { createMutation } from '../utils';
import UserType from '../types/User';
import signUpResolver from '../resolvers/signUp';

const signUp = createMutation({
  name: 'SignUp',
  inputFields: {
    email: {
      type: new GraphQLNonNull(GraphQLString),
    },
    username: {
      type: new GraphQLNonNull(GraphQLString),
    },
    password: {
      type: new GraphQLNonNull(GraphQLString),
    },
    passwordConfirmation: {
      type: new GraphQLNonNull(GraphQLString),
    },
    recaptcha: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  outputFields: {
    user: {
      type: UserType,
      resolve: ({ user }) => user,
    },
  },
  mutateAndGetPayload: signUpResolver,
});

export default signUp;
