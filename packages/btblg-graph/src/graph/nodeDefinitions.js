import { getNode } from 'join-monster';
import { nodeDefinitions, fromGlobalId } from 'graphql-relay';
import { knex } from '../database/helpers';

export const { nodeInterface, nodeField } = nodeDefinitions(
  (globalId, context, info) => {
    const { id, type } = fromGlobalId(globalId);

    return getNode(type, info, context, parseInt(id, 10), sql => knex.raw(sql));
  },
  ({ __type__ }) => __type__,
);
