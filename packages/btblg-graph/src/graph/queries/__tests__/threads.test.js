import threads from '../threads';

jest.mock('knex', () => () => ({ raw: str => str }));

const { where, args } = threads;

test('has correct arguments', () => {
  expect(args).toMatchSnapshot('arguments');
});

test('has where property', () => {
  expect(where).toBeInstanceOf(Function);
});

test('title SQL WHERE query', () => {
  expect(where('test', { title: 'Test' })).toMatchSnapshot('title');
});

test('username SQL WHERE query', () => {
  expect(where('test', { username: 'TestUser' })).toMatchSnapshot('username');
});

test('category SQL WHERE query', () => {
  expect(where('test', { category: 'TestCategory' })).toMatchSnapshot(
    'category',
  );
});

test('tags SQL WHERE query', () => {
  expect(where('test', { tags: ['news', 'v8', 'node'] })).toMatchSnapshot(
    'tags',
  );
});

test('dateRange SQL WHERE query', () => {
  expect(
    where('test', {
      dateRange: [new Date(2018, 12, 1).toISOString(), null],
    }),
  ).toMatchSnapshot('>=');

  expect(
    where('test', {
      dateRange: [null, new Date(2018, 12, 12).toISOString()],
    }),
  ).toMatchSnapshot('<=');

  expect(
    where('test', {
      dateRange: [
        new Date(2018, 12, 1).toISOString(),
        new Date(2018, 12, 12).toISOString(),
      ],
    }),
  ).toMatchSnapshot('>= and <=');
});

test('multiple arguments SQL WHERE query', () => {
  expect(
    where('test', {
      title: 'Test',
      username: 'TestUser',
      category: 'TestCategory',
      dateRange: [
        new Date(2018, 12, 1).toISOString(),
        new Date(2018, 12, 12).toISOString(),
      ],
    }),
  ).toMatchSnapshot('multiple arguments');
});
