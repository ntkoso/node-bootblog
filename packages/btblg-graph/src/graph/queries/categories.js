// @flow
import {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
} from 'graphql';
import { forwardConnectionArgs } from 'graphql-relay';
import pgEscape from 'pg-escape';
import { createQuery, toOrderByFields } from '../utils';
import CategoriesConnection from '../types/CategoriesConnection';

const categories = createQuery({
  type: CategoriesConnection,
  args: {
    ...forwardConnectionArgs,
    orderBy: {
      type: GraphQLString,
    },
    search: {
      type: GraphQLString,
    },
  },
  sqlPaginate: true,
  orderBy: toOrderByFields,
  where: (table, { search }) =>
    typeof search === 'string' &&
    search !== '' &&
    pgEscape(`${table}.title LIKE %%%L%%`, search),
});

export default categories;
