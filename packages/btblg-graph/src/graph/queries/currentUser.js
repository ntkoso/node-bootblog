// @flow
import { fromGlobalId } from 'graphql-relay';
import users from '../../database/users';
import { createQuery } from '../utils';
import UserType from '../types/User';

const currentUser = createQuery({
  type: UserType,
  resolve: async (_, __, { tokenManager }) => {
    const accessToken = tokenManager.getAccessToken();

    console.log(accessToken);

    if (!accessToken) {
      return null;
    }

    const { id } = fromGlobalId(accessToken.sub);

    const [user] = await users()
      .select()
      .where({ id })
      .limit(1);

    return user;
  },
});

export default currentUser;
