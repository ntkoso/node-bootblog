// @flow
import { GraphQLID, GraphQLString } from 'graphql';
import pgEscape from 'pg-escape';
import { createQuery } from '../utils';
import ThreadType from '../types/Thread';

type ThreadArgs = { id?: string, slug?: string };

const thread = createQuery({
  type: ThreadType,
  args: {
    id: { name: 'id', type: GraphQLID },
    slug: { name: 'slug', type: GraphQLString },
  },
  sqlPaginate: true,
  where: (table: string, { slug }: ThreadArgs) =>
    typeof slug === 'string' &&
    slug !== '' &&
    pgEscape(`${table}.slug = %L`, slug),
});

export default thread;
