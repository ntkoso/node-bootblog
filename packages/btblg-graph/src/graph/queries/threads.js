// @flow
import { GraphQLID, GraphQLString, GraphQLList } from 'graphql';
import { GraphQLDate } from 'graphql-iso-date';
import { forwardConnectionArgs } from 'graphql-relay';
import pgEscape from 'pg-escape';
import { knex } from '../../database/helpers';
import { createQuery, toOrderByFields } from '../utils';
import ThreadType from '../types/Thread';
import ThreadsConnection from '../types/ThreadsConnection';

type ThreadsArgs = {
  title?: string,
  username?: string,
  category?: string,
  tags?: Array<string>,
  dateRange?: [string, string],
};

const isEmptyString = (str: ?string): boolean =>
  typeof str === 'string' || str !== '';

const isEmptyArray = (arr: ?Array<string> | any): boolean =>
  Array.isArray(arr) && arr.length > 0;

const threads = createQuery({
  type: ThreadsConnection,
  args: {
    ...forwardConnectionArgs,
    orderBy: {
      type: GraphQLString,
    },
    title: { name: 'title', type: GraphQLString },
    username: { name: 'username', type: GraphQLString },
    category: { name: 'category', type: GraphQLString },
    tags: { name: 'tags', type: new GraphQLList(GraphQLString) },
    dateRange: { name: 'dateRange', type: new GraphQLList(GraphQLDate) },
  },
  sqlPaginate: true,
  orderBy: toOrderByFields,
  where: (
    table: string,
    { title, username, category, tags, dateRange }: ThreadsArgs,
  ) => {
    const result = [];

    if (!isEmptyString(title)) {
      result.push(pgEscape('%I.%I LIKE %%%L%%', table, 'title', title));
    }

    if (!isEmptyString(username)) {
      result.push(
        pgEscape(
          '%I.%I IN (SELECT id FROM %I WHERE username_canonical = %L)',
          table,
          'author_id',
          'boot_users',
          username.toLowerCase(),
        ),
      );
    }

    if (!isEmptyString(category)) {
      result.push(
        pgEscape(
          '%I.%I IN (SELECT id FROM %I WHERE title = %L)',
          table,
          'category_id',
          'boot_categories',
          category,
        ),
      );
    }

    if (!isEmptyArray(tags)) {
      result.push(
        pgEscape(
          '%I.%I IN (SELECT thread_id FROM %I WHERE tag_id IN (SELECT id FROM %I WHERE body IN %L))',
          table,
          'thread_id',
          'boot_threads_tags',
          'boot_tags',
          tags,
        ),
      );
    }

    if (!isEmptyArray(dateRange)) {
      const [start, end] = dateRange;

      if (start && end) {
        result.push(
          pgEscape(
            '%I.%I >= %L AND %I.%I <= %L',
            table,
            'created_at',
            start,
            table,
            'created_at',
            end,
          ),
        );
      }

      if (start && !end) {
        result.push(pgEscape('%I.%I >= %L', table, 'created_at', dateRange[0]));
      }

      if (end && !start) {
        result.push(pgEscape('%I.%I <= %L', table, 'created_at', dateRange[1]));
      }
    }

    return result.join(' AND ');
  },
});

export default threads;
