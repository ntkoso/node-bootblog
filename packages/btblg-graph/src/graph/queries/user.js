// @flow
import { GraphQLID, GraphQLString } from 'graphql';
import pgEscape from 'pg-escape';
import { createQuery } from '../utils';
import UserType from '../types/User';

type UserArgs = {
  id?: string,
  username?: string,
};

const isEmptyString = (param: string) =>
  typeof param === 'string' || param !== '';

const user = createQuery({
  type: UserType,
  args: {
    id: { name: 'id', type: GraphQLID },
    username: { name: 'username', type: GraphQLString },
  },
  /* where: (table: string, { id, username }: UserArgs) => {
    console.log(table);
    if (!isEmptyString(id)) {
      return pgEscape(`%I.%I = %L`, table, 'id', id);
    }

    if (!isEmptyString(username)) {
      return pgEscape(
        `%I.%I = %L`,
        'boot_users',
        'username_canonical',
        username.toLowerCase(),
      );
    }

    return null;
  }, */
});

export default user;
