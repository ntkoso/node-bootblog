// @flow
import { GraphQLString } from 'graphql';
import { forwardConnectionArgs } from 'graphql-relay';
import { createQuery, toOrderByFields } from '../utils';
import UsersConnection from '../types/UsersConnection';

const users = createQuery({
  type: UsersConnection,
  args: {
    ...forwardConnectionArgs,
    orderBy: {
      type: GraphQLString,
    },
  },
  sqlPaginate: true,
  orderBy: toOrderByFields,
});

export default users;
