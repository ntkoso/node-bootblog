import { validate, validationError } from '@ntkoso/btblg-validations';
import { confirmEmail as confirmEmailSchema } from '@ntkoso/btblg-validations/es/mutations';
import users, { update as updateUser } from '../../database/users';

const confirmEmail = async input => {
  const { result, errors } = await validate(confirmEmailSchema, input);

  if (errors) {
    return { errors };
  }

  const { confirmationToken } = result;

  let [user] = await users()
    .select()
    .where({ confirmation_token: confirmationToken })
    .limit(1);

  if (!user) {
    return {
      errors: [
        validationError(
          'auth.email_confirmation.invalid_token',
          null,
          'confirmationToken',
        ),
      ],
    };
  }

  user = await updateUser({ ...user, active: true, confirmation_token: null });

  return { user };
};

export default confirmEmail;
