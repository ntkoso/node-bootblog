import uuidV4 from 'uuid/v4';
import { encrypt as encryptToken } from '../../utils/token/encryption/token';

const refreshToken = async (_, { tokenManager }) => {
  if (!tokenManager.hasValidXsrfToken()) {
    throw new Error('Invalid XsrfToken');
  }

  const accessToken = tokenManager.getAccessToken();

  const newXsrfToken = uuidV4();
  const newExpiration = accessToken.exp + Number(3600 * 1000);

  const refreshedAccessToken = {
    ...accessToken,
    xsrfToken: newXsrfToken,
    exp: newExpiration,
  };

  const encryptedToken = await encryptToken(
    refreshedAccessToken,
    process.env.SECRET,
  );

  tokenManager.setTokenCookies(encryptedToken, newXsrfToken);

  return {}; // mutation is required to return an object
};

export default refreshToken;
