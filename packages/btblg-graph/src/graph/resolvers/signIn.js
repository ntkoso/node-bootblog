import { compare } from 'bcrypt';
import uuidV4 from 'uuid/v4';
import { toGlobalId } from 'graphql-relay';
import { signIn as signInSchema } from '@ntkoso/btblg-validations/es/mutations';
import { validate, validationError } from '@ntkoso/btblg-validations';
import users, { update as updateUser } from '../../database/users';
import { encrypt as encryptToken } from '../../utils/token/encryption/token';
import { compare as compareSymfony2 } from '../../utils/token/encryption/symfony2';
import generateCredentials from '../../utils/token/generateCredentials';

const signIn = async (input, { tokenManager, request }) => {
  const { result, errors } = await validate(signInSchema, input);

  if (errors) return { errors };

  const { email, password } = result;

  const [registeredUser] = await users()
    .select()
    .where({ email })
    .limit(1);

  if (!registeredUser) {
    return {
      errors: [
        validationError('validation.email.not_registered', null, 'email'),
      ],
    };
  }

  const registeredPassword = registeredUser.password.toString();

  const passwordIsValid = await compare(password, registeredPassword);

  if (!passwordIsValid) {
    if (!compareSymfony2(password, registeredPassword, registeredUser.salt)) {
      return {
        errors: [
          validationError('validation.password.invalid', null, 'password'),
        ],
      };
    }
  }

  const user = await updateUser({ ...registeredUser, last_login: Date.now() });

  const xsrfToken = uuidV4();

  const credentials = generateCredentials(
    request.hostname,
    toGlobalId('User', user.id),
    user.roles,
    xsrfToken,
  );

  const token = await encryptToken(credentials, process.env.SECRET);

  tokenManager.setTokenCookies(token, xsrfToken);

  return {
    accessTokenTTL: 604800 * 1000,
    accessTokenRefreshTTL: 3600 * 1000,
    user,
  };
};

export default signIn;
