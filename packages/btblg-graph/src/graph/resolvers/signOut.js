// @flow

const signOut = async (_, { tokenManager }) => {
  tokenManager.clearTokenCookies();

  return {};
};

export default signOut;
