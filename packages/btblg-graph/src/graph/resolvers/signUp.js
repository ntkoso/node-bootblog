// @flow
import React from 'react';
import { signUp as signUpSchema } from '@ntkoso/btblg-validations/es/mutations';
import { validate, validationError } from '@ntkoso/btblg-validations';
import { encrypt as encryptText } from 'utils/token/encryption/text';
import users from 'database/users';
import { createUserWithProfile } from 'database/common';
import { EmailConfirmation, messages as tm } from 'components';

const pickSignUpFields = ({ email, username, password }) => ({
  email,
  username,
  password,
});

// TODO: send confirmation email using mailgun-js
const signUp = async (input, { request, intl }) => {
  const { result, errors } = await validate(signUpSchema, input);

  if (errors) {
    return { errors };
  }

  const { email, username } = result;

  const [userWithEmail] = await users()
    .select('id')
    .where({ email })
    .limit(1);

  if (userWithEmail) {
    return {
      errors: [
        validationError('validation.email.already_in_use', null, 'email'),
      ],
    };
  }

  const [userWithUsername] = await users()
    .select('id')
    .where({ username })
    .limit(1);

  if (userWithUsername) {
    return {
      errors: [
        validationError('validation.username.already_in_use', null, 'username'),
      ],
    };
  }

  let user = pickSignUpFields(result);

  /*
  const confirmationToken = await encryptText(
    `${JSON.stringify(user)}:${Date.now()}`,
    process.env.SECRET,
  );
  */

  try {
    user = await createUserWithProfile(user);
  } catch (e) {
    return {
      errors: [validationError(e.message)],
    };
  }

  /*
  const subject = intl.formatMessage(tm.emailConfirmationSubject);
  const siteName = intl.formatMessage(tm.siteName);
  const { hostname } = request;

  const html = renderEmail(
    <EmailConfirmation
      email={user.email}
      formatMessage={intl.formatMessage}
      hostname={hostname}
      publicPath={'/assets/'}
    prefix={process.env.GRAPHQL_PREFIX || '/graphql'}
      subject={subject}
      tokenLink={`/email-confirmation/${confirmationToken}`}
      username={user.username}
    />,
  );

  try {
    await nodemailerInstance.sendMail({
      from: {
        name: siteName,
        address: `noreply@${hostname}`,
      },
      to: email,
      subject,
      html,
    });
  } catch (err) {
    console.error(err);
  }
  */

  return { user };
};

export default signUp;
