import { GraphQLObjectType, GraphQLString } from 'graphql';
import { globalIdField } from 'graphql-relay';
import { GraphQLDateTime } from 'graphql-iso-date';
import { nodeInterface } from '../nodeDefinitions';
import { belongsTo } from '../../database/relationships';
import UserType from './User';
import QuestionType from './Question';

const Answer = new GraphQLObjectType({
  name: 'Answer',
  description: 'Answer for question',
  sqlTable: 'boot_answers',
  uniqueKey: 'id',
  fields: () => ({
    id: {
      ...globalIdField(),
      sqlColumn: 'id',
    },
    body: {
      type: GraphQLString,
    },
    author: {
      type: UserType,
      sqlJoin: belongsTo('author_id'),
    },
    question: {
      type: QuestionType,
      sqlJoin: belongsTo('question_id'),
    },
    createdAt: {
      type: GraphQLDateTime,
      sqlColumn: 'created_at',
    },
    updatedAt: {
      type: GraphQLDateTime,
      sqlColumn: 'updated_at',
    },
  }),
  interfaces: [nodeInterface],
});

export default Answer;
