import { createConnection } from '../utils';
import AnswerType from './Answer';

const AnswersConnection = createConnection({
  name: 'Answers',
  nodeType: AnswerType,
});

export default AnswersConnection.connectionType;
