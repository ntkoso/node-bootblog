import { createConnection } from '../utils';
import CategoryType from './Category';

const CategoriesConnection = createConnection({
  name: 'Categories',
  nodeType: CategoryType,
});

export default CategoriesConnection.connectionType;
