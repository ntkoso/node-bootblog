import { GraphQLObjectType, GraphQLString } from 'graphql';
import { globalIdField, forwardConnectionArgs } from 'graphql-relay';
import { hasMany } from '../../database/relationships';
import ThreadsConnection from './ThreadsConnection';
import { nodeInterface } from '../nodeDefinitions';

const Category = new GraphQLObjectType({
  name: 'Category',
  sqlTable: 'boot_categories',
  uniqueKey: 'id',
  fields: () => ({
    id: {
      ...globalIdField(),
      sqlColumn: 'id',
    },
    title: {
      type: GraphQLString,
    },
    slug: {
      type: GraphQLString,
    },
    threads: {
      type: ThreadsConnection,
      args: forwardConnectionArgs,
      sqlJoin: hasMany('category_id'),
    },
  }),
  interfaces: [nodeInterface],
});

export default Category;
