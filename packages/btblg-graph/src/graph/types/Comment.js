import { GraphQLObjectType, GraphQLString } from 'graphql';
import { globalIdField } from 'graphql-relay';
import { GraphQLDateTime } from 'graphql-iso-date';
import { nodeInterface } from '../nodeDefinitions';
import { belongsTo } from '../../database/relationships';
import UserType from './User';
import ThreadType from './Thread';

const Comment = new GraphQLObjectType({
  name: 'Comment',
  sqlTable: 'boot_comments',
  uniqueKey: 'id',
  fields: () => ({
    id: {
      ...globalIdField(),
      sqlColumn: 'id',
    },
    body: {
      type: GraphQLString,
    },
    author: {
      type: UserType,
      sqlJoin: belongsTo('author_id'),
    },
    thread: {
      type: ThreadType,
      sqlJoin: belongsTo('thread_id'),
    },
    createdAt: {
      type: GraphQLDateTime,
      sqlColumn: 'created_at',
    },
    updatedAt: {
      type: GraphQLDateTime,
      sqlColumn: 'updated_at',
    },
  }),
  interfaces: [nodeInterface],
});

export default Comment;
