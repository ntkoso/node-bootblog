import { createConnection } from '../utils';
import CommentType from './Comment';

const CommentsConnection = createConnection({
  name: 'Comments',
  nodeType: CommentType,
});

export default CommentsConnection.connectionType;
