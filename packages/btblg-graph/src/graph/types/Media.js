import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
  GraphQLInt,
} from 'graphql';
import { globalIdField } from 'graphql-relay';
import { GraphQLDateTime } from 'graphql-iso-date';
import { nodeInterface } from '../nodeDefinitions';

const MediaType = new GraphQLObjectType({
  name: 'Media',
  description: 'Media object, image, video, audio',
  sqlTable: 'boot_media',
  uniqueKey: 'id',
  fields: () => ({
    id: {
      ...globalIdField(),
      sqlColumn: 'id',
    },
    path: {
      type: GraphQLString,
    },
    size: {
      type: GraphQLInt,
    },
    mimeType: {
      type: GraphQLString,
    },
    deleted: {
      type: GraphQLBoolean,
    },
    createdAt: {
      type: GraphQLDateTime,
      sqlColumn: 'created_at',
    },
    updatedAt: {
      type: GraphQLDateTime,
      sqlColumn: 'updated_at',
    },
  }),
  interfaces: [nodeInterface],
});

export default MediaType;
