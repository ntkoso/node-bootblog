import { GraphQLObjectType } from 'graphql';
import { globalIdField } from 'graphql-relay';
import { GraphQLDateTime } from 'graphql-iso-date';
import { belongsTo } from '../../database/relationships';
import { nodeInterface } from '../nodeDefinitions';
import MediaType from './Media';
import UserType from './User';

const Profile = new GraphQLObjectType({
  name: 'Profile',
  description: 'User profile',
  sqlTable: 'boot_profiles',
  uniqueKey: 'id',
  fields: () => ({
    id: {
      ...globalIdField(),
      sqlColumn: 'id',
    },
    avatar: {
      type: MediaType,
      sqlJoin: belongsTo('avatar_id'),
    },
    user: {
      type: UserType,
      sqlJoin: belongsTo('user_id'),
    },
    createdAt: {
      type: GraphQLDateTime,
      sqlColumn: 'created_at',
    },
    updatedAt: {
      type: GraphQLDateTime,
      sqlColumn: 'updated_at',
    },
  }),
  interfaces: [nodeInterface],
});

export default Profile;
