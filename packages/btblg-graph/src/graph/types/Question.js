import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
  GraphQLInt,
} from 'graphql';
import { globalIdField, forwardConnectionArgs } from 'graphql-relay';
import { GraphQLDateTime } from 'graphql-iso-date';
import { belongsTo, hasMany } from '../../database/relationships';
import { nodeInterface } from '../nodeDefinitions';
import UserType from './User';
import AnswersConnection from './AnswersConnection';

const Question = new GraphQLObjectType({
  name: 'Question',
  description:
    'Question that user could ask about anything ang get an answer from admin',
  sqlTable: 'boot_questions',
  uniqueKey: 'id',
  fields: () => ({
    id: {
      ...globalIdField(),
      sqlColumn: 'id',
    },
    title: {
      type: GraphQLString,
    },
    body: {
      type: GraphQLString,
    },
    resolved: {
      type: GraphQLBoolean,
    },
    views: {
      type: GraphQLInt,
    },
    slug: {
      type: GraphQLString,
    },
    author: {
      type: UserType,
      sqlJoin: belongsTo('author_id'),
    },
    anwers: {
      type: AnswersConnection,
      args: forwardConnectionArgs,
      sqlJoin: hasMany('question_id'),
    },
    createdAt: {
      type: GraphQLDateTime,
      sqlColumn: 'created_at',
    },
    updatedAt: {
      type: GraphQLDateTime,
      sqlColumn: 'updated_at',
    },
  }),
  interfaces: [nodeInterface],
});

export default Question;
