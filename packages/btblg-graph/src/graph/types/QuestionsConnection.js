import { createConnection } from '../utils';
import QuestionType from './Question';

const QuestionsConnection = createConnection({
  name: 'Questions',
  nodeType: QuestionType,
});

export default QuestionsConnection.connectionType;
