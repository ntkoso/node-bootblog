import { GraphQLObjectType, GraphQLString } from 'graphql';
import { globalIdField, forwardConnectionArgs } from 'graphql-relay';
import { nodeInterface } from '../nodeDefinitions';
import { belongsToMany } from '../../database/relationships';
import ThreadsConnection from './ThreadsConnection';

const TagType = new GraphQLObjectType({
  name: 'Tag',
  description: "Thread's tag, currently only for threads",
  sqlTable: 'boot_tags',
  uniqueKey: 'id',
  fields: () => ({
    id: {
      ...globalIdField(),
      sqlColumn: 'id',
    },
    body: {
      type: GraphQLString,
    },
    slug: {
      type: GraphQLString,
    },
    threads: {
      type: ThreadsConnection,
      args: forwardConnectionArgs,
      junction: {
        sqlTable: 'boot_threads_tags',
        sqlJoins: belongsToMany('tag_id', 'thread_id'),
      },
    },
  }),
  interfaces: [nodeInterface],
});

export default TagType;
