import {
  GraphQLObjectType,
  GraphQLEnumType,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLInputObjectType,
  GraphQLNonNull,
} from 'graphql';
import { globalIdField, forwardConnectionArgs } from 'graphql-relay';
import { GraphQLDateTime } from 'graphql-iso-date';
import { nodeInterface } from '../nodeDefinitions';
import { belongsTo, belongsToMany, hasMany } from '../../database/relationships';
import CommentsConnection from './CommentsConnection';
import UserType from './User';
import CategoryType from './Category';
import TagType from './Tag';

export const threadOrderByEnum = new GraphQLEnumType({
  name: 'threadOrderBy',
  values: {
    TITLE: { value: 'title' },
    VIEWS: { value: 'views' },
    CREATED_AT: { value: 'createdAt' },
    UPDATED_AT: { value: 'updatedAt' },
  },
});

export const OrderByColumn = new GraphQLInputObjectType({
  name: 'OrderByColumn',
  fields: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    direction: { type: GraphQLString, defaultValue: 'asc' },
  },
});

const Thread = new GraphQLObjectType({
  name: 'Thread',
  description: 'Thread - blog post',
  sqlTable: 'boot_threads',
  uniqueKey: 'id',
  fields: () => ({
    id: {
      ...globalIdField(),
      sqlColumn: 'id',
    },
    title: {
      type: GraphQLString,
    },
    body: {
      type: GraphQLString,
    },
    views: {
      type: GraphQLInt,
    },
    slug: {
      type: GraphQLString,
    },
    author: {
      type: UserType,
      sqlJoin: belongsTo('author_id'),
    },
    category: {
      type: CategoryType,
      sqlJoin: belongsTo('category_id'),
    },
    comments: {
      type: CommentsConnection,
      args: forwardConnectionArgs,
      sqlJoin: hasMany('thread_id'),
    },
    tags: {
      type: new GraphQLList(TagType),
      junction: {
        sqlTable: 'boot_threads_tags',
        sqlJoins: belongsToMany('thread_id', 'tag_id'),
      },
    },
    createdAt: {
      type: GraphQLDateTime,
      sqlColumn: 'created_at',
    },
    updatedAt: {
      type: GraphQLDateTime,
      sqlColumn: 'updated_at',
    },
  }),
  interfaces: [nodeInterface],
});

export default Thread;
