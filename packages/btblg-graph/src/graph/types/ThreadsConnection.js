import { createConnection } from '../utils';
import ThreadType from './Thread';

const ThreadsConnection = createConnection({
  name: 'Threads',
  nodeType: ThreadType,
});

export default ThreadsConnection.connectionType;
