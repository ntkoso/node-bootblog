import { GraphQLObjectType, GraphQLString } from 'graphql';
import { globalIdField, forwardConnectionArgs } from 'graphql-relay';
import { nodeInterface } from '../nodeDefinitions';
import { hasMany, hasOne } from '../../database/relationships';
import ThreadsConnection from './ThreadsConnection';
import CommentsConnection from './CommentsConnection';
import QuestionsConnection from './QuestionsConnection';
import AnswersConnection from './AnswersConnection';
import ProfileType from './Profile';

const User = new GraphQLObjectType({
  name: 'User',
  description: 'User',
  sqlTable: 'boot_users',
  uniqueKey: 'id',
  fields: () => ({
    id: {
      ...globalIdField(),
      sqlColumn: 'id',
    },
    username: {
      type: GraphQLString,
      description: 'Visible username',
    },
    usernameCanonical: {
      type: GraphQLString,
      description: 'Lowercased username used in URLs',
      sqlColumn: 'username_canonical',
    },
    email: {
      type: GraphQLString,
      description: 'The email of the user',
    },
    profile: {
      type: ProfileType,
      description: "User's profile",
      sqlJoin: hasOne('user_id'),
    },
    threads: {
      type: ThreadsConnection,
      args: forwardConnectionArgs,
      orderBy: {
        created_at: 'desc',
        id: 'asc',
      },
      sqlPaginate: true,
      sqlJoin: hasMany('author_id'),
    },
    comments: {
      type: CommentsConnection,
      args: forwardConnectionArgs,
      sqlPaginate: true,
      sqlJoin: hasMany('author_id'),
    },
    questions: {
      type: QuestionsConnection,
      args: forwardConnectionArgs,
      sqlPaginate: true,
      sqlJoin: hasMany('author_id'),
    },
    answers: {
      type: AnswersConnection,
      args: forwardConnectionArgs,
      sqlPaginate: true,
      sqlJoin: hasMany('author_id'),
    },
  }),
  interfaces: [nodeInterface],
});

export default User;
