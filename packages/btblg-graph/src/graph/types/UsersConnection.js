import { createConnection } from '../utils';
import User from './User';

const UsersConnection = createConnection({
  name: 'Users',
  nodeType: User,
}).connectionType;

export default UsersConnection;
