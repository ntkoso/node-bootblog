import { GraphQLInt } from 'graphql';
import { connectionDefinitions } from 'graphql-relay';

const createConnection = config => {
  const connectionFields = config.connectionFields || {};

  return connectionDefinitions({
    ...config,
    connectionFields: {
      total: { type: GraphQLInt },
      ...connectionFields,
    },
  });
};

export default createConnection;
