import {
  GraphQLNonNull,
  GraphQLList,
  GraphQLString,
  GraphQLObjectType,
} from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';
import { formatErrorMessage } from '@ntkoso/utils';

const ValidationError = new GraphQLObjectType({
  name: 'ValidationError',
  description:
    'Validation error, e.g. form error, duplicate email, wrong password, etc...',
  fields: () => ({
    path: {
      type: GraphQLString,
      description: `Error path name (typically is form field name),
       can be null if error is more generic`,
    },
    message: {
      type: new GraphQLNonNull(GraphQLString),
      description:
        'Error message in react-intl message id format (validation.path.error_type)',
    },
    messageTranslated: {
      type: GraphQLString,
      description: 'Translated error message',
      resolve: (error, second, { intl }) => {
        if (error && error.message && intl && intl.formatMessage) {
          return formatErrorMessage(intl.formatMessage, error).message;
        }

        return null;
      },
    },
  }),
});

const resolveMaybeThunk = fields =>
  typeof fields === 'function' ? fields() : fields;

const createMutation = ({
  name,
  inputFields,
  outputFields,
  mutateAndGetPayload,
}) =>
  mutationWithClientMutationId({
    name,
    inputFields,
    mutateAndGetPayload,
    outputFields: {
      ...resolveMaybeThunk(outputFields),
      errors: {
        type: new GraphQLList(ValidationError),
      },
    },
  });

export default createMutation;
