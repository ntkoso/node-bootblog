// @flow
import { withCache } from '@ntkoso/graphql-custom-resolver-cache';
import { jm } from '../../database/helpers';

const resolve = withCache((root, args, context, resolveInfo) =>
  jm(resolveInfo, context),
);

type RootField<T> = {
  resolve: typeof resolve,
  ...T,
};

const createQuery = <T>(params: T): RootField<T> => ({
  resolve,
  ...params,
});

export default createQuery;
