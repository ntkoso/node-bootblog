// @flow

type InstanceOf<T> = (type: Class<T>) => (value: T) => T | null;
const instanceOf: InstanceOf<*> = type => value =>
  value instanceof type ? value : null;

export default instanceOf;
