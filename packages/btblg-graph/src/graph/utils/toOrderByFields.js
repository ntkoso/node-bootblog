// @flow
import snakeCase from 'snake-case';

type Direction = 'asc' | 'desc';

const toOrderByFields = ({
  orderBy = 'id asc',
}: {
  orderBy?: string,
}): { [field: string]: Direction } =>
  orderBy
    .split(',')
    .map(field => field.trim())
    .reduce((prev, order) => {
      const [field, direction] = order.split(' ');

      return { ...prev, [snakeCase(field)]: direction };
    }, {});

export default toOrderByFields;
