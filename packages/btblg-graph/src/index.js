import 'babel-polyfill';
import http from 'http';
import log from 'log';
import app from './app';

const appCallback = app.callback();
const server = http.createServer(appCallback);

server.listen(process.env.PORT, () => {
  log.info({ port: process.env.PORT }, 'listen');
});
