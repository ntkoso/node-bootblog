import pino from 'pino';

const logger = pino({
  name: 'graphql',
  level: process.env.DEBUG ? 'debug' : 'info',
  prettyPrint: process.env.NODE_ENV !== 'production',
  base: {
    pid: process.pid,
    application: 'btblg',
    package: '@ntkoso/btblg-graph',
  },
});

export default logger;
