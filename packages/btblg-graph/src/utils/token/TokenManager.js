// @flow

class TokenManager {
  context: Object;
  domain: string;
  accessTokenKey: string;
  xsrfTokenKey: string;
  xsrfHeaderKey: string;
  xsrfCookieKey: string;
  secureProxy: boolean;

  constructor(
    ctx: Object,
    options?: {
      accessTokenKey?: string,
      xsrfTokenKey?: string,
      xsrfHeaderKey?: string,
      xsrfCookieKey?: string,
    } = {},
    secureProxy?: boolean = true,
  ) {
    this.context = ctx;
    this.domain = `.${ctx.headers.host}`;
    this.accessTokenKey = options.accessTokenKey || 'accessToken';
    this.xsrfTokenKey = options.xsrfTokenKey || 'xsrfToken';
    this.xsrfHeaderKey = options.xsrfHeaderKey || 'X-XSRF-Token';
    this.xsrfCookieKey = options.xsrfCookieKey || 'xsrf_token';
    this.secureProxy = secureProxy;
  }

  getAccessToken(): string {
    return this.context[this.accessTokenKey];
  }

  isXsrfTokenValid(): boolean {
    const accessTokenXsrf =
      this.context.state[this.accessTokenKey] &&
      this.context.state[this.accessTokenKey][this.xsrfTokenKey];

    const headerXsrf = this.context.headers[this.xsrfHeaderKey]
      ? this.context.headers[this.xsrfHeaderKey]
      : this.context.headers[this.xsrfHeaderKey.toLowerCase()];

    const cookieXsrf = this.context.cookies.get(this.xsrfCookieKey);

    return accessTokenXsrf === headerXsrf && accessTokenXsrf === cookieXsrf;
  }

  setTokenCookies(accessToken: {}, xsrfToken: {}): void {
    this.context.cookies.set(this.accessTokenKey, accessToken, {
      domain: this.domain,
      secureProxy: this.secureProxy,
      httpOnly: true, // Client JS can't access this cookie
    });

    this.context.cookies.set(this.xsrfCookieKey, xsrfToken, {
      domain: this.domain,
      secureProxy: this.secureProxy,
      httpOnly: false, // Client JS can access this cookie
    });
  }

  clearTokenCookies(): void {
    this.context.cookies.set(this.accessTokenKey, '', {
      domain: this.domain,
      overwrite: true,
      httpOnly: true,
      secureProxy: true,
    });

    this.context.cookies.set(this.xsrfCookieKey, '', {
      domain: this.domain,
      overwrite: true,
      httpOnly: false,
    });
  }
}

export default TokenManager;
