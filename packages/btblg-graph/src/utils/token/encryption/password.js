// @flow
import { genSalt, hash, compare } from 'bcrypt';

export const encrypt = async (
  password: string,
  saltFactor: number = 10,
): Promise<string> => {
  const salt = await genSalt(saltFactor);

  return hash(password, salt);
};

export const encryptWithSalt = async (
  password: string,
  saltFactor: number = 10,
): Promise<{| password: string, salt: string |}> => {
  const salt = await genSalt(saltFactor);
  const text = await hash(password, salt);

  return { password: text, salt };
};

export { compare };
