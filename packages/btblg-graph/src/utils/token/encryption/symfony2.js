// @flow
import { createHash } from 'crypto';

export const encrypt = (password: string, salt: string): string => {
  const salted = `${password}{${salt}}`;

  let hash = createHash('sha512').update(salted, 'utf8');

  for (let i = 1; i < 5000; i += 1) {
    hash = createHash('sha512').update(hash.digest('binary') + salted);
  }

  return hash.digest('base64');
};

export const compare = (
  password: string,
  dbPassword: string,
  dbSalt: string,
) => {
  const encryptedPassword = encrypt(password, dbSalt);

  return dbPassword === encryptedPassword;
};
