// @flow
import { createCipher, createDecipher } from 'crypto';

export const encrypt = (
  text: string,
  secret: string,
  algorithmName: string = 'aes-256-cbc',
): string => {
  const cipher = createCipher(algorithmName, secret);

  return cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
};

export const decrypt = (
  encryptedText: string,
  secret: string,
  algorithmName: string = 'aes-256-cbc',
): string => {
  const decipher = createDecipher(algorithmName, secret);

  return decipher.update(encryptedText, 'hex', 'utf8') + decipher.final('utf8');
};
