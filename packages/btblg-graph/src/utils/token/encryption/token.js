// @flow
import JWT from 'jsonwebtoken';
import pify from 'pify';
import { encrypt as encryptText, decrypt as decryptText } from './text';
import type { Credentials } from '../generateCredentials';

type SignAsync = (token: string, secret: string) => Promise<string>;
const signAsync: SignAsync = pify(JWT.sign);

type VerifyAsync = (token: string, secret: string) => Promise<string>;
const verifyAsync: VerifyAsync = pify(JWT.verify);

export const encrypt = async (
  credentials: Credentials,
  secret: string,
): Promise<string> =>
  signAsync(encryptText(JSON.stringify(credentials), secret), secret);

export const decrypt = async (
  token: string,
  secret: string,
): Promise<Credentials> => {
  const encCredentials = await verifyAsync(token, secret);

  return JSON.parse(decryptText(encCredentials, secret));
};
