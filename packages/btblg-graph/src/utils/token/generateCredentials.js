// @flow
import uuidV4 from 'uuid/v4';

export type Credentials = {
  iss: string,
  iat: number,
  exp: number,
  sub: string,
  scope: Array<string>,
  jti: string,
  token: string,
};

const generateCredentials = (
  issuer: string,
  subject: string,
  token: string,
  ttl: number | string = 604800 * 1000,
): Credentials => {
  const now = Date.now();
  const jti = uuidV4();

  return {
    iss: issuer,
    iat: now,
    exp: now + Number(ttl),
    subject,
    scope,
    jti,
    token,
  };
};

export default generateCredentials;
