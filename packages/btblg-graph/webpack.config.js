const path = require('path');
const RestartServerPlugin = require('reload-server-webpack-plugin');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const {
  inlineEnv,
  reactIntlLocalesPick,
} = require('@ntkoso/btblg-webpack-parts/lib/plugins');
const { js } = require('@ntkoso/btblg-webpack-parts/lib/rules');
const { resolve } = require('@ntkoso/btblg-webpack-parts/lib/settings');
const babelrc = require('./.babelrc');
const pckg = require('./package.json');

const isProduction = process.env.NODE_ENV === 'production';

const entry = ['./src/index.js'];

let plugins = [
  !isProduction
    ? new webpack.NamedModulesPlugin()
    : new webpack.HashedModuleIdsPlugin(),
  reactIntlLocalesPick(),
  inlineEnv('server'),
];

if (!isProduction) {
  plugins = [
    ...plugins,
    new RestartServerPlugin({ script: './.build/index.js' }),
  ];
}

module.exports = {
  entry,
  mode: !isProduction ? 'development' : 'production',
  watch: !isProduction,
  target: 'node',
  externals: [
    nodeExternals({ modulesFromFile: true, whitelist: [/@ntkoso/i] }),
  ],
  module: {
    rules: [js({ target: 'server', name: pckg.name, babelrc })],
  },
  plugins,
  resolve: resolve(),
  output: {
    path: path.join(__dirname, '.build'),
    filename: 'index.js',
  },
};
