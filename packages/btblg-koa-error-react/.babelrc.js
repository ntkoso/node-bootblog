const isProduction = process.env.NODE_ENV === 'production';
const isTest = process.env.NODE_ENV === 'test';
const moduleType = process.env.MODULE_ENV || false;

const presets = [
  [
    '@babel/preset-env',
    {
      debug: !isProduction,
      modules: isTest ? 'commonjs' : moduleType,
      forceAllTransforms: true,
    },
  ],
  '@babel/preset-react',
];

let plugins = [
  '@babel/plugin-proposal-object-rest-spread',
  '@babel/plugin-syntax-flow',
  '@babel/plugin-syntax-object-rest-spread',
  '@babel/plugin-transform-flow-comments',
];

if (isProduction) {
  plugins = [
    '@babel/plugin-transform-react-constant-elements',
    '@babel/plugin-transform-react-inline-elements',
    ...plugins,
  ];
}

if (!isProduction) {
  plugins = ['@babel/plugin-transform-react-jsx-source', ...plugins];
}

module.exports = { presets, plugins };
