// @flow
import React from 'react';
import type { Element } from 'react';

type Props = { body: string };

export const Error = ({ body }: Props): Element<'li'> => <li>{body}</li>;

export default Error;
