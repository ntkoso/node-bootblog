// @flow
import React from 'react';
import type { Element } from 'react';
import ErrorBody from './ErrorBody';

type Props = {
  env: string,
  status: number | string,
  message: string,
  stack: string,
};

export const ErrorApplication = ({
  env,
  status,
  message,
  stack,
}: Props): Element<'html'> => (
  <html lang="en">
    <head>
      <title>{`Error - ${status} - ${message}`}</title>
      <meta charSet="UTF-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>
    <body>
      <div className="ErrorApplication">
        <h1>Error</h1>
        <ErrorBody env={env} status={status} error={message} stack={stack} />
      </div>
    </body>
  </html>
);

export default ErrorApplication;
