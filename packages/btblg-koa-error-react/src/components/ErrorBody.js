// @flow
import React from 'react';
import type { Element } from 'react';

export type Props = {
  env: string,
  status: number | string,
  error: string,
  stack: string,
};

export const ErrorBody = ({
  env,
  status,
  error,
  stack,
}: Props): Element<'div'> | null => {
  if (env !== 'production') {
    return (
      <div>
        <h2>Status:</h2>
        <pre>
          <code>{status}</code>
        </pre>
        <h2>Message:</h2>
        <pre>
          <code>{error}</code>
        </pre>
        <h2>Stack:</h2>
        <pre>
          <code>{stack}</code>
        </pre>
      </div>
    );
  }

  return null;
};

export default ErrorBody;
