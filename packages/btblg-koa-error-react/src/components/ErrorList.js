// @flow
import React from 'react';
import type { Element } from 'react';
import Error from './Error';

type ErrorType = { message: string, status: number | string };

type Props = { errors: Array<ErrorType> };

export const ErrorList = ({ errors }: Props): Element<'ul'> => (
  <ul>
    {errors.map((error: ErrorType): Element<typeof Error> => (
      <Error body={error.message} key={error.message} />
    ))}
  </ul>
);

export default ErrorList;
