import React from 'react';
import { create } from 'react-test-renderer';
import Error from '../Error';

test('renders error string', () => {
  expect(create(<Error body="error message" />)).toMatchSnapshot();
});
