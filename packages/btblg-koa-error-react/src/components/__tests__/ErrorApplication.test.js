import React from 'react';
import { create } from 'react-test-renderer';
import ErrorApplication from '../ErrorApplication';

test('renders html document with error', () => {
  expect(
    create(
      <ErrorApplication
        env="development"
        status={500}
        error="error message"
        stack="Error: error message at..."
      />,
    ),
  ).toMatchSnapshot();
});
