import React from 'react';
import { create } from 'react-test-renderer';
import ErrorBody from '../ErrorBody';

const errorData = {
  status: 500,
  error: 'error message',
  stack: 'Error: error message at...',
};

test('renders error in development environment', () => {
  expect(
    create(<ErrorBody env="development" {...errorData} />),
  ).toMatchSnapshot();
});

test('renders error user screen or null in production environment', () => {
  expect(
    create(<ErrorBody env="production" {...errorData} />),
  ).toMatchSnapshot();
});
