import React from 'react';
import { create } from 'react-test-renderer';
import ErrorList from '../ErrorList';

test('renders list of errors', () => {
  expect(
    create(
      <ErrorList
        errors={[
          { message: 'error message', status: 500 },
          { message: 'Not Found', status: '404' },
        ]}
      />,
    ),
  ).toMatchSnapshot();
});
