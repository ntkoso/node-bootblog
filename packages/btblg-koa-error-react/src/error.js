// @flow
import http from 'http';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import type { Middleware } from 'koa';
import ErrorApplication from './components/ErrorApplication';

const env = process.env.NODE_ENV || 'development';

type ErrorMiddleware = () => Middleware;

type KoaError = {
  code?: number,
  message: $PropertyType<Error, 'message'>,
  stack: $PropertyType<Error, 'stack'>,
  status?: number,
};

const error: ErrorMiddleware = () => async (ctx, next) => {
  try {
    await next();
    if (ctx.response.status === 404 && ctx.response.body) {
      ctx.throw(404);
    }
  } catch (error) {
    const err: KoaError = error;

    const currentContext = ctx;

    currentContext.status = err.status || 500;
    currentContext.app.emit('error', err, currentContext);

    switch (currentContext.accepts('html', 'text', 'json')) {
      case 'text':
        if (env === 'development') {
          currentContext.body = err.message;
        } else if (err.expose) {
          currentContext.body = err.message;
        } else {
          throw err;
        }
        break;

      case 'json':
        if (env === 'development') {
          currentContext.body = {
            message: err.message,
            status: currentContext.status,
            stack: err.stack,
          };
        } else if (err.expose) {
          currentContext.body = {
            message: err.message,
            status: currentContext.status,
          };
        } else {
          currentContext.body = {
            message: err.message,
            status: http.STATUS_CODES[currentContext.status],
          };
        }
        break;

      case 'html':
        currentContext.body = ReactDOMServer.renderToStaticMarkup(
          <ErrorApplication
            code={err.code}
            env={env}
            message={err.message}
            stack={err.stack}
            status={currentContext.status}
          />,
        );
        break;

      default:
        if (env === 'development') {
          currentContext.body = err.message;
        } else if (err.expose) {
          currentContext.body = err.message;
        } else {
          throw err;
        }
    }
  }
};

export default error;
