// @flow
import type { Middleware } from 'koa';

type Jwt = (
  secret: string,
  decryptToken: (token: string, secret: string) => Promise<string>,
  settings?: {
    key?: string,
    passthrough?: boolean,
    cookie?: ?string,
  },
) => Middleware;

const jwt: Jwt = (
  secret,
  decryptToken,
  { key = 'access_token', passthrough = false, cookie = false } = {},
) => async (ctx, next) => {
  let token;
  let tokenPayload;
  let message;
  const currentContext = ctx;

  if (cookie && currentContext.cookies.get(cookie)) {
    token = currentContext.cookies.get(cookie);
  } else if (currentContext.header.authorization) {
    const [scheme, credentials] = currentContext.header.authorization.split(
      ' ',
    );
    if (scheme && credentials) {
      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    } else if (!passthrough) {
      currentContext.throw(401, 'Invalid Authorization header format');
    }
  } else if (!passthrough) {
    currentContext.throw(401, 'Authorization header is required');
  }

  try {
    if (typeof token === 'string') {
      tokenPayload = await decryptToken(token, secret);
    }
  } catch (error) {
    const err = (error: Error);
    message = `Invalid token. ${err.message}`;
  }

  if (tokenPayload || passthrough) {
    currentContext.state = currentContext.state || {};
    currentContext.state[key] = tokenPayload;

    await next();
  } else {
    currentContext.throw(401, message);
  }
};

export default jwt;
