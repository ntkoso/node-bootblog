// @flow
import { addLocaleData } from 'react-intl';
import type { Middleware } from 'koa';
import { dependencies, peerDependencies } from '../package.json';

type Messages = { [id: string]: string };

type MessagesCache = { [locale: string]: Messages };
let messagesCache: MessagesCache = {};

type Dependencies = { [package: string]: string };

type IntlState = {| locale: string, messages: Messages |};

type Intl = (arg: { key?: string }) => Middleware;
const intl: Intl = ({ key = 'intl' } = {}) => async (ctx, next) => {
  const { request, log, state } = ctx;

  const pckgDeps = (dependencies: ?Dependencies);
  const pckgPeerDeps = (peerDependencies: ?Dependencies);

  const deps = pckgDeps ? Object.keys(pckgDeps) : [];
  const peer = pckgPeerDeps ? Object.keys(pckgPeerDeps) : [];
  const packages = [...deps, ...peer];

  const locales = packages
    .filter(p => p.includes('btblg-locale'))
    .map(p => p.split('btblg-locale-').pop());

  const locale = request.acceptsLanguages(locales) || 'en';

  if (!messagesCache[locale]) {
    try {
      const messages: ?Messages = require(`@ntkoso/btblg-locale-${locale}`)
        .default;

      if (messages) {
        addLocaleData(require(`react-intl/locale-data/${locale}`));

        messagesCache = {
          ...messagesCache,
          [locale]: messages,
        };
      }
    } catch (err) {
      if (log && typeof log.error === 'function') {
        log.error(err);
      } else {
        console.error(err);
      }
    }
  }

  const intlState: IntlState = {
    locale,
    messages: messagesCache[locale] ? messagesCache[locale] : {},
  };

  state[key] = intlState;

  return next();
};

export default intl;
