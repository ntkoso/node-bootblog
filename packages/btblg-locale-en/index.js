var messages = {
  "categories.title": "Categories",
  "category.threadsCount":
    "{threadsCount, plural, =0 {no threads} one {# thread} other {# threads}}",
  "validation.email.required": "{path} is required",
  "validation.email.invalid":
    "{path} should be a valid email, e.g. foo@bar.com",
  "validation.password.required": "{path} is invalid",
  "validation.password.long": "{path} should be 8 or more symbols long",
  "validation.password.uppercase_letters":
    "{path} should include at least 1 uppercase letter",
  "validation.password.lowercase_letters":
    "{path} should include at least 1 lowercase letter",
  "validation.password.numbers": "{path} should include at least 1 number",
  "validation.password.invalid": "{path} is required"
};

export default messages;
