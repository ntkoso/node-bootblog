const isProduction = process.env.NODE_ENV === 'production';
const isTest = process.env.NODE_ENV === 'test';
const moduleType = process.env.MODULE_ENV || false;

let presets = [
  [
    '@babel/preset-env',
    {
      debug: !isProduction,
      modules: isTest ? 'commonjs' : moduleType,
      forceAllTransforms: true,
    },
  ],
  '@babel/preset-react',
];

let plugins = [
  '@babel/plugin-syntax-class-properties',
  '@babel/plugin-syntax-flow',
  '@babel/plugin-syntax-object-rest-spread',
  '@babel/plugin-proposal-class-properties',
  '@babel/plugin-proposal-object-rest-spread',
  '@babel/plugin-transform-flow-comments',
  'babel-plugin-polished',
];

const emotion = {
  sourceMap: !isProduction,
  hoist: isProduction,
  autoLabel: true,
  primaryInstance: '@ntkoso/emotion',
  instances: ['@ntkoso/emotion', '@ntkoso/react-emotion'],
};

if (isProduction) {
  plugins = [
    '@babel/plugin-transform-react-constant-elements',
    '@babel/plugin-transform-react-inline-elements',
    ['babel-plugin-emotion', emotion],
    ...plugins,
  ];
}

if (!isProduction) {
  plugins = [
    ['babel-plugin-emotion', emotion],
    '@babel/plugin-transform-react-jsx-source',
    ...plugins,
  ];
}

module.exports = { presets, plugins };
