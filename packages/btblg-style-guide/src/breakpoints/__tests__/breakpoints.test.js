import * as breakpoints from '../index';

test('breakpoints', () => {
  Object.values(breakpoints).forEach(breakpoint => {
    expect(breakpoint).toMatchSnapshot();
  });
});
