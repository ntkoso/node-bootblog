// @flow
import { rem } from 'polished';

type CreateMWQuery = (rems: number | string) => string;
const createMWQuery: CreateMWQuery = rems => `(min-width: ${rem(rems)})`;

type CreateBreakpoint = (query: string) => string;
const createBreakpoint: CreateBreakpoint = query => `@media ${query}`;

export const smallQuery = createMWQuery(375);
export const smallLargerQuery = createMWQuery(525);
export const mediumQuery = createMWQuery(640);
export const mediumLargerQuery = createMWQuery(800);
export const largeQuery = createMWQuery(900);
export const extraLargeQuery = createMWQuery(1200);

export const small = createBreakpoint(smallQuery);
export const smallLarger = createBreakpoint(smallLargerQuery);
export const medium = createBreakpoint(mediumQuery);
export const mediumLarger = createBreakpoint(mediumLargerQuery);
export const large = createBreakpoint(largeQuery);
export const extraLarge = createBreakpoint(extraLargeQuery);
