// @flow
import React from 'react';
import type { Element } from 'react';
import styled from '@ntkoso/react-emotion';
import LazyImage from '../LazyImage';

type SizeEnum = 'small' | 'medium' | 'large';

const getDimension = (size: SizeEnum): number => {
  switch (size) {
    case 'small':
      return 36;

    case 'medium':
      return 128;

    case 'large':
      return 256;

    default:
      return 36;
  }
};

const brdrRadius = { borderRadius: '50%' };

type Props = {
  src: string,
  size?: SizeEnum,
};

const Avatar = ({ src, size = 'small' }: Props): Element<typeof LazyImage> => (
  <LazyImage
    width={getDimension(size)}
    height={getDimension(size)}
    renderPlaceholder={({ src, style }) => (
      <div style={style}>Placeholder {src}</div>
    )}
    renderError={({ src, style }) => <div style={style}>Error {src}</div>}
    src={src}
    alt={`Avatar Image`}
  />
);

export default Avatar;
