// @flow
import React from 'react';
import type { Element } from 'react';
import styled from '@ntkoso/react-emotion';
import { readableColor, darken, transparentize } from 'polished';
import { Button as ButtonTypography } from '../typography';
import { ButtonWrapper, IconWrapper } from './wrappers';
import type { ButtonProps } from './types';

const Wrapper = styled(ButtonWrapper)({
  borderColor: 'black',
  color: 'black',

  ':hover': {
    backgroundColor: transparentize(0.8, 'black'),
  },

  ':active': {
    backgroundColor: transparentize(0.6, 'black'),
    borderColor: transparentize(0.8, 'black'),
  },

  '&[disabled]': {
    color: transparentize(0.75, 'black'),
    borderColor: transparentize(0.75, 'black'),
  },
});

const Button = ({
  icon,
  children,
  ...rest
}: ButtonProps): Element<typeof Wrapper> => (
  <Wrapper {...rest}>
    {icon && <IconWrapper>{icon}</IconWrapper>}
    <ButtonTypography>{children}</ButtonTypography>
  </Wrapper>
);

export default Button;
