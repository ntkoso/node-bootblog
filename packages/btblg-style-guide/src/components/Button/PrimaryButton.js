// @flow
import React from 'react';
import type { Element } from 'react';
import styled from '@ntkoso/react-emotion';
import { readableColor, darken, transparentize } from 'polished';
import { Button as ButtonTypography } from '../typography';
import { ButtonWrapper, IconWrapper } from './wrappers';
import type { ButtonProps } from './types';

const Wrapper = styled(ButtonWrapper)(
  ({ theme: { color: { primary, primaryDarker } } }) => ({
    backgroundColor: primary,
    borderColor: primary,
    color: readableColor(primary || 'black'),

    ':hover': {
      backgroundColor: primaryDarker,
      borderColor: primaryDarker,
    },

    ':active': {
      backgroundColor: darken(0.1, primaryDarker),
      borderColor: darken(0.1, primaryDarker),
    },

    '&[disabled]': {
      backgroundColor: transparentize(0.75, primary),
      borderColor: transparentize(0.75, primary),
      color: transparentize(0.75, readableColor(primary || 'black')),
    },
  }),
);

const PrimaryButton = ({
  icon,
  children,
  ...rest
}: ButtonProps): Element<typeof Wrapper> => (
  <Wrapper {...rest}>
    {icon && <IconWrapper>{icon}</IconWrapper>}
    <ButtonTypography>{children}</ButtonTypography>
  </Wrapper>
);

export default PrimaryButton;
