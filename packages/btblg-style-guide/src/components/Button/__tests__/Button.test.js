import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Button from '../Button';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Button renders correctly', () => {
  const defButton = create(
    <Button
      theme={{
        color: {
          primary: 'black',
          primaryDarker: 'black',
        },
      }}
    >
      TEST
    </Button>,
  );

  expect(defButton.toJSON()).toMatchSnapshot();
});

test('Button with icon', () => {
  const defButton = create(
    <Button
      icon={<div>icon element</div>}
      theme={{
        color: {
          primary: 'black',
          primaryDarker: 'black',
        },
      }}
    >
      TEST
    </Button>,
  );

  expect(defButton.toJSON()).toMatchSnapshot();
});
