import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import PrimaryButton from '../PrimaryButton';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Primary button renders correctly using theme', () => {
  const primButton = create(
    <PrimaryButton
      theme={{
        color: {
          primary: 'black',
          primaryDarker: 'black',
        },
      }}
    >
      TEST
    </PrimaryButton>,
  );

  expect(primButton.toJSON()).toMatchSnapshot();
});

test('Primary Button with icon', () => {
  const defButton = create(
    <PrimaryButton
      icon={<div>icon element</div>}
      theme={{
        color: {
          primary: 'black',
          primaryDarker: 'black',
        },
      }}
    >
      TEST
    </PrimaryButton>,
  );

  expect(defButton.toJSON()).toMatchSnapshot();
});
