// @flow
import Button from './Button';

export { default as Button } from './Button';
export { default as PrimaryButton } from './PrimaryButton';

export default Button;
