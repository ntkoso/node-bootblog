// @flow
import type { Node, ElementProps } from 'react';

export type ButtonProps = {
  children: Node,
  icon?: Node,
} & ElementProps<'button'>;
