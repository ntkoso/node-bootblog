import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';

export const ButtonWrapper = styled.button({
  display: 'flex',
  flex: 'none',
  borderStyle: 'solid',
  borderRadius: rem(4),
  borderWidth: rem(2),
  cursor: 'pointer',
  height: rem(36),
  minWidth: rem(8),

  paddingLeft: rem(16),
  paddingRight: rem(16),

  '&[disabled]': { pointerEvents: 'none' },

  transition: 'background-color 0.15s ease-in',
});

export const IconWrapper = styled.span({ marginRight: rem(8) });
