// @flow
import React from 'react';
import { rem } from 'polished';
import styled from '@ntkoso/react-emotion';
import { shadow } from '../../mixins';

export const Card = styled.div({ padding: rem(5) }, shadow(2));

export default Card;
