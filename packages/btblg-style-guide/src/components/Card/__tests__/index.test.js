import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import { Card } from '../Card';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Card default render', () => {
  const card = create(
    <Card>
      <div />
    </Card>,
  );

  expect(card.toJSON()).toMatchSnapshot();
});
