// @flow
import React from 'react';
import type { Element, Node } from 'react';
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';
import { extraLarge } from '../../breakpoints';

const Grid = styled.div({
  display: 'flex',
  flexDirection: 'column',
  '& > *': {
    flex: `1 1 auto`,
  },
  '& > *:first-child': {
    flex: `0 0 ${rem(48)}`,
  },
  '& > *:last-child': {
    flex: `1 0 ${rem(48)}`,
  },
  height: '100vh',

  '@supports (grid-area: auto)': {
    [extraLarge]: {
      display: 'grid',
      gridTemplateColumns: `auto minmax(${rem(374)}, ${rem(1440)}) auto`,
      gridTemplateRows: `${rem(48)} auto minmax(${rem(48)}, max-content)`,
      gridTemplateAreas: `
        ". header ."
        ". content ."
        ". footer ."
      `,
    },
  },
});

type Props = {
  header: (param: { gridArea: 'header' }) => Element<any>,
  content: (param: { gridArea: 'content' }) => Element<any>,
  footer: (param: { gridArea: 'footer' }) => Element<any>,
};

const Layout = ({ header, content, footer }: Props): Element<typeof Grid> => (
  <Grid>
    {typeof header === 'function' && header({ gridArea: 'header' })}
    {typeof content === 'function' && content({ gridArea: 'content' })}
    {typeof footer === 'function' && footer({ gridArea: 'footer' })}
  </Grid>
);

export default Layout;
