import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Layout from '../Layout';

test('Uses header render props to render the header layout part', () => {
  const element = create(<Layout header={() => <header>header</header>} />);

  expect(element.toJSON()).toMatchSnapshot();
});

test('Uses content render prop to render the content layout part', () => {
  const element = create(<Layout content={() => <div>content</div>} />);

  expect(element.toJSON()).toMatchSnapshot();
});

test('Uses footer render props to render the footer layout part', () => {
  const element = create(<Layout footer={() => <footer>footer</footer>} />);

  expect(element.toJSON()).toMatchSnapshot();
});

test('Uses multiple render props to render whole layout', () => {
  const element = create(
    <Layout
      header={() => <header>header</header>}
      content={() => <div>content</div>}
      footer={() => <footer>footer</footer>}
    />,
  );

  expect(element.toJSON()).toMatchSnapshot();
});
