// @flow
import React from 'react';
import type { Element, Node } from 'react';
import IntersectionObserver from 'react-intersection-observer';
import { Transition } from 'react-spring';

const loadImg = (src: string): Promise<string> => {
  let image = new Image();

  return new Promise((resolve, reject) => {
    image.onload = () => resolve(src);
    image.onerror = err => reject(err);
    image.src = src;
  });
};

type PlaceholderProps = {
  src: string,
  style: { opacity: number },
};

type Props = {
  src: string,
  renderPlaceholder: (props: PlaceholderProps) => Node,
  renderError: (props: PlaceholderProps) => Node,
};

type State = {
  loaded: boolean,
  error: Error | null,
};

class LazyImage extends React.Component<Props, State> {
  state: State = { loaded: false, error: null };

  componentDidMount() {
    const { src } = this.props;

    loadImg(src)
      .then(() => this.setState({ loaded: true }))
      .catch(error => this.setState({ error }));
  }

  render() {
    const { src, renderPlaceholder, renderError, ...rest } = this.props;
    const { error, loaded } = this.state;

    return (
      <IntersectionObserver>
        {isInView => (
          <Transition enter={{ opacity: 1 }} leave={{ opacity: 0 }}>
            {({ opacity }) => {
              const props = { src, style: { opacity }, ...rest };

              return !isInView ? (
                renderPlaceholder(props)
              ) : !loaded ? (
                error ? (
                  renderError(props)
                ) : (
                  renderPlaceholder(props)
                )
              ) : (
                <img {...props} />
              );
            }}
          </Transition>
        )}
      </IntersectionObserver>
    );
  }
}

export default LazyImage;
