// @flow strict
import { rem } from 'polished';
import styled, { keyframes } from '@ntkoso/react-emotion';

const rotate = keyframes({
  '100%': {
    transform: 'rotate(360deg)',
  },
});

const animation = `${rotate} 2300ms linear infinite`;

export const LoadingSpinner = styled.div({
  margin: `-${rem(24)} 0 0 -${rem(24)}`,
  height: rem(49),
  width: rem(49),
  animation,

  ':before': {
    content: '""',
    margin: `-${rem(22)} 0 0 -${rem(22)}`,
    height: rem(43),
    width: rem(43),
    animation,
  },

  ':after': {
    content: '""',
    margin: `-${rem(28)} 0 0 -${rem(28)}`,
    height: rem(55),
    width: rem(55),
    animation,
  },

  '&, :after, :before': {
    position: 'absolute',
    top: '50%',
    left: '50%',
    border: '1px solid rgb(204, 204, 204)',
    borderLeftColor: 'rgb(0, 0, 0)',
    borderRadius: rem(974),
  },
});

export default LoadingSpinner;
