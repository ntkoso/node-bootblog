// @flow
import React from 'react';
import type { Element } from 'react';
import { Route, Link, Switch } from 'react-router-dom';
import type { Match } from 'react-router-dom';
import styled from '@ntkoso/react-emotion';
import { withTheme } from 'emotion-theming';
import { rgba, rem } from 'polished';
import type { Theme } from '../themes/types';
import Button, { PrimaryButton } from './Button';
import { Checkbox, DatePicker, Radio, Select, Text, TextArea } from './inputs';

const Color = styled.div(
  {
    borderRadius: rem(6),
    width: rem(64),
    height: rem(64),
    margin: rem(16),
  },
  ({ type, theme: { color } }) => ({ backgroundColor: color[type] }),
);

const ColorName = styled.b(({ color }) => ({ color }));

const List = styled.ul({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start',
  width: '100vh',
  height: `calc(100vh - ${rem(48)})`,
});

const Li = styled.li({ listStyle: 'none', marginTop: rem(16) });

const Colors = withTheme(({ theme: { color } }: { theme: Theme }): Element<
  typeof List,
> => (
  <List>
    <Li>
      Primary Color (<ColorName color={color.primary}>
        {color.primary}
      </ColorName>):
      <Color type="primary" />
    </Li>
    <Li>
      Primary Darker Color (<ColorName color={color.primaryDarker}>
        {color.primaryDarker}
      </ColorName>):
      <Color type="primaryDarker" />
    </Li>
    <Li>
      Primary Lighter Color (<ColorName color={color.primaryLighter}>
        {color.primaryLighter}
      </ColorName>):
      <Color type="primaryLighter" />
    </Li>
    <Li>
      Accent Color (<ColorName color={color.accent}>{color.accent}</ColorName>):
      <Color type="accent" />
    </Li>
    <Li>
      Accent Darker Color (<ColorName color={color.accentDarker}>
        {color.accentDarker}
      </ColorName>):
      <Color type="accentDarker" />
    </Li>
    <Li>
      Accent Lighter Color (<ColorName color={color.accentLighter}>
        {color.accentLighter}
      </ColorName>):
      <Color type="accentLighter" />
    </Li>
  </List>
));

const Buttons = (): Element<typeof List> => (
  <List>
    <Li>
      Default button:
      <Button>Default</Button>
    </Li>
    <Li>
      Default disabled button:
      <Button disabled>Def disabled</Button>
    </Li>
    <Li>
      Primary button:
      <PrimaryButton>Primary</PrimaryButton>
    </Li>
    <Li>
      Primary disabled button:
      <PrimaryButton disabled>P disabled</PrimaryButton>
    </Li>
  </List>
);

const Inputs = (): Element<typeof List> => (
  <List>
    <Li>
      Checkbox:
      <Checkbox />
    </Li>
    <Li>
      Date picker:
      <DatePicker />
    </Li>
    <Li>
      Radio:
      <Radio>Radio</Radio>
    </Li>
    <Li>
      Select:
      <Select />
    </Li>
    <Li>
      Text:
      <Text />
    </Li>
    <Li>
      TextArea:
      <TextArea />
    </Li>
  </List>
);

const Wrapper = styled.div({
  display: 'flex',
  maxWidth: rem(1440),
  margin: '0 auto',
});

const Navigation = styled.nav({
  flex: 'none',
  backgroundColor: rgba(255, 0, 0, 0.5),
  width: rem(240),
});

const NavigationList = styled.ul({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  aiignContent: 'center',
});

const Main = styled.main({
  display: 'flex',
  flex: '1 1 auto',
  backgroundColor: rgba(0, 255, 0, 0.5),
  justifyContent: 'center',
});

type Props = { match: Match };

const StyleGuide = ({ match: { url } }: Props): Element<typeof Wrapper> => (
  <Wrapper>
    <Navigation>
      <NavigationList>
        <Li>
          <Link to={`${url}/colors`}>Colors</Link>
        </Li>
        <Li>
          <Link to={`${url}/buttons`}>Buttons</Link>
        </Li>
        <Li>
          <Link to={`${url}/inputs`}>Inputs</Link>
        </Li>
      </NavigationList>
    </Navigation>
    <Main>
      <Switch>
        <Route path={`${url}/colors`} component={Colors} />
        <Route path={`${url}/buttons`} component={Buttons} />
        <Route path={`${url}/inputs`} component={Inputs} />
      </Switch>
    </Main>
  </Wrapper>
);

export default StyleGuide;
