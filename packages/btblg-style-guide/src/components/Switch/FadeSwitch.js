// @flow
import React from 'react';
import type { Node } from 'react';
import { withRouter, Switch } from 'react-router';
import type { Location } from 'react-router';
import { Transition, config } from 'react-spring';

type Props = {
  children: Node,
  location: Location,
};

export const FadeSwitch = ({ children, location }: Props) => (
  <Transition
    keys={location.pathname}
    from={{ opacity: 0 }}
    enter={{ opacity: 1 }}
    leave={{ opacity: 0 }}
    config={config.slow}
  >
    {({ opacity }) => (
      <Switch location={location}>
        {React.Children.map(children, child =>
          React.cloneElement(child, { style: { opacity } }),
        )}
      </Switch>
    )}
  </Transition>
);

export default withRouter(FadeSwitch);
