// @flow
import React from 'react';
import type { Node } from 'react';
import { withRouter, Switch } from 'react-router';
import type { Location } from 'react-router';
import { Transition, config } from 'react-spring';

type Props = {
  children: Node,
  location: Location,
};

export const SlideSwitch = ({ children, location }: Props) => (
  <Transition
    keys={location.pathname}
    from={{ x: '0%' }}
    enter={{ x: '100%' }}
    leave={{ x: '0%' }}
    config={config.gentle}
  >
    {({ x }) => (
      <Switch location={location}>
        {React.Children.map(children, child =>
          React.cloneElement(child, {
            style: { transform: `traslate3d(${x}, 0, 0)` },
          }),
        )}
      </Switch>
    )}
  </Transition>
);

export default withRouter(SlideSwitch);
