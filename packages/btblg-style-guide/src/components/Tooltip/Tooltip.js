// @flow
import React, { createRef } from 'react';
import type { Element, Node } from 'react';
import { Portal } from 'react-portal';
import Transition from 'react-transition-group/Transition';
import { rem } from 'polished';
import styled from '@ntkoso/react-emotion';
import { transition } from '../../mixins';
import Card from '../Card';

const transparent = { opacity: 0, visibility: 'hidden' };
const solid = { opacity: 1, visibility: 'visible' };

const transitionClasses = ({ hover, active, status }) => {
  const styles = {
    entering: hover || active ? solid : transparent,
    entered: solid,
    exiting: solid,
    exited: hover || active ? solid : transparent,
  };

  return styles[status];
};

const Wrapper = styled.div(
  {
    position: 'absolute',
    zIndex: 50,
  },
  ({ transitionProp }) => transition([transitionProp, 'visibility']),
);

type Side = 'top' | 'right' | 'bottom' | 'left';

type Props = {
  active?: boolean,
  side: Side,
  target: HTMLElement,
  children: Node,
};

type State = {
  hover: boolean,
  transition: 'opacity' | 'all',
};

type Coordinates = { top?: number, left?: number };

class TargetedTooltip extends React.Component<Props, State> {
  tooltipRef: {
    current: HTMLElement | null,
  } = createRef();

  margin: number = 10;

  state = { hover: false, transition: 'opacity' };

  handleMouseEnter = () => this.setState({ hover: true });

  handleMouseLeave = () => this.setState({ hover: false });

  getTooltipPositionStyles(): Coordinates {
    const { target, side } = this.props;
    const { current: tooltip } = this.tooltipRef;

    const targetPosition = target.getBoundingClientRect();
    const scrollX = window.scrollX ? window.scrollX : window.pageXOffset;
    const scrollY = window.scrollY ? window.scrollY : window.pageYOffset;
    const top = scrollY + targetPosition.top;
    const left = scrollX + targetPosition.left;

    let style = {};

    if (side === 'left' && tooltip) {
      style = {
        top: top + target.offsetHeight / 2 - tooltip.offsetHeight / 2,
        left: left - tooltip.offsetWidth - this.margin,
      };
    }

    if (side === 'right' && tooltip) {
      style = {
        top: top + target.offsetHeight / 2 - tooltip.offsetHeight / 2,
        left: left + target.offsetWidth + this.margin,
      };
    }

    if (side === 'top' && tooltip) {
      style = {
        left: left - tooltip.offsetWidth / 2 + target.offsetWidth / 2,
        top: top - tooltip.offsetHeight - this.margin,
      };
    }

    if (side === 'bottom' && tooltip) {
      style = {
        left: left - tooltip.offsetWidth / 2 + target.offsetWidth / 2,
        top: top + target.offsetHeight + this.margin,
      };
    }

    return style;
  }

  static getDerivedStateFromProps(nextProps: Props, prevState: State): State {
    return {
      ...prevState,
      transition: prevState.hover || nextProps.active ? 'all' : 'opacity',
    };
  }

  render() {
    const { active, side, children } = this.props;
    const { hover, transition } = this.state;
    const { tooltipRef } = this;

    return (
      <Portal>
        <Transition in={active} timeout={200} appear>
          {status => (
            <Wrapper
              style={{
                ...transitionClasses({ hover, active, status }),
                ...this.getTooltipPositionStyles(),
              }}
              side={side}
              transitionProp={transition}
              innerRef={tooltipRef}
              onMouseEnter={this.handleMouseEnter}
              onMouseLeave={this.handleMouseLeave}
            >
              <Card>{children}</Card>
            </Wrapper>
          )}
        </Transition>
      </Portal>
    );
  }
}

export const Tootlip = ({
  target,
  ...rest
}: Props): Element<typeof TargetedTooltip> | null =>
  target ? <TargetedTooltip target={target} {...rest} /> : null;

export default Tootlip;
