// @flow
import styled from '@ntkoso/react-emotion';
import { withProps } from 'recompose';

const Checkbox = withProps({ type: 'checkbox' })(styled.input({}));

export default Checkbox;
