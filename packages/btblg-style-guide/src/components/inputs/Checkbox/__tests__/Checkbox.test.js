import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Checkbox from '../Checkbox';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Checkbox renders correctly', () => {
  const element = create(<Checkbox>TEST</Checkbox>);

  expect(element.toJSON()).toMatchSnapshot();
});
