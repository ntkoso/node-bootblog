import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import DatePicker from '../DatePicker';

expect.addSnapshotSerializer(createSerializer(emotion));

test('DatePicker renders correctly', () => {
  const element = create(<DatePicker>TEST</DatePicker>);

  expect(element.toJSON()).toMatchSnapshot();
});
