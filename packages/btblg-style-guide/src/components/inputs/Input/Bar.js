// @flow
import get from 'get-value';
import styled from '@ntkoso/react-emotion';
import { transition } from '../../../mixins';

const Bar = styled.span(
  {
    position: 'relative',
    display: 'block',
    width: '100%',

    '&::before, &::after': {
      position: 'absolute',
      bottom: 0,
      width: 0,
      height: '2px',
      content: '""',
    },

    '&::before': {
      left: '50%',
    },

    '&::after': {
      right: '50%',
    },
  },
  ({ theme }) => ({
    '&::before, &::after': {
      backgroundColor: get(theme, ['color', 'primary'], 'grey'),
      ...transition(['width', 'background-color'], {
        animation: theme.animation,
      }),
    },
  }),
);

export default Bar;
