// @flow
import { rem } from 'polished';
import get from 'get-value';
import styled from '@ntkoso/react-emotion';
import { transition } from '../../../mixins';

const Hint = styled.div(
  {
    position: 'absolute',
    top: `calc(${rem(20)} + 1.5 * ${rem(8)})`,
    left: 0,
    fontSize: rem(16),
    lineHeight: rem(16),
    pointerEvents: 'none',
    opacity: 0,
  },
  ({ theme }) => ({
    color: get(theme, ['color', 'text'], 'black'),
    ...transition('color', { animation: theme.animation }),
  }),
);

export default Hint;
