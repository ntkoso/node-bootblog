// @Flow
import styled from '@ntkoso/react-emotion';
import { rgba, rem } from 'polished';
import { transition } from '../../../mixins';

const iconSize = rem(48);

const IconWrapper = styled.span(
  {
    display: 'block',
    position: 'absolute',
    transform: `translateY(-${rem(40)})`,
    left: `calc(-1 * ${iconSize})`,
    width: iconSize,
    height: iconSize,
    fontSize: `calc(${iconSize} / 2)`,
    lineHeight: iconSize,
    color: rgba('black', 0.26),
    textAlign: 'center',
  },
  ({ theme }) => ({ ...transition('color', theme) }),
);

export default IconWrapper;
