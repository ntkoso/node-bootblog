// @flow
import styled from '@ntkoso/react-emotion';
import { rgb, rgba, rem } from 'polished';
import get from 'get-value';
import Label from './Label';

const labelTransformedStyles = {
  transform: `translateY(-${rem(48)})`,
  fontSize: rem(12),
};

const focused = '&:focus:not([disabled]):not([readonly])';

const InnerInput = styled.input(
  {
    display: 'block',
    width: '100%',
    padding: `${rem(8)} 0`,
    fontSize: rem(16),
    backgroundColor: 'transparent',
    border: 0,
    borderBottom: `1px solid ${rgba('black', 0.12)}`,
    outline: 'none',

    [`&:-webkit-autofill,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus,
    &:-webkit-autofill:active`]: {
      WebkitTransition: 'color 9999s ease-out, background-color 9999s ease-out',
      WebkitTransitionDelay: '9999s',
    },

    '&[disabled]': {
      color: rgba('black', 0.26),
      borderBottomStyle: 'dotted',
    },

    [`${focused} ~ ${Label},
      &:[type='date'] ~ ${Label},
      &:[type='time'] ~ ${Label}`]: labelTransformedStyles,
  },
  ({ error, value, theme }) => {
    const errorStyles = error && {
      borderBottomColor: rgb(222, 50, 38),
    };

    const filledStyles = value &&
      value !== '' && {
        '& ~ .Input__Label': labelTransformedStyles,
        '& ~ .Input__Hint': { display: 'none' },
      };

    return {
      color: get(theme, ['color', 'text'], 'black'),
      ...errorStyles,

      [focused]: {
        [`& ~ .Input__Bar::before,
          & ~ .Input__Bar::after`]: { width: '50%' },

        '& ~ .Input__Hint': { opacity: 1 },

        [`& ~ .Input__Label,
          & ~ .Input__IconWrapper`]: {
          color: get(theme, ['color', 'primary'], 'blue'),
        },

        '& ~ .Input__Label::after': {
          color: rgb(222, 50, 38),
        },
      },

      ...filledStyles,
    };
  },
);

export default InnerInput;
