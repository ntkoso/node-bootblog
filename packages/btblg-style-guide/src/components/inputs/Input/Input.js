// @flow
import React from 'react';
import type { Element } from 'react';
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';
import Bar from './Bar';
import Hint from './Hint';
import IconWrapper from './IconWrapper';
import InnerInput from './InnerInput';
import InputError from './InputError';
import Label from './Label';

const wrapperMargins = ({
  error,
  hasIcon,
}: {
  error?: boolean,
  hasIcon?: boolean,
}) => ({
  paddingBottom: error ? 0 : undefined,
  marginLeft: hasIcon ? rem(48) : 0,
});

const Wrapper = styled.div(
  {
    position: 'relative',
    padding: `${rem(20)} 0`,
  },
  wrapperMargins,
);

type Props = {
  field: {|
    +name: string,
    +value: any,
    +onChange: Function,
    +onBlur: Function,
  |},
  form: { touched: { [key: string]: boolean }, errors: { [key: string]: any } },
  hint?: string,
  label?: string,
  icon?: Element<*>,
  disabled?: boolean,
  required?: boolean,
  renderError?: (err: { path: string, message: string }) => Node,
  type?: string,
};

export const Input = ({
  field,
  form,
  hint,
  label,
  icon,
  disabled,
  required,
  renderError,
  type = 'text',
}: Props): Element<typeof Wrapper> => {
  const { touched, errors } = form;
  const { name } = field;
  const hasError = Boolean(touched[name] && errors[name]);

  return (
    <Wrapper error={hasError} hasIcon={Boolean(icon)}>
      <InnerInput
        type={type}
        error={hasError}
        disabled={disabled}
        required={required}
        aria-label={label}
        {...field}
      />
      {label && (
        <Label
          className="Input__Label"
          disabled={disabled}
          error={hasError}
          required={required}
        >
          {label}
        </Label>
      )}
      {icon && <IconWrapper className="Input__IconWrapper">{icon}</IconWrapper>}
      <Bar className="Input__Bar" />
      {hint && <Hint className="Input__Hint">{hint}</Hint>}
      {touched[name] &&
        errors[name] && (
          <InputError name={name}>
            {typeof renderError === 'function'
              ? renderError({ path: name, message: errors[name] })
              : errors[name]}
          </InputError>
        )}
    </Wrapper>
  );
};

export default Input;
