// @flow
import React from 'react';
import type { Node } from 'react';

type Props = {
  children: Node,
  name: string,
};

const InputError = ({ children, name }: Props) => <div>{children}</div>;

export default InputError;
