// @flow
import styled from '@ntkoso/react-emotion';
import { rgb, rgba, rem } from 'polished';
import { transition } from '../../../mixins';

const Label = styled.label(
  {
    position: 'absolute',
    transform: `translateY(-${rem(26)})`,
    left: 0,
    paddingBottom: rem(8),
    fontSize: rem(16),
    lineHeight: rem(16),
    pointerEvents: 'none',
  },
  ({ error }, theme) => ({
    color: error ? rgb(222, 50, 38) : rgba('black', 0.26),
    ...transition(['transform', 'fontSize', 'color'], theme),
  }),
);

export default Label;
