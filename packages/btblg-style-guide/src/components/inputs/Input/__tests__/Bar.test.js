import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Bar from '../Bar';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Bar renders correctly', () => {
  const element = create(
    <Bar
      theme={{
        color: {
          text: 'grey',
        },
        animation: {
          curve: 'linear',
          duration: 200,
          delay: 200 / 5,
        },
      }}
    >
      TEST
    </Bar>,
  );

  expect(element.toJSON()).toMatchSnapshot();
});
