import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Hint from '../Hint';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Hint renders correctly', () => {
  const element = create(
    <Hint
      theme={{
        color: {
          text: 'grey',
        },
        animation: {
          curve: 'linear',
          duration: 200,
          delay: 200 / 5,
        },
      }}
    >
      TEST
    </Hint>,
  );

  expect(element.toJSON()).toMatchSnapshot();
});
