import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import IconWrapper from '../IconWrapper';

expect.addSnapshotSerializer(createSerializer(emotion));

test('IconWrapper renders correctly', () => {
  const element = create(<IconWrapper>TEST</IconWrapper>);

  expect(element.toJSON()).toMatchSnapshot();
});
