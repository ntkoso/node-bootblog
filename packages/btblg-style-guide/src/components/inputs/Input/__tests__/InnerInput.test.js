import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import InnerInput from '../InnerInput';

expect.addSnapshotSerializer(createSerializer(emotion));

test('InnerInput renders correctly', () => {
  const element = create(
    <InnerInput
      theme={{
        color: {
          text: 'grey',
        },
      }}
    >
      TEST
    </InnerInput>,
  );

  expect(element.toJSON()).toMatchSnapshot();
});
