import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Input from '../Input';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Input renders correctly', () => {
  const element = create(
    <Input field={{ name: 'test' }} form={{ touched: {}, errors: {} }}>
      TEST
    </Input>,
  );

  expect(element.toJSON()).toMatchSnapshot();
});

test('Input renders with label', () => {
  const element = create(
    <Input
      field={{ name: 'test' }}
      form={{ touched: {}, errors: {} }}
      label="Test field label"
    >
      TEST
    </Input>,
  );

  expect(element.toJSON()).toMatchSnapshot();
});

test('Input renders with icon', () => {
  const element = create(
    <Input
      field={{ name: 'test' }}
      form={{ touched: {}, errors: {} }}
      icon={<img alt="test icon" />}
    >
      TEST
    </Input>,
  );

  expect(element.toJSON()).toMatchSnapshot();
});

test('Input renders with hint', () => {
  const element = create(
    <Input
      field={{ name: 'test' }}
      form={{ touched: {}, errors: {} }}
      hint="Test field hint"
    >
      TEST
    </Input>,
  );

  expect(element.toJSON()).toMatchSnapshot();
});

test('Input renders with error', () => {
  const element = create(
    <Input
      field={{ name: 'test' }}
      form={{ touched: { test: true }, errors: { test: 'Error' } }}
    >
      TEST
    </Input>,
  );

  expect(element.toJSON()).toMatchSnapshot();
});

test('Input renders with label, icon, hint and error', () => {
  const element = create(
    <Input
      field={{ name: 'test' }}
      form={{ touched: { test: true }, errors: { test: 'Error' } }}
      label="Test field label"
      icon={<img alt="test icon" />}
      hint="Test field hint"
    >
      TEST
    </Input>,
  );

  expect(element.toJSON()).toMatchSnapshot();
});
