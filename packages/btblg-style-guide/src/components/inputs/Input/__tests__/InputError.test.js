import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import InputError from '../InputError';

expect.addSnapshotSerializer(createSerializer(emotion));

test('InputError renders correctly', () => {
  const element = create(<InputError>TEST</InputError>);

  expect(element.toJSON()).toMatchSnapshot();
});
