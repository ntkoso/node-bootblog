import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Label from '../Label';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Label renders correctly', () => {
  const element = create(<Label error={false}>TEST</Label>);

  expect(element.toJSON()).toMatchSnapshot();
});

test('Error label renders correctly', () => {
  const element = create(<Label error>TEST</Label>);

  expect(element.toJSON()).toMatchSnapshot();
});
