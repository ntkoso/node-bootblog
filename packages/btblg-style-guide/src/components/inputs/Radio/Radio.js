// @flow
import React from 'react';
import type { Node, Element } from 'react';
import styled from '@ntkoso/react-emotion';
import { withProps } from 'recompose';

const Radio = withProps({ type: 'radio' })(styled.input({}));

export default Radio;
