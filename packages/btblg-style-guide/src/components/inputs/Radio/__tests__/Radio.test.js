import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Radio from '../Radio';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Radio renders correctly', () => {
  const element = create(<Radio>TEST</Radio>);

  expect(element.toJSON()).toMatchSnapshot();
});
