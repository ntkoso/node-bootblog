// @flow
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';

const Select = styled.select({
  borderRadius: rem(4),
});

export default Select;
