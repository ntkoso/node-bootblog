import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Select from '../Select';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Select renders correctly', () => {
  const element = create(<Select>TEST</Select>);

  expect(element.toJSON()).toMatchSnapshot();
});
