// @flow
import styled from '@ntkoso/react-emotion';
import { withProps } from 'recompose';

const Text = withProps({ type: 'text' })(styled.input({}));

export default Text;
