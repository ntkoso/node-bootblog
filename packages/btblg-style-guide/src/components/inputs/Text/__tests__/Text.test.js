import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import Text from '../Text';

expect.addSnapshotSerializer(createSerializer(emotion));

test('Text renders correctly', () => {
  const element = create(<Text>TEST</Text>);

  expect(element.toJSON()).toMatchSnapshot();
});
