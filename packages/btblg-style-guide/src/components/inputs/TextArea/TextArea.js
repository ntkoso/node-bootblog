// @flow
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';

const TextArea = styled.textarea({
  borderRadius: rem(4),
});

export default TextArea;
