import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import TextArea from '../TextArea';

expect.addSnapshotSerializer(createSerializer(emotion));

test('TextArea renders correctly', () => {
  const element = create(<TextArea>TEST</TextArea>);

  expect(element.toJSON()).toMatchSnapshot();
});
