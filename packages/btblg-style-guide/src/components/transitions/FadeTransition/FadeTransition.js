// @flow
import React from 'react';
import type { Node, Element } from 'react';
import { CSSTransition } from 'react-transition-group';
import { css } from '@ntkoso/react-emotion';

const duration = 300;

const transition = `opacity ${duration}ms ease-in`;

const enter = css({ opacity: 0 });
const enterActive = css({ opacity: 1, transition });

const exit = css({ opacity: 1 });
const exitActive = css({ opacity: 0, transition });

const appear = css({ opacity: 0 });
const appearActive = css({ opacity: 1, transition });

export const FadeTransition = ({
  children,
  ...props
}: {
  children: Node,
}): Element<typeof CSSTransition> => (
  <CSSTransition
    {...props}
    classNames={{
      enter,
      enterActive,
      exit,
      exitActive,
      appear,
      appearActive,
    }}
    timeout={{ enter: duration, exit: duration }}
  >
    {children}
  </CSSTransition>
);

export default FadeTransition;
