import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import FadeTransition from '../FadeTransition';

test('Wraps children component in CSSTransition', () => {
  const transitionComponent = create(
    <FadeTransition>
      <div appear>TEST</div>
    </FadeTransition>,
  );

  expect(transitionComponent.toJSON()).toMatchSnapshot();
});
