// @flow
import styled from '@ntkoso/react-emotion';
import { body } from '../../typography';

const Body = styled.span(body);

export default Body;
