// @flow
import styled from '@ntkoso/react-emotion';
import { rem } from 'polished';

const Button = styled.span({
  fontSize: rem(14),
  fontWeight: 600,
  textTransform: 'capitalize',
});

export default Button;
