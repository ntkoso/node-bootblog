// @flow
import styled from '@ntkoso/react-emotion';
import { content } from '../../typography';

const Content = styled.span(content, ({ bold }) => bold && ({
  fontWeight: 500,
}));

export default Content;
