// @flow strict
import styled from '@ntkoso/react-emotion';
import { h1 } from '../../typography';

const H1 = styled.h1(h1);

export default H1;
