// @flow strict
import styled from '@ntkoso/react-emotion';
import { h2 } from '../../typography';

const H2 = styled.h2(h2);

export default H2;
