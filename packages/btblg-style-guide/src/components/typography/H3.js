// @flow
import styled from '@ntkoso/react-emotion';
import { h3 } from '../../typography';

const H3 = styled.h3(h3);

export default H3;
