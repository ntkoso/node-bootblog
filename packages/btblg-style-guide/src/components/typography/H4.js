// @flow
import styled from '@ntkoso/react-emotion';
import { h4 } from '../../typography';

const H4 = styled.h4(h4);

export default H4;
