// @flow
import styled from '@ntkoso/react-emotion';
import { h5 } from '../../typography';

const H5 = styled.h5(h5);

export default H5;
