// @flow
import styled from '@ntkoso/react-emotion';
import { h6 } from '../../typography';

const H6 = styled.h6(h6);

export default H6;
