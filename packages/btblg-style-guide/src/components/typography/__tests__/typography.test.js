import React from 'react';
import { create } from 'react-test-renderer';
import { createSerializer } from 'jest-emotion';
import * as emotion from '@ntkoso/emotion';
import * as typography from '../index';

expect.addSnapshotSerializer(createSerializer(emotion));

test('typography components', () => {
  Object.values(typography).map(Component => {
    expect(create(<Component>TEST</Component>).toJSON()).toMatchSnapshot();
  });
});
