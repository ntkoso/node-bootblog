beforeEach(() => {
  jest.resetModules();
});

test('global css', () => {
  const mockedInjectGlobal = jest.fn(_ => {});

  jest.doMock('@ntkoso/react-emotion', () => ({
    injectGlobal: mockedInjectGlobal,
  }));

  const injectGlobalRules = require('../index').default;

  expect(injectGlobalRules).toBeInstanceOf(Function);
  injectGlobalRules();

  expect(mockedInjectGlobal.mock.calls[0]).toMatchSnapshot();
});
