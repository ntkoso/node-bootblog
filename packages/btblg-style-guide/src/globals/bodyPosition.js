// @flow
const bodyPosition = `
  body {
    position: absolute;
    width: 100%;
    height: 100%;
  }
`;

export default bodyPosition;
