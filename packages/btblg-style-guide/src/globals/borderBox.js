// @flow
const borderBox = `
  *, *::before, *::after {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
  }
`;

export default borderBox;
