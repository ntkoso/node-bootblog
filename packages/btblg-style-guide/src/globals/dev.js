// @flow
const dev = `
  *:not(input):not(textarea):not(select) {
    background-color: rgba(0, 0, 255, 0.1);
  }
`;

export default dev;
