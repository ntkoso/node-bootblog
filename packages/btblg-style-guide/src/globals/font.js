// @flow
import { body } from '../typography';

const mediumSystemFontstack = `
  font-family: -apple-system,
               BlinkMacSystemFont,
               "Segoe UI",
               Roboto,
               Oxygen,
               Ubuntu,
               Cantarell,
               "Open Sans",
               "Helvetica Neue",
               sans-serif;
`;

const font = `
  html {
    font-size: 100%;
    text-size-adjust: 100%;
  }

  body {
    ${mediumSystemFontstack}
    letter-spacing: 0;
    font-weight: 400;
    font-style: normal;

    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    font-size: ${body.fontSize};
    line-height: ${body.lineHeight}
  }
`;

export default font;
