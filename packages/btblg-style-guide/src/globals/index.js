// @flow
import { injectGlobal } from '@ntkoso/react-emotion';
import { rem, rgba } from 'polished';
import normalize from './libs/normalize';
import reactVirtualized from './libs/reactVirtualized';
import viewport from './viewport';
import borderBox from './borderBox';
import dev from './dev';
import bodyPosition from './bodyPosition';
import font from './font';

const blockquote = `
  .blockquote {
    padding-left: ${rem(11)};
    margin-left: ${rem(9)};
    background-color: ${rgba('red', 0.24)} /* TODO: same as below */
    border-left: ${rem(4)} solid red; /* TODO: replace with primary color */
  }
`;

const staticStyle = `
  ${normalize}
  ${viewport}
  ${borderBox}
  ${dev}
  ${bodyPosition}
  ${font}
  ${reactVirtualized}
  ${blockquote}
`;

const insertGlobalRules = (): void => injectGlobal(staticStyle);

export default insertGlobalRules;
