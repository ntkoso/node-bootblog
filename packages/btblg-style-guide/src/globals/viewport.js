// @flow
const viewport = `
  @viewport {
    zoom: 1.0;
    width: device-width;
  }
`;

export default viewport;
