import shadow from '../shadow';

test('shadow mixin', () => {
  const params = ['focus', undefined, ...Array(25).keys()];

  const testFn = p => expect(shadow(p)).toMatchSnapshot(`${p} param`);

  params.forEach(testFn);
});
