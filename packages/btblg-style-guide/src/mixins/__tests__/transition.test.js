import transition from '../transition';

const settings = { animation: { duration: 300, curve: 'ease-in' } };

test('single property', () => {
  expect(transition('color', settings)).toMatchSnapshot();
});

test('multiple properties', () => {
  expect(transition(['color', 'backgroundColor'], settings)).toMatchSnapshot();
});

test('default settings', () => {
  expect(transition('color')).toMatchSnapshot();
});
