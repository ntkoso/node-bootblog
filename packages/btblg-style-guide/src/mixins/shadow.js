// @flow
import { rgba } from 'polished';

const blcka: Blcka = (opacity: number): string => rgba('black', opacity);

type BoxShadowStyle = {| boxShadow: string |};

const generateBoxShadow = (
  umbra: string,
  penumbra: string,
  ambient: string,
): BoxShadowStyle => ({
  boxShadow: `${umbra} ${blcka(0.2)},
              ${penumbra} ${blcka(0.14)},
              ${ambient} ${blcka(0.12)}`,
});

const focus: BoxShadowStyle = {
  boxShadow: `0 0 8px ${blcka(0.18)},
              0 8px 16px ${blcka(0.36)}`,
};

const dp1 = generateBoxShadow(
  '0px 2px 1px -1px',
  '0px 1px 1px 0px',
  '0px 1px 3px 0px',
);

const dp2 = generateBoxShadow(
  '0px 3px 1px -2px',
  '0px 2px 2px 0px',
  '0px 1px 5px 0px',
);

const dp3 = generateBoxShadow(
  '0px 3px 3px -2px',
  '0px 3px 4px 0px',
  '0px 1px 8px 0px',
);

const dp4 = generateBoxShadow(
  '0px 2px 4px -1px',
  '0px 4px 5px 0px',
  '0px 1px 10px 0px',
);

const dp5 = generateBoxShadow(
  '0px 3px 5px -1px',
  '0px 5px 8px 0px',
  '0px 1px 14px 0px',
);

const dp6 = generateBoxShadow(
  '0px 3px 5px -1px',
  '0px 6px 10px 0px',
  '0px 1px 18px 0px',
);

const dp7 = generateBoxShadow(
  '0px 4px 5px -2px',
  '0px 7px 10px 1px',
  '0px 2px 16px 1px',
);

const dp8 = generateBoxShadow(
  '0px 5px 5px -3px',
  '0px 8px 10px 1px',
  '0px 3px 14px 2px',
);

const dp9 = generateBoxShadow(
  '0px 5px 6px -3px',
  '0px 9px 12px 1px',
  '0px 3px 16px 2px',
);

const dp10 = generateBoxShadow(
  '0px 6px 6px -3px',
  '0px 10px 14px 1px',
  '0px 4px 18px 3px',
);

const dp11 = generateBoxShadow(
  '0px 6px 7px -4px',
  '0px 11px 15px 1px',
  '0px 4px 20px 3px',
);

const dp12 = generateBoxShadow(
  '0px 7px 8px -4px',
  '0px 12px 17px 2px',
  '0px 5px 22px 4px',
);

const dp13 = generateBoxShadow(
  '0px 7px 8px -4px',
  '0px 13px 19px 2px',
  '0px 5px 24px 4px',
);

const dp14 = generateBoxShadow(
  '0px 7px 9px -4px',
  '0px 14px 21px 2px',
  '0px 5px 26px 4px',
);

const dp15 = generateBoxShadow(
  '0px 8px 9px -5px',
  '0px 15px 22px 2px',
  '0px 6px 28px 5px',
);

const dp16 = generateBoxShadow(
  '0px 8px 10px -5px',
  '0px 16px 24px 2px',
  '0px 6px 30px 5px',
);

const dp17 = generateBoxShadow(
  '0px 8px 11px -5px',
  '0px 17px 26px 2px',
  '0px 6px 32px 5px',
);

const dp18 = generateBoxShadow(
  '0px 9px 11px -5px',
  '0px 18px 28px 2px',
  '0px 7px 34px 6px',
);

const dp19 = generateBoxShadow(
  '0px 9px 12px -6px',
  '0px 19px 29px 2px',
  '0px 7px 36px 6px',
);

const dp20 = generateBoxShadow(
  '0px 10px 13px -6px',
  '0px 20px 31px 3px',
  '0px 8px 38px 7px',
);

const dp21 = generateBoxShadow(
  '0px 10px 13px -6px',
  '0px 21px 33px 3px',
  '0px 8px 40px 7px',
);

const dp22 = generateBoxShadow(
  '0px 10px 14px -6px',
  '0px 22px 35px 3px',
  '0px 8px 42px 7px',
);

const dp23 = generateBoxShadow(
  '0px 11px 14px -7px',
  '0px 23px 36px 3px',
  '0px 9px 44px 8px',
);

const dp24 = generateBoxShadow(
  '0px 11px 15px -7px',
  '0px 24px 38px 3px',
  '0px 9px 46px 8px',
);

const shadow = (level?: number | string): BoxShadowStyle => {
  switch (level) {
    case 'focus':
      return focus;

    case 1:
      return dp1;
    case 2:
      return dp2;
    case 3:
      return dp3;
    case 4:
      return dp4;
    case 5:
      return dp5;
    case 6:
      return dp6;
    case 7:
      return dp7;
    case 8:
      return dp8;
    case 9:
      return dp9;
    case 10:
      return dp10;
    case 11:
      return dp11;
    case 12:
      return dp12;
    case 13:
      return dp13;
    case 14:
      return dp14;
    case 15:
      return dp15;
    case 16:
      return dp16;
    case 17:
      return dp17;
    case 18:
      return dp18;
    case 19:
      return dp19;
    case 20:
      return dp20;
    case 21:
      return dp21;
    case 22:
      return dp22;
    case 23:
      return dp23;
    case 24:
      return dp24;

    default:
      return {
        boxShadow: 'none',
      };
  }
};

export default shadow;
