// @flow
import castArray from 'cast-array';
import dashify from 'dashify';

type Properties = Array<string> | string;
type AnimationThemeParts = {
  animation?: { duration?: number, curve?: string },
};

type TransitionStyle = {
  transitionTimingFunction: string,
  transitionDuration: string,
  transitionProperty: string,
};

const transitionProps = (props: Properties): string =>
  castArray(props)
    .map(dashify)
    .join(', ');

const transition = (
  properties: Properties,
  {
    animation: { duration = 200, curve = 'ease-in-out' } = {},
  }: AnimationThemeParts = {},
): TransitionStyle => ({
  transitionTimingFunction: curve,
  transitionDuration: `${duration}ms`,
  transitionProperty: transitionProps(properties),
});

export default transition;
