import theme from '../light';

test('animation', () => {
  expect(theme.animation).toMatchSnapshot();
});

test('colors', () => {
  expect(theme.color).toMatchSnapshot();
});

test('maxWidth', () => {
  expect(theme.maxWidth).toMatchSnapshot();
});
