// @flow
import { rem } from 'polished';
import type { Animation, Color, MaxWidth, Theme } from '../types';

const animationCurves = {
  fastOutLinearIn: 'cubic-bezier(0.4, 0, 1, 1)',
  fastOutSlowIn: 'cubic-bezier(0.4, 0, 0.2, 1)',
  linearOutSlowIn: 'cubic-bezier(0, 0, 0.2, 1)',
};

const animation: Animation = {
  curve: animationCurves.fastOutSlowIn,

  duration: 200,
  delay: 200 / 5,
};

const color: Color = {
  black: '#444444',
  white: '#FFFFFF',

  primary: '#3F51B5',
  primaryDarker: '#303F9F',
  primaryLighter: '#C5CAE9',

  accent: '#FFD740',
  accentDarker: '#FFC400',
  accentLighter: '#FFE57F',

  text: '#212121',
  textSecondary: '#757575',

  divider: '#EEEEEE',
};

const maxWidth: MaxWidth = rem(1440);

const theme: Theme = {
  animation,
  color,
  maxWidth,
};

export default theme;
