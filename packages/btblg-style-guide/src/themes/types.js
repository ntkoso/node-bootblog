// @flow
export type MaxWidth = string | number;

export type Animation = {
  curve: string,
  duration: number | string,
  delay: number | string,
};

export type Color = {
  black: string,
  white: string,
  primary: string,
  primaryDarker: string,
  primaryLighter: string,
  accent: string,
  accentDarker: string,
  accentLighter: string,
  text: string,
  textSecondary: string,
  divider: string,
};

export type Theme = {
  animation: Animation,
  color: Color,
  maxWidth: MaxWidth,
};
