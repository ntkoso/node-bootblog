import Shevy from 'shevyjs';

export const {
  lineHeightSpacing: lhs,
  baseSpacing: bs,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  body,
  content,
} = new Shevy({
  baseFontSize: '1rem',
  proximity: true,
});
