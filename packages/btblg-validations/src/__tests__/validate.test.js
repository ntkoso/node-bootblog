import { object, string } from 'yup';
import validate from '../validate';

const schema = object().shape({
  test: string()
    .min(3)
    .required(),
});

test("returns result when it's valid", async () => {
  expect.assertions(1);
  const { result } = await validate(schema, { test: 'test' });

  expect(result).toEqual({ test: 'test' });
});

test('returns ValidationError when data is invalid', async () => {
  expect.assertions(1);
  const { errors } = await validate(schema, {});

  expect(errors).toMatchSnapshot();
});
