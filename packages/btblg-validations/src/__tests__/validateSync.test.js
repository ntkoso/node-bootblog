import { object, string } from 'yup';
import validateSync from '../validateSync';

const schema = object().shape({
  test: string()
    .min(3)
    .required(),
});

test("returns result when it's valid", () => {
  const { result } = validateSync(schema, { test: 'test' });

  expect(result).toEqual({ test: 'test' });
});

test('returns ValidationError when data is invalid', async () => {
  const { errors } = validateSync(schema, {});

  expect(errors).toMatchSnapshot();
});
