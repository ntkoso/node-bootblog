import validationError from '../validationError';

test('generates validation error data', () => {
  expect(validationError('error', 'value', 'path')).toMatchSnapshot();
});
