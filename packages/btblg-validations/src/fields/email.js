// @flow strict
import { string } from 'yup';

const email = string()
  .required('validation.email.required')
  .email('validation.email.invalid');

export default email;
