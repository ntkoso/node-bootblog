// @flow strict
import { string } from 'yup';

const password = string()
  .required('validation.password.required')
  .test('password-long', 'validation.password.long', value =>
    /^[\s\S]{8,}$/.test(value),
  )
  .test(
    'password-uppercase-letters',
    'validation.password.uppercase_letters',
    value => /[A-Z]/.test(value),
  )
  .test(
    'password-lowercase-letters',
    'validation.password.lowercase_letters',
    value => /[a-z]/.test(value),
  )
  .test('passsword-has-numbers', 'validation.password.numbers', value =>
    /[0-9]/.test(value),
  );

export default password;
