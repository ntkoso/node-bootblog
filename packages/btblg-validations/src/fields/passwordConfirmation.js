// @flow strict
import { string } from 'yup';

const passwordConfirmation = string().when(
  'password',
  (pass, schema) => (pass ? schema.equals([pass]) : schema),
);

export default passwordConfirmation;
