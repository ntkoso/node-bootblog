// @flow strict
import { string } from 'yup';

const recaptcha = string().required('validation.recaptcha.required');

export default recaptcha;
