// @flow strict
import { string } from 'yup';

const username = string()
  .required('validation.username.required')
  .min(3, 'validation.username.min');

export default username;
