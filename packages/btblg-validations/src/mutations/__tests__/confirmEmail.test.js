import confirmEmailValidations from '../confirmEmail';

test('catch invalid data', () => {
  try {
    confirmEmailValidations.validateSync({
      confirmationToken: ''
    });
  } catch (err) {
    expect(err).toMatchSnapshot();
  }
});

test('returns valid data', () => {
  expect(
    confirmEmailValidations.validateSync({
      confirmationToken: 'token'
    }),
  ).toMatchSnapshot();
});
