import signInValidations from '../signIn';

test('catch invalid data', () => {
  try {
    signInValidations.validateSync({
      password: 'test',
      email: 'test',
    });
  } catch (err) {
    expect(err).toMatchSnapshot();
  }
});

test('returns valid data', () => {
  expect(
    signInValidations.validateSync({
      email: 'email@test.co',
      password: 'l0ngpAssworD',
    }),
  ).toMatchSnapshot();
});
