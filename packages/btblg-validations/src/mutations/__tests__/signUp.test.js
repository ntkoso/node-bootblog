import signUpValidations from '../signUp';

test('catch invalid data', () => {
  try {
    signUpValidations.validateSync({
      password: 'test',
      email: 'test',
    });
  } catch (err) {
    expect(err).toMatchSnapshot();
  }
});

test('returns valid data', () => {
  expect(
    signUpValidations.validateSync({
      email: 'email@test.co',
      username: 'Test',
      password: 'l0ngpAssworD',
      passwordConfirmation: 'l0ngpAssworD',
      recaptcha: 'test',
    }),
  ).toMatchSnapshot();
});
