// @flow strict
import { object, string } from 'yup';

const confirmEmail = object({
  confirmationToken: string().required('validation.confirmationToken.required'),
});

export default confirmEmail;
