// @flow strict
import { object } from 'yup';
import { email, password } from '../fields';

const signIn = object({
  email,
  password,
});

export default signIn;
