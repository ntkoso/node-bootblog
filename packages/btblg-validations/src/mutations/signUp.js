// @flow strict
import { object } from 'yup';
import {
  email,
  username,
  password,
  passwordConfirmation,
  recaptcha,
} from '../fields';

const signUp = object({
  email,
  username,
  password,
  passwordConfirmation,
  recaptcha,
});

export default signUp;
