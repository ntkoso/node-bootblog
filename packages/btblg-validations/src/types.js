// @flow strict
export type ValidationError = {
  name: 'ValidationError',
  path?: string,
  errors: Array<string>,
  inner: Array<ValidationError>,
};

export type ValidationResult<V> =
  | {| result: V |}
  | {| errors: ValidationError |};

export type Schema<V> = {
  validate: (value: V, options?: { abortEarly?: boolean }) => Promise<V>,
  validateSync: (value: V, options?: { abortEarly?: boolean }) => V,
};
