// @flow strict
import type { ValidationError, ValidationResult, Schema } from './types';

const validate = <V>(
  schema: Schema<V>,
  value: V,
  abortEarly?: boolean = false,
): Promise<ValidationResult<V>> =>
  schema
    .validate(value, { abortEarly })
    .then(result => ({ result }))
    .catch((errors: ValidationError) => ({ errors }));

export default validate;
