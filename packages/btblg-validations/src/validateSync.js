// @flow strict
import type { ValidationError, ValidationResult, Schema } from './types';

const validateSync = <V>(
  schema: Schema<V>,
  value: V,
  abortEarly?: boolean = false,
): ValidationResult<V> => {
  try {
    const result = schema.validateSync(value, { abortEarly });

    return { result };
  } catch (error) {
    const err = (error: ValidationError);

    return { errors: err };
  }
};

export default validateSync;
