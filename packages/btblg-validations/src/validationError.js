// @flow strict
import { ValidationError } from 'yup';

type Errors = string | Array<string>;

const pickErrorFields = ({ path, message, errors, inner }) => ({
  path,
  message,
  errors,
  inner,
});

const validationError = (errors: Errors, value?: mixed, path?: string) =>
  pickErrorFields(new ValidationError(errors, value, path));

export default validationError;
