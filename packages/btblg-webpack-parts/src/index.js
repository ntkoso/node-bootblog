// @flow
export {
  brotli as brotliPlugin,
  bundleAnalyzer as bundleAnalyzerPlugin,
  gzip as gzipPlugin,
  inlineEnv as inlineEnvPlugin,
  reactIntlLocalesPick as reactIntlLocalesPickPlugin,
  stats as statsPlugin,
} from './plugins';

export {
  minimizer as minimizerOptimization,
  runtimeChunk as runtimeChunkOptimization,
} from './optimization';

export {
  fonts as fontsRule,
  gql as gqlRule,
  js as jsRule,
  svgr as svgrRule,
} from './rules';

export { resolve as resolveSettings } from './settings';
