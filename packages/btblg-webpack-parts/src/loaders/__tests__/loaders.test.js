const { babel, cache } = require('../index');

test('babel', () => {
  expect(babel({})).toMatchSnapshot();
});

test('cache', () => {
  expect(cache('server', 'test')).toMatchSnapshot();
});
