// @flow

type Result<C> = {|
  loader: 'babel-loader',
  options: { ...C, babelrc: false },
|};

const babel = <C: {}>(config: C): Result<C> => ({
  loader: 'babel-loader',
  options: Object.assign({}, config, { babelrc: false }),
});

export default babel;
