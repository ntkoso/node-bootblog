// @flow
const env = process.env.NODE_ENV;

type Result = {|
  loader: 'cache-loader',
  options: {| cacheDirectory: string |},
|};

const cache = (type: string, name: string): Result => ({
  loader: 'cache-loader',
  options: {
    cacheDirectory: `/tmp/webpack/cache-loader/${name}-${type}${
      typeof env === 'string' ? `-${env}` : ''
    }`,
  },
});

export default cache;
