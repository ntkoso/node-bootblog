import { minimizer, runtimeChunk } from '../index';

test('minimizer optimization option', () => {
  expect(minimizer).toMatchSnapshot();
});

test('runtimeChunk optimization option', () => {
  expect(runtimeChunk).toMatchSnapshot();
});
