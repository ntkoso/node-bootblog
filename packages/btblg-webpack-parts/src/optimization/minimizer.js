// @flow
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';

const minimizer = [new UglifyJsPlugin({
  uglifyOptions: {
    compress: { passes: 2 },
  },
})];

export default minimizer;
