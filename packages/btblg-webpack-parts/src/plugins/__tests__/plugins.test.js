const {
  brotli,
  bundleAnalyzer,
  gzip,
  inlineEnv,
  reactIntlLocalesPick,
  stats,
} = require('../index');

test('brotli', () => {
  expect(brotli()).toMatchSnapshot();
});

test('bundleAnalyzer', () => {
  expect(bundleAnalyzer()).toMatchSnapshot();
});

test('gzip', () => {
  expect(gzip()).toMatchSnapshot();
});

test('inlineEnv', () => {
  expect(inlineEnv('server')).toMatchSnapshot();
});

test('reactIntlLocalesPick', () => {
  expect(reactIntlLocalesPick()).toMatchSnapshot();
  expect(reactIntlLocalesPick(['en', 'fr'])).toMatchSnapshot();
});

test('stats', () => {
  expect(stats()).toMatchSnapshot();
});
