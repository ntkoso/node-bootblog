// @flow
import BrotliWebpackPlugin from 'brotli-webpack-plugin';

const brotli = () =>
  new BrotliWebpackPlugin({
    asset: '[path].br[query]',
    test: /\.(js|css|html|svg)$/,
  });

export default brotli;
