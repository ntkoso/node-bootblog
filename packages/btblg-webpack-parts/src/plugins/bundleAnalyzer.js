// @flow
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

const bundleAnalyzer = () =>
  new BundleAnalyzerPlugin({
    analyzerMode: 'static',
    reportFilename: 'webpack-report.html',
    openAnalyzer: false,
  });

export default bundleAnalyzer;
