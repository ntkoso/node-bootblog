// @flow
import ZopfliWebpackPlugin from 'zopfli-webpack-plugin';

const gzip = () =>
  new ZopfliWebpackPlugin({
    asset: '[path].gz[query]',
    algorithm: 'zopfli',
    test: /\.(js|css|html|svg)$/,
  });

export default gzip;
