// @flow
import webpack from 'webpack';

const inlineEnv = (target: string) =>
  new webpack.EnvironmentPlugin({
    NODE_ENV: 'development',
    BUILD_TARGET: target,
    DEBUG: false,
    GRAPHQL_PREFIX: 'graphql',
  });

export default inlineEnv;
