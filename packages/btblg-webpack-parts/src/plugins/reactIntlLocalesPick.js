// @flow
import webpack from 'webpack';

const reactIntlLocalesPick = (
  locales?: Array<string> = ['en', 'ru'],
): webpack.ContextReplacementPlugin => {
  const regex = new RegExp(`^./${locales.join('|')}$`);

  return new webpack.ContextReplacementPlugin(
    /react-intl[\/\\]locale-data/,
    regex,
  );
};

export default reactIntlLocalesPick;
