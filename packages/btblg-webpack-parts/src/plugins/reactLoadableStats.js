// @flow
import fs from 'fs';
import path from 'path';
import url from 'url';

function buildManifest(compiler, compilation) {
  const context = compiler.options.context;
  const manifest = {};

  for (const chunkGroup of compilation.chunkGroups) {
    const files = [];
    for (const chunk of chunkGroup.chunks) {
      for (const file of chunk.files) {
        const publicPath = url.resolve(
          compilation.outputOptions.publicPath || '',
          file,
        );
        files.push({
          file,
          publicPath,
          chunkName: chunk.name,
        });
      }
    }

    for (const block of chunkGroup.blocksIterable) {
      let name;
      let id = null;
      const dependency =
        block.module &&
        block.module.dependencies &&
        block.module.dependencies.find(dep => block.request === dep.request);

      if (dependency) {
        const module = dependency.module;
        id = module.id;
        name =
          typeof module.libIdent === 'function'
            ? module.libIdent({ context })
            : null;
      }

      for (const file of files) {
        file.id = id;
        file.name = name;
      }

      manifest[block.request] = files;
    }
  }

  return manifest;
}

class ReactLoadablePlugin {
  constructor(opts = {}) {
    this.filename = opts.filename;
  }

  apply(compiler) {
    compiler.hooks.emit.tapAsync(
      'ReactLoadablePlugin',
      (compilation, callback) => {
        const manifest = buildManifest(compiler, compilation);
        const json = JSON.stringify(manifest, null, 2);
        const outputDirectory = path.dirname(this.filename);
        try {
          fs.mkdirSync(outputDirectory);
        } catch (err) {
          if (err.code !== 'EEXIST') {
            throw err;
          }
        }
        fs.writeFileSync(this.filename, json);
        callback();
      },
    );
  }
}

const reactLoadableStats = opts => new ReactLoadablePlugin(opts);

export default reactLoadableStats;
