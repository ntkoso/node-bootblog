// @flow
import { StatsWriterPlugin } from 'webpack-stats-plugin';

const transform = <Assets>(stats: {
  assetsByChunkName: Assets,
  publicPath: string,
}): string =>
  JSON.stringify({
    assetsByChunkName: stats.assetsByChunkName,
    publicPath: stats.publicPath,
  });

const stats = (): StatsWriterPlugin =>
  new StatsWriterPlugin({
    fields: ['assetsByChunkName', 'publicPath'],
    transform,
  });

export default stats;
