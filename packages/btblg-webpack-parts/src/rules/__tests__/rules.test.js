const { fonts, gql, js } = require('../index');

test('fonts', () => {
  expect(fonts('./output')).toMatchSnapshot();
});

test('gql', () => {
  expect(gql()).toMatchSnapshot();
});

test('js', () => {
  expect(
    js({
      target: 'server',
      name: 'test',
      babelrc: {},
    }),
  ).toMatchSnapshot();
});
