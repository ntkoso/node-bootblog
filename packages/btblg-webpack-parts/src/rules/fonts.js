// @flow
const env = process.env.NODE_ENV;

type Result = {|
  test: RegExp,
  loader: 'file-loader',
  options: {|
    limit: number,
    mimetype: string,
    name: string,
    outputPath: string,
  |},
|};

const fonts = (outputPath: string): Result => ({
  test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
  loader: 'file-loader',
  options: {
    limit: 50000,
    mimetype: 'application/woff',
    name:
      env !== 'production' ? `fonts/[name].[ext]` : `fonts/[name].[hash].[ext]`,
    outputPath,
  },
});

export default fonts;
