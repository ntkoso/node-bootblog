// @flow

type Result = {|
  test: RegExp,
  exclude: RegExp,
  loader: 'graphql-tag/loader',
|};

const gql = (): Result => ({
  test: /\.(graphql|gql)$/,
  exclude: /node_modules/,
  loader: 'graphql-tag/loader',
});

export default gql;
