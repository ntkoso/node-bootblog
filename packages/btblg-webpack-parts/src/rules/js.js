// @flow
import { cache, babel } from '../loaders';

type Config = {| target: string, name: string, babelrc: {} |};

type Result = {| test: RegExp, exclude: RegExp, use: [*, *] |};

const js = ({ target, name, babelrc }: Config): Result => ({
  test: /\.jsx?$/,
  exclude: /node_modules/,
  use: [cache(target, name), babel(babelrc)],
});

export default js;
