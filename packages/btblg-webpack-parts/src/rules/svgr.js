// @flow

type Result = {|
  test: RegExp,
  use: [{ loader: 'svgr/webpack', options: {} }],
|};

const svgr = (options: {}): Result => ({
  test: /\.svg$/,
  use: [{ loader: 'svgr/webpack', options }],
});

export default svgr;
