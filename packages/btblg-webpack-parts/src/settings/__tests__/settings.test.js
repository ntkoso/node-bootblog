const settings = require('../index');

test('settings', () => {
  Object.values(settings).forEach(setting =>
    expect(setting()).toMatchSnapshot(),
  );
});
