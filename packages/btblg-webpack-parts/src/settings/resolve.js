// @flow
type Result = {| modules: Array<string>, symlinks: false |};

const resolve = (): Result => ({
  modules: ['./src', './src/modules', 'node_modules'],
  symlinks: false,
});

export default resolve;
