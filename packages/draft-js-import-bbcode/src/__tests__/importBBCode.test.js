import { ContentState } from 'draft-js';
import importBBCode from '../importBBCode';
import bbcodeTags from '../tags';

test('importBBCode basic functionality', () => {
  expect(
    importBBCode(
      `[img width="300" height="244"]http://noviyvitok.com//uploads/images/4fec608de1fe2.jpg[/img]

 [i]Нет.. ближе к обеду - я уже другой человек. Но вот с утра..

 Помните у Лермонтова..[/i]

 [b]Одну молитву чудную[/b]
 [b]Твержу я наизусть.[/b]

 [i]]Нет, по утрам мне молитва не вспоминается, скорее бесы с чертом..))
 А у вас какое утро??
 Где-то мне попал список [b]"25 способов начать свое утро"[/b]..Кое-что понравилось.[/i]
 [i]Сейчас..если найду...[/i]

 [b]1. Медитация[/b]

 [b]5. Откройте все окна[/b]

 [b]13. Напишите [url="http://noviyvitok.com/thread/188/show"][u]благодарственное[/u][/url] письмо[/b]

 [b]15. Выучите новые слова на иностранном языке[/b]

 [b]22. Встречайте [url="http://noviyvitok.com/thread/253/show"][u]восход Солнца[/u][/url][/b]

 [b]24. Отмените одну задачу[/b]

 [i]Круто, да?? )) На самом деле я могу поторчать на балконе пока вскипает вода для кофе, а потом посерфить любимый форум. Это, кажется и приводит меня в чувство. В более бодрое настроение.))[/i]`,
      bbcodeTags,
    ),
  ).toBeInstanceOf(ContentState);
});
