// @flow
import BBCodeParseTree from 'bbcode-parser/bbCodeParseTree';
import { ContentState, ContentBlock, genKey } from 'draft-js';
import combineTextChunks from './internal/combineTextChunks';
import processTree from './internal/processTree';

const toContentBlock = ({ type, depth, textChunks }) => {
  const result = combineTextChunks(textChunks);
  const characterMeta = result.characterMeta;
  let text = result.text;

  text = text.replace('\r', '\n');

  return new ContentBlock({
    text,
    type,
    depth,
    key: genKey(),
    characterList: characterMeta.toList(),
  });
};

const treeToContentState = tree => {
  let chunks = [];

  chunks = processTree(chunks, tree);

  const contentBlocks = chunks.map(toContentBlock);

  return ContentState.createFromBlockArray(contentBlocks);
};

const importBBCode = (
  content: string,
  tags: { [key: string]: any },
): ContentState => {
  const bbcodeTree = BBCodeParseTree.buildTree(content, tags);

  if (!bbcodeTree || !bbcodeTree.isValid()) {
    return ContentState.createFromText(content);
  }

  const contentState = treeToContentState(bbcodeTree);

  return contentState;
};

export default importBBCode;
