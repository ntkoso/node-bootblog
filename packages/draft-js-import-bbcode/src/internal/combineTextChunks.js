// @flow
import { Seq } from 'immutable';

const combineTextChunks = (
  textChunks: Array<{| text: string, characterMeta: Seq |}>,
) => {
  const defaultTextChunk = {
    text: '',
    characterMeta: new Seq(),
  };

  return textChunks.reduce(
    ({ text, characterMeta }, textChunk) => ({
      text: text + textChunk.text,
      characterMeta: characterMeta.concat(textChunk.characterMeta),
    }),
    defaultTextChunk,
  );
};

export default combineTextChunks;
