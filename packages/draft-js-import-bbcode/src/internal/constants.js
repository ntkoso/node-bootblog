export const TREE_TYPES = {
  Root: 0,
  Text: 1,
  Tag: 2,
};

export const BLOCK_TAGS = ['quote'];

export const ATOMIC_TAGS = ['img', 'video'];
