// @flow
const trimSpacesFromTextChunk = textData => {
  let { text, characterMeta } = textData;

  while (text[0] === ' ') {
    text = text.slice(1);
    characterMeta = characterMeta.slice(1);
  }

  while (text[text.length - 1] === ' ') {
    text = text.slice(0, text.length - 1);
    characterMeta = characterMeta.slice(0, characterMeta.length - 1);
  }

  return { text, characterMeta };
};

const fixTextChunkWhitespace = textChunk => {
  const { text, characterMeta } = trimSpacesFromTextChunk(textChunk);

  return { text, characterMeta };
};

export default fixTextChunkWhitespace;
