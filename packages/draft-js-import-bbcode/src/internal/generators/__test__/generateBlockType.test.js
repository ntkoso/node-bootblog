import generateBlockType from '../generateBlockType';

test('generates default unstyled block type', () => {
  expect(generateBlockType()).toEqual('unstyled');
});

test('generates blockquote type for quote tag', () => {
  expect(generateBlockType({ tagName: 'quote' })).toEqual('blockquote');
});
