import generateEntity from '../generateEntity';

test('url bbtag', () => {
  const urlEntity = generateEntity({
    tagName: 'url',
    attributes: { url: 'http://imgur.com' },
  });

  expect(urlEntity).toMatchSnapshot();
});

test('img bbtag', () => {
  const imgEntity = generateEntity({
    tagName: 'img',
    content: '/img.jpg',
  });

  expect(imgEntity).toMatchSnapshot();
});

test('video bbtag', () => {
  const videoEntiity = generateEntity({
    tagName: 'video',
    attributes: { video: 'youtube' },
    content: 'HAuXJVI_bUs',
  });

  expect(videoEntiity).toMatchSnapshot();
});
