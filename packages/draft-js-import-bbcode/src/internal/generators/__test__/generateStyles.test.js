import { Set } from 'immutable';
import generateStyles from '../generateStyles';

let styleSet;

beforeEach(() => (styleSet = new Set()));

test('bold', () => {
  expect(generateStyles({ tagName: 'b' }, styleSet)).toMatchSnapshot('BOLD');
});

test('italic', () => {
  expect(generateStyles({ tagName: 'i' }, styleSet)).toMatchSnapshot('ITALIC');
});

test('underline', () => {
  expect(generateStyles({ tagName: 'u' }, styleSet)).toMatchSnapshot(
    'UNDERLINE',
  );
});

test('code', () => {
  expect(generateStyles({ tagName: 'code' }, styleSet)).toMatchSnapshot('CODE');
});

test('colors', () => {
  expect(
    generateStyles(
      {
        tagName: 'color',
        attributes: { color: 'blue' },
      },
      styleSet,
    ),
  ).toMatchSnapshot('COLOR_BLUE');

  expect(
    generateStyles(
      {
        tagName: 'color',
        attributes: { color: '#000000' },
      },
      styleSet,
    ),
  ).toMatchSnapshot('COLOR_#000000');
});

test('colors - missing color', () => {
  expect(
    generateStyles(
      {
        tagName: 'color',
        attributes: {},
      },
      styleSet,
    ),
  ).toBe(styleSet);
});

test('no style', () => {
  expect(generateStyles({ tagName: 'unsupportedTag' }, styleSet)).toBe(
    styleSet,
  );
});
