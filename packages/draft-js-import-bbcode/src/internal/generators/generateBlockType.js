//@flow

type GenerateBlockType = ({ tagName?: string }) => string;
const generateBlockType: GenerateBlockType = ({ tagName } = {}) => {
  if (tagName === 'quote') {
    return 'blockquote';
  }

  return 'unstyled';
};

export default generateBlockType;
