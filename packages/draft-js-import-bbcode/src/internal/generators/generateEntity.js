import { Entity } from 'draft-js';

const generateEntity = ({ tagName, content, attributes }, entity) => {
  if (tagName === 'url') {
    let { url } = attributes;
    url = url || content;

    return Entity.create('LINK', 'MUTABLE', { url });
  }

  if (tagName === 'img') {
    return Entity.create('IMAGE', 'IMMUTABLE', { ...attributes, src: content });
  }

  if (tagName === 'video') {
    const { video } = attributes;

    if (video === 'youtube') {
      return Entity.create('EMBED', 'IMMUTABLE', {
        url: `https://www.youtube.com/watch?v=${content}`,
      });
    }

    return entity;
  }

  return entity;
};

export default generateEntity;
