const generateStyles = ({ tagName, attributes }, styleSet) => {
  if (tagName === 'b') {
    return styleSet.add('BOLD');
  }

  if (tagName === 'i') {
    return styleSet.add('ITALIC');
  }

  if (tagName === 'u') {
    return styleSet.add('UNDERLINE');
  }

  if (tagName === 'code') {
    return styleSet.add('CODE');
  }

  if (tagName === 'color') {
    if (typeof attributes.color === 'string' && attributes.color.length > 0) {
      return styleSet.add(`COLOR_${attributes.color.toUpperCase()}`);
    }

    return styleSet;
  }

  return styleSet;
};

export default generateStyles;
