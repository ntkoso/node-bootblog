// @flow
import { CharacterMetadata } from 'draft-js';
import { Repeat } from 'immutable';
import createTagFromTree from './utils/createTagFromTree';
import createEmptyChunk from './utils/createEmptyChunk';
import getLastStyleAndEntity from './utils/getLastStyleAndEntity';
import isAtomic from './utils/isAtomic';
import generateBlockType from './generators/generateBlockType';
import generateEntity from './generators/generateEntity';
import generateStyles from './generators/generateStyles';
import { TREE_TYPES, BLOCK_TAGS } from './constants';

const update = (index, value, array) => {
  const result = [...array];

  result[index] = value;

  return result;
};

const processRootSubtree = (chunks, tree) => {
  const chunk = createEmptyChunk();

  const currentChunks = [...chunks, chunk];

  return processTree(currentChunks, tree); // eslint-disable-line
};

export const processTextTree = (chunks, tree) => {
  let currentChunk = chunks.slice(-1)[0];

  const { style, entity } = getLastStyleAndEntity(currentChunk);
  let { content } = tree;

  content = content.replace(/(\r\n|\r|\n)/g, '\n');
  content = content.replace(/(\n\n)/g, '\n');
  content = content.split('\u200B').join('\r');

  const charMetadata = CharacterMetadata.create({ style, entity });
  const characterMetaSequence = new Repeat(charMetadata, content.length);

  currentChunk = {
    ...currentChunk,
    textChunks: [
      ...currentChunk.textChunks,
      {
        text: content,
        characterMeta: characterMetaSequence,
      },
    ],
  };

  return update(chunks.length - 1, currentChunk, chunks);
};

const processSubtrees = (chunks, trees) =>
  Array.isArray(trees) && trees.length > 0
    ? trees.reduce(processTree, chunks)
    : chunks;

export const processBlockTagTree = (chunks, tree) => {
  const type = generateBlockType(createTagFromTree(tree));

  const chunk = createEmptyChunk(type);

  let currentChunks = [...chunks, chunk];

  currentChunks = processSubtrees(currentChunks, tree.subTrees);

  return currentChunks;
};

export const processInlineTagTree = (chunks, tree) => {
  let currentChunk = chunks.slice(-1)[0];
  let { style, entity } = getLastStyleAndEntity(currentChunk);
  const tag = createTagFromTree(tree);

  style = generateStyles(tag, style);
  entity = generateEntity(tag, entity);

  currentChunk = {
    ...currentChunk,
    type: isAtomic(tag) ? 'atomic' : 'unstyled',
    styles: [...currentChunk.styles, style],
    entities: [...currentChunk.entities, entity],
  };

  let currentChunks = update(chunks.length - 1, currentChunk, chunks);

  currentChunks = processSubtrees(currentChunks, tree.subTrees); // eslint-disable-line

  let lastChunk = currentChunks.slice(-1)[0];
  lastChunk = {
    ...lastChunk,
    styles: lastChunk.styles.slice(0, lastChunk.styles.length - 1),
    entities: lastChunk.entities.slice(0, lastChunk.entities.length - 1),
  };

  currentChunks = update(currentChunks.length - 1, lastChunk, currentChunks);

  return currentChunks;
};

export const processRootTree = (chunks, tree) =>
  tree.subTrees.reduce(processRootSubtree, chunks);

export default function processTree(chunks, tree) {
  let currentChunks = chunks;

  if (tree.treeType === TREE_TYPES.Text) {
    currentChunks = processTextTree(currentChunks, tree);
  } else if (tree.treeType === TREE_TYPES.Tag) {
    if (BLOCK_TAGS.some(tag => tag === tree.content)) {
      currentChunks = processBlockTagTree(currentChunks, tree);
    } else {
      currentChunks = processInlineTagTree(currentChunks, tree);
    }
  } else if (tree.treeType === TREE_TYPES.Root) {
    currentChunks = processRootTree(currentChunks, tree);
  }

  return currentChunks;
}
