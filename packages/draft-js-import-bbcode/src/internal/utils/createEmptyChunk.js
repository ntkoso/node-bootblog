import { OrderedSet } from 'immutable';

const createEmtyChunk = (type = 'unstyled') => ({
  type,
  depth: 0,
  textChunks: [],
  styles: [new OrderedSet()],
  entities: [null],
});

export default createEmtyChunk;
