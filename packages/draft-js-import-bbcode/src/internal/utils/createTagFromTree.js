const createTagFromTree = ({ content, attributes, subTrees }) => ({
  tagName: content,
  content: subTrees[0] ? subTrees[0].content : content,
  attributes,
});

export default createTagFromTree;
