// @flow
const getLastStyleAndEntity = <Style, Entity>(block: {
  styles: Array<Style>,
  entities: Array<Entity>,
}): {| style: Style, entity: Entity |} => ({
  style: block.styles.slice(-1)[0],
  entity: block.entities.slice(-1)[0],
});

export default getLastStyleAndEntity;
