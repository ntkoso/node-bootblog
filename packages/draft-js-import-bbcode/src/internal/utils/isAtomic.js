import { ATOMIC_TAGS } from '../constants';

const isAtomic = ({ tagName }) => ATOMIC_TAGS.some(tag => tag === tagName);

export default isAtomic;
