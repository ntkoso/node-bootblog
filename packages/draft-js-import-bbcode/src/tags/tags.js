import { defaultTags } from 'bbcode-parser';
import BBTag from 'bbcode-parser/bbTag';
import escape from 'escape-html';
import { absoluteToRelativePath } from '@ntkoso/utils';

const videoWrprClass = 'BBCodeParser__thirdPartyVideoWrapper';
const imgClass = 'BBCodeParser__image';

// NOTE: old site was using absolute links...
const oldAbsolutePathStart = 'http://noviyvitok.com/';

const fullWidthThirdPartyVideoWrapper = ifrm =>
  `<div class="${videoWrprClass}">${ifrm}</div>`;

const tags = {
  ...defaultTags(),
  color: BBTag.createTag(
    'color',
    (tag, content, { color }) =>
      color
        ? `<span style="color: ${escape(color)}">${content}</span>`
        : content,
  ),
  img: BBTag.createTag('img', (tag, content, { width, height }) => {
    let src = content;

    if (content.startsWith(oldAbsolutePathStart)) {
      src = absoluteToRelativePath(content, oldAbsolutePathStart);
    }

    let widthAttr = '';
    if (width) {
      widthAttr = `width=\"${escape(width)}\"`;
    }

    let heightAttr = '';
    if (height) {
      heightAttr = `height=\"${escape(height)}\"`;
    }

    return `<img class="${imgClass}" src="${escape(src)}" ${widthAttr} ${
      heightAttr
    }/>`;
  }),
  url: BBTag.createTag('url', (tag, content, { url }) => {
    let href = url;

    if (url.startsWith(oldAbsolutePathStart)) {
      href = absoluteToRelativePath(url, oldAbsolutePathStart);
    }

    return `<a href=${escape(href)}>${content}</a>`;
  }),
  quote: BBTag.createTag(
    'quote',
    (tag, content) => `<blockquote>${content}</blockquote>`,
  ),
  video: BBTag.createTag(
    'video',
    (tag, content, { video }) =>
      video === 'youtube'
        ? fullWidthThirdPartyVideoWrapper(
            `<iframe frameborder="0" src="https://www.youtube.com/embed/${escape(
              content,
            )}"></iframe>`,
          )
        : `${video} block`,
  ),
};

export default tags;
