import * as moduleExports from '../index';

test('has correct exports', () => {
  expect(
    Object.entries(moduleExports).map(([key, value]) => ({
      [key]: Boolean(value),
    })),
  ).toMatchSnapshot();
});
