// @flow
import * as emotion from '@ntkoso/emotion';
import createEmotionServer from 'create-emotion-server';

export const {
  extractCritical,
  renderStylesToString,
  renderStylesToNodeStream
} = createEmotionServer(emotion);
