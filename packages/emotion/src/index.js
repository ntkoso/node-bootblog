// @flow
import uuidV4 from 'uuid/v4';
import createEmotion from 'create-emotion';

const context = typeof global !== 'undefined' ? global : {};

let nonce;

if (typeof nonce === 'undefined' && typeof window === 'undefined') {
  nonce = uuidV4();
} else if (
  typeof nonce === 'undefined' &&
  typeof window !== 'undefined' &&
  typeof window.__EMOTION_NONCE__ !== 'undefined'
) {
  nonce = window.__EMOTION_NONCE__;
}

export { nonce };

export const {
  flush,
  hydrate,
  cx,
  merge,
  getRegisteredStyles,
  injectGlobal,
  keyframes,
  css,
  sheet,
  caches,
} = createEmotion(context, {
  nonce,
  prefix: true,
  key: 'css',
});
