const isProduction = process.env.NODE_ENV === 'production';
const isTest = process.env.NODE_ENV === 'test';
const moduleType = process.env.MODULE_ENV || false;

const presets = [
  [
    '@babel/preset-env',
    {
      debug: !isProduction,
      modules: isTest ? 'commonjs' : moduleType,
      forceAllTransforms: true,
    },
  ],
];

let plugins = [
  '@babel/plugin-transform-flow-comments',
  '@babel/plugin-proposal-class-properties',
  '@babel/plugin-proposal-object-rest-spread',
  '@babel/plugin-syntax-class-properties',
  '@babel/plugin-syntax-flow',
  '@babel/plugin-syntax-object-rest-spread',
];

if (isProduction) {
}

module.exports = { presets, plugins };
