// @flow
import type { LRUCache } from 'lru-cache';
import type { CacheProvider, SetOptions } from '../types';

class LRUCacheProvider
  implements CacheProvider<LRUCache<string, any>, SetOptions> {
  cache: LRUCache<string, any>;

  constructor(lru: LRUCache<string, any>) {
    this.cache = lru;
  }

  async get(key: string) {
    return this.cache.get(key);
  }

  async set(key: string, value: any, { maxAge }: SetOptions = {}) {
    this.cache.set(key, value, maxAge);
  }
}

export default LRUCacheProvider;
