// @flow
import type { Redis } from 'ioredis';
import type { CacheProvider, SetOptions } from '../types';

class RedisCacheProvider implements CacheProvider<Redis, SetOptions> {
  cache: Redis;

  constructor(redis: Redis) {
    this.cache = redis;
  }

  async get(key: string) {
    return this.cache.hgetallAsync(key);
  }

  async set(key: string, value: any, { maxAge }: SetOptions = {}) {}
}

export default RedisCacheProvider;
