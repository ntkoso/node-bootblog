// @flow

export interface CacheProvider<C, O> {
  cache: C;

  get(key: string): Promise<any>;

  set(key: string, value: any, options: O): Promise<void>;
}

export type SetOptions = {
  maxAge?: number,
};
