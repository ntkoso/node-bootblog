// @flow
import h from 'object-hash';
import contextKey from './contextKey';

const stripObject = (obj = {}) => JSON.parse(JSON.stringify(obj));

const withCache = (resolver: Function, options = {}) => async (
  root,
  args,
  context,
  resolveInfo,
) => {
  if (!context[contextKey]) {
    throw new Error(`Missing ${contextKey} property on GraphQL context`);
  }

  const cacheKey = `${resolver.name || 'resolver'}:${h(stripObject(root))}:${h(
    stripObject(args),
  )}`;

  let result = await context[contextKey].get(cacheKey);

  console.log('CACHE HIT', result);

  if (!result) {
    result = await resolver(root, args, context, resolveInfo);

    context[contextKey].set(cacheKey, result, options);
  }

  return result;
};

export default withCache;
