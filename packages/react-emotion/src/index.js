// @flow
import React from 'react';
import * as emotion from '@ntkoso/emotion';
import createEmotionStyled from 'create-emotion-styled';

export default createEmotionStyled(emotion, React);

export * from '@ntkoso/emotion';
