import absoluteToRelativePath from '../absoluteToRelativePath';

test('removes absolute portion of path', () => {
  expect(
    absoluteToRelativePath('http://path.tst/address', 'http://path.tst'),
  ).toMatchSnapshot();
});
