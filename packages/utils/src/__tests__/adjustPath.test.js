import adjustPath from '../adjustPath';

test('preprepends forward slash', () => {
  expect(adjustPath('path')).toMatchSnapshot();
});

test('avoids adding extra forward slash', () => {
  expect(adjustPath('/path')).toMatchSnapshot();
});
