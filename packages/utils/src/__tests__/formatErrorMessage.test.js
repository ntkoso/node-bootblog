import formatErrorMessage from '../formatErrorMessage';

const fm = jest.fn((message, error) =>
  JSON.stringify({ ...error, ...message }),
);

test('formats valid translation message', () => {
  expect(formatErrorMessage(fm, { message: 'test.string' })).toMatchSnapshot();
});

test('ignores invalid translation message', () => {
  expect(formatErrorMessage(fm, { message: 'invalid' })).toMatchSnapshot();
});
