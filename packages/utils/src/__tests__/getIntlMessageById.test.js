import getIntlMessageById from '../getIntlMessageById';

const id = 'test.translation';

test('generates default message descriptor', () => {
  expect(getIntlMessageById(id, {})).toEqual({
    id,
    defaultMessage: `${id} - translation message is missing`,
  });
});

test('generates correct message descriptor', () => {
  expect(
    getIntlMessageById(id, {
      [id]: { id, defaultMessage: 'translated message' },
    }),
  ).toEqual({
    id,
    defaultMessage: 'translated message',
  });
});
