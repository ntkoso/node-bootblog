import * as moduleExports from '../index';

test('has correct module exports', () => {
  expect(
    Object.entries(moduleExports).map(([key, value]) => ({
      [key]: Boolean(value),
    })),
  ).toMatchSnapshot();
});
