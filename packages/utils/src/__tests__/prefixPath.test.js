import prefixPath from '../prefixPath';

test('adds prefix correctly', () => {
  expect(prefixPath('prefix', '/path')).toMatchSnapshot();
});
