import srvValidationToFormErrors from '../srvValidationToFormErrors';

test('reduces array of errors to the formik formatted error', () => {
  expect(
    srvValidationToFormErrors([
      {
        path: 'error',
        message: 'Something gone wrong',
      },
      {
        path: 'secondError',
        message: 'Something gone wrong again',
      },
    ]),
  ).toEqual({
    error: 'Something gone wrong',
    secondError: 'Something gone wrong again',
  });
});
