// @flow
import adjustPath from './adjustPath';

const absoluteToRelative = (content: string, path: string): string =>
  adjustPath(content.substr(path.length));

export default absoluteToRelative;
