// @flow

const adjustPath = (path: string) => {
  if (path === '/') {
    return '';
  }

  if (!path.startsWith('/')) {
    return `/${path}`;
  }

  return path;
};

export default adjustPath;
