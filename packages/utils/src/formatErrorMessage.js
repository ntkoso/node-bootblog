// @flow
import type { MessageDescriptor } from 'react-intl';

const defaultMessage = '[{message}] - missing translation';
const description = 'User Error, e.g. wrong form field';

const isValid = (str: ?string): boolean =>
  typeof str === 'string' &&
  str.includes('.') &&
  !str.includes(' ') &&
  !str.startsWith('.');

type FormatMessage = (
  messageDescriptor: MessageDescriptor,
  values?: {},
) => string;

const formatErrorMessage = (
  formatMessage: FormatMessage,
  error: { message: string },
): { message: string } => {
  const { message } = error;

  return isValid(message)
    ? {
        ...error,
        message: formatMessage(
          { id: message, defaultMessage, description },
          error,
        ),
      }
    : error;
};

export default formatErrorMessage;
