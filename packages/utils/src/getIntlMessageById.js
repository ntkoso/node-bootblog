// @flow
import type { MessageDescriptor } from 'react-intl';

const getIntlMessageById = (
  id: string,
  messages: { [key: string]: MessageDescriptor },
): MessageDescriptor => {
  const message = Object.entries(messages).find(
    ([messageId]) => id === messageId,
  );

  return message && message[1]
    ? message[1]
    : {
        id,
        defaultMessage: `${id} - translation message is missing`,
      };
};

export default getIntlMessageById;
