// @flow
import adjustPath from './adjustPath';

const prefixPath = (prefix: string, path: string): string =>
  prefix + adjustPath(path);

export default prefixPath;
