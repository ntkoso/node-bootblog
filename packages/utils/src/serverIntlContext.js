// @flow
import type { MessageDescriptor } from 'react-intl';
import memoizeFormatConstructor from 'intl-format-cache';
import IntlMessageFormat from 'intl-messageformat';

const state = {
  getMessageFormat: memoizeFormatConstructor(IntlMessageFormat),
};

/* Extracted from https://github.com/yahoo/react-intl/blob/master/src/format.js#L166-L253 */

type Config = {|
  +locale: string,
  +formats: {},
  +messages: { [id: string]: string },
  +defaultLocale?: string,
  +defaultFormats?: {},
|};

type Values = { +[id: string]: string };

type ServerIntlContext = {|
  +formatMessage: (
    messageDescriptor: MessageDescriptor,
    values?: Values,
  ) => string,
|};

const serverIntlContext = ({
  locale,
  formats,
  messages,
  defaultLocale = 'en',
  defaultFormats = {},
}: Config): ServerIntlContext => ({
  formatMessage: (
    { id, defaultMessage }: MessageDescriptor,
    values?: Values = {},
  ): string => {
    const message = messages && messages[id];
    const hasValues = Object.keys(values).length > 0;

    // Avoid expensive message formatting for simple messages without values. In
    // development messages will always be formatted in case of missing values.
    if (!hasValues && process.env.NODE_ENV === 'production') {
      return message || defaultMessage || id;
    }

    let formattedMessage;

    if (message) {
      try {
        const formatter = state.getMessageFormat(message, locale, formats);

        formattedMessage = formatter.format(values);
      } catch (e) {
        if (process.env.NODE_ENV !== 'production') {
          console.error(
            // eslint-disable-line
            `[React Intl] Error formatting message: "${id}" for locale: "${
              locale
            }"
            ${defaultMessage ? ', using default message as fallback.' : ''}
            ${e}`,
          );
        }
      }
    }

    if (process.env.NODE_ENV !== 'production') {
      // This prevents warnings from littering the console in development
      // when no `messages` are passed into the <IntlProvider> for the
      // default locale, and a default message is in the source.
      if (
        !defaultMessage ||
        (locale && locale.toLowerCase() !== defaultLocale.toLowerCase())
      ) {
        console.error(
          // eslint-disable-line
          `[React Intl] Missing message: "${id}" for locale: "${locale}"
          ${defaultMessage ? ', using default message as fallback.' : ''}`,
        );
      }
    }

    if (!formattedMessage && defaultMessage) {
      try {
        const formatter = state.getMessageFormat(
          defaultMessage,
          defaultLocale,
          defaultFormats,
        );

        formattedMessage = formatter.format(values);
      } catch (e) {
        if (process.env.NODE_ENV !== 'production') {
          console.error(
            // eslint-disable-line
            `[React Intl] Error formatting the default message for: "${id}"` +
              `\n${e}`,
          );
        }
      }
    }

    if (!formattedMessage) {
      if (process.env.NODE_ENV !== 'production') {
        console.error(
          // eslint-disable-line
          `[React Intl] Cannot format message: "${id}", ` +
            `using message ${
              message || defaultMessage ? 'source' : 'id'
            } as fallback.`,
        );
      }
    }

    return formattedMessage || message || defaultMessage || id;
  },
});

export default serverIntlContext;
