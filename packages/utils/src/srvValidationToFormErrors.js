// @flow
import set from 'immutable-set';

const srvValidationToFormErrors = <T: Object>(validationError: T) =>
  validationError.reduce(
    (prev, { path, message }) => set(prev, path, message),
    {},
  );

export default srvValidationToFormErrors;
